var path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const globImporter = require('node-sass-glob-importer');


module.exports = {
  entry: ["babel-polyfill", "./assets/js/outerapp.tsx"],
  output: {
    path: path.resolve(__dirname, '.tmp/public/'),
    filename: 'js/outerapp.js'
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  mode: "development",
  devtool: "cheap-module-source-map",
  module: {
    rules: [
      {
        test: /\.(tsx|ts)?$/,
        loader: "ts-loader",
        options: {
          configFile: "../../tsconfig-fe.json"
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};
