import * as React from "react";
import PostList from "../components/posts/postlist";
import SideCard from "../components/infocards/SideCard";
import WhoToFollow from "../components/homepage/WhoToFollow";
import WhatToFollow from "../components/homepage/WhatToFollow";
import UserInfoBoard from "../components/homepage/UserInfoBoard";
import OrderBy from "../components/posts/orderby";
import { IPublicPostInfo } from "../../../api/interfaces/responses/IPublicPostInfo";
import { IWithRouter } from "../Interfaces/IWithRouter";
import CategoryServices from "../services/CategoryServices";

interface IProps extends IWithRouter {

}


export default class Category extends React.Component<IProps,{
    categoryInfo?: any
}> {
    constructor(props: any){
        super(props);
        this.state = {
            categoryInfo: null
        }
    }

    private fetchInfo() {
        CategoryServices.getCategory((this.props.match.params as any)["categoryname"]).then((response)=>{
            this.setState({
                categoryInfo: response
            });
        });
    }

    componentDidMount() {
        this.fetchInfo();
    }

    componentDidUpdate() {
        if (this.state.categoryInfo && this.state.categoryInfo.id != null && ((this.props.match.params as any)["categoryname"] != this.state.categoryInfo.categoryName)) {
            this.setState({
                categoryInfo: null
            }, ()=>{
                this.fetchInfo();
            });
        }
    }


    fetchPosts(selectSettings: {orderby: string, category?: number }, posts: any[], callback: (res: IPublicPostInfo[])=>void) {
        if (!selectSettings.category) return;
        let req = new Request(`/posts/category/${selectSettings.orderby}/10/${posts ? posts.length : 0}/${selectSettings.category}`);
        fetch(req).then((stream)=>{
            stream.json().then((res: IPublicPostInfo[])=>{
                callback(res);
            });
        });
    }

    render() {
        return (
            <div className="container-fluid">
                {this.state.categoryInfo != null ? 
                    (this.state.categoryInfo.id != null ? 
                        <div className="category layout-common">
                            <div className="col-info">
                                <SideCard className="float-right p-4">
                                    <WhoToFollow/>
                                    <WhatToFollow/>
                                </SideCard>
                            </div>
                            <div className="col-posts">
                                <PostList orderable={true} categorySelector={true} categoryId={this.state.categoryInfo.id} fetchPosts={this.fetchPosts.bind(this)}/>
                            </div>
                            <div className="col-info">
                                <SideCard className="float-left p-0">
                                    <UserInfoBoard/>
                                </SideCard>
                            </div>
                        </div>
                    
                    : <div className="post no-content">There no category {(this.props.match.params as any)["categoryname"]} yet.</div>)
                : <div className="loader page"></div>}
                <div className="background-image">
                    <img src={"/images/cats/" + (this.props.match.params as any)["categoryname"] + ".png"}/>
                    {this.state.categoryInfo && this.state.categoryInfo.id != null ? <div style={{color: this.state.categoryInfo.associatedColor}} className="background-title">{this.state.categoryInfo.categoryName}</div> : ""}
                </div>
            </div>
        );
    }
}