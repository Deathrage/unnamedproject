import * as React from "react";
import { Link } from 'react-router-dom'
import HeaderProfile from "../components/header/HeaderProfile";
import SearchBar from "../components/header/SearchBar";
import PostButton from "../components/header/PostButton";
import Notifications from "./Notifications";
import { URL } from "../ulrs";
import Menu from "../components/header/Menu";

export default class Header extends React.Component {
    constructor(props:any){
        super(props)
    }
    
    render() {
        return (
            <header className="navbar inapp d-flex justify-content-between">
                <HeaderProfile className="col-1"/>
                <nav className="nav d-flex col-10 align-items-center justify-content-around">
                    <PostButton/>
                    <Link to={URL.ACTIVITY}>
                        <i className="fas fa-chart-bar"></i>
                    </Link>
                    <SearchBar/>
                    <Notifications/>
                    <Link to={URL.HOMEPAGE} className="navbar-brand">
                        <img className="idle" src="/images/feed.white.svg" width="30" height="30"/>
                        <img className="hover" src="/images/feed.grey.svg" width="30" height="30"/>
                    </Link>
                </nav>
                <Menu/>
            </header>
        );
    }
}