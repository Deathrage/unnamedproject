import * as React from "react";

export default class Notifications extends React.Component<{
    className?: string
}, {
    notificationCount: number
}> {
    constructor(props:any){
        super(props);
        this.state = {
            notificationCount: 0
        }
    }
    
    render() {
        return (
            <a className={this.props.className || "" + " notification d-flex align-items-center"}>
                <i className="fas fa-envelope"></i>
                <span className="notification-count">
                    {this.state.notificationCount}
                </span>
            </a>
        );
    }
}