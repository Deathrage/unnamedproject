import * as React from "react"
import Header from "./Header";
import { Switch, Route } from "react-router";
import Homepage from "./Homepage";
import Activity from "./Activity";
import MyProfile from "./MyProfile";
import Profile from "./Profile";
import { withRouter } from "react-router";
import { IWithRouter } from "../Interfaces/IWithRouter";
import { Action } from "history";
import * as queryString from "query-string";
import PopupManager from "../components/popups/PopupManager";
import Category from "./Category";
import SmartSearch from "./SmartSearch";
import PostDetail from "./PostDetail";
import NotFound from "./NotFound";

interface IProps extends IWithRouter {

}

class InnerApp extends React.Component<IProps ,{}>{
    unlisten: Function;
    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this.handeRedirection(null,null);
        this.unlisten = this.props.history.listen(this.handeRedirection.bind(this));
    }
    componentWillUnmount() {
        this.unlisten();
    }

    private handeRedirection(location: Location, action: Action) {
        window.context.queryString = queryString.parse(window.location.search);
    }

    render() {
        return (
            <div className="app">
                <Header />
                <Switch>
                    <Route exact path="/" component={Homepage} />
                    <Route exact path="/activity" component={Activity} />
                    <Route exact path="/profile" component={MyProfile} />
                    <Route exact path="/profile/:username" component={Profile} />
                    <Route exact path="/category/:categoryname" component={Category} />
                    <Route exact path="/search/:phrase" component={SmartSearch} />
                    <Route exact path="/post/:postid" component={PostDetail} />
                    <Route exact path="*" component={NotFound} />
                </Switch>
                <PopupManager/>
            </div>
        );
    }
}

export default withRouter(InnerApp);