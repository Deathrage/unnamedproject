import * as React from "react";
import { IWithRouter } from "../Interfaces/IWithRouter";
import { IPublicPostInfo } from "../../../api/interfaces/responses/IPublicPostInfo";
import { Redirect } from "react-router";
import MediaHelper from "../utils/MediaHelper";
import { Link } from "react-router-dom";
import CommentList from "../components/posts/comments/commentlist";
import AddComment from "../components/posts/comments/addcomment";
import Comments from "../components/posts/comments/comments";
import Stylings from "../utils/Stylings";
import LikeButton from "../components/posts/likes/LikeButton";

export default class PostDetail extends React.Component<IWithRouter,{
    post: IPublicPostInfo
}> {
    private boundFunctions: any;
    constructor(props: any){
        super(props);
        this.state = {
            post: null
        }
        this.boundFunctions = {
            incrementComments: this.incrementComments.bind(this)
        };
    }

    componentWillUnmount() {
        io.socket.off("commentadd", this.boundFunctions.incrementComments);
    }
    componentDidMount() {
        io.socket.on("commentadd", this.boundFunctions.incrementComments);
        this.fetchPostDetail(); 
    }

    componentDidUpdate() {
        //console.log(this.state.post);
    }

    componentWillReceiveProps(newprops: any) {
    }

    private openAddComment() {
        let com = this.refs["comments"] as Comments;
        if (com) com.toggleOpen();
    }

    render() {
        return (
            <div className="container-fluid">
                {this.state.post != null ?
                    (this.state.post.id > 0 ? 
                        <div className="post-detail layout-common justify-content-center">
                            {this.state.post.attachedFile > 0 ? 
                                <div className="col-posts">
                                    <div className="card image">
                                        <div className="card-img">
                                            {this.state.post.mime.indexOf("image") > -1 ? 
                                                <img src={this.state.post.attachedFileUrl} alt={this.state.post.name} />
                                            : <a download href={this.state.post.attachedFileUrl} className="card-file" dangerouslySetInnerHTML={{__html: MediaHelper.getFileIcon(this.state.post.extension) + "<div>" + this.state.post.name + "." + this.state.post.extension + "</div>"}}></a>}
                                        </div>
                                    </div>
                                </div> 
                            : "" }
                            <div className="col-posts">
                                <div className="post card">
                                    <div className="card-header">
                                        <div className="row">
                                            <div className="col-6">
                                                <Link to={encodeURI("/profile/" + this.state.post.username)} className="cursor-pointer profile d-inline-flex">
                                                    {this.state.post.profilePicture.length > 0 ?<div className="img"> <img src={this.state.post.profilePicture.split("/").pop() != "" ? this.state.post.profilePicture : "/images/image-placeholder.png"}/> </div>: ""} 
                                                    <div className="info">
                                                        <span className="text-weight-semibold username">@{this.state.post.username}</span>
                                                    </div>
                                                </Link>
                                            </div>
                                            <div className="col-6 d-flex justify-content-start flex-column flex-wrap">
                                                <h5 className="card-title">{this.state.post.title}</h5>
                                                <div className="text-right card-date w-100">{Stylings.formatDate(new Date(Number(this.state.post.createdAt)))}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <p>{this.state.post.description}</p>
                                    </div>
                                    <div className="card-footer d-block">
                                        <div className="d-flex">
                                            <div className="comment-button cursor-pointer" onClick={this.openAddComment.bind(this)}>
                                                <i className="far fa-comment"></i>
                                                <span className="count">{this.state.post.commentCount || 0}</span>
                                            </div>
                                            <div className="share-button">
                                                <i className="fas fa-share"></i>
                                            </div>
                                            <LikeButton post={this.state.post} />
                                        </div>
                                    </div>
                                </div>
                                <Comments ref="comments"/>
                            </div>
                        </div>
                    : <Redirect to="/404" />)
                : <div className="loader page"></div>}
            </div>
        );
    }

    private incrementComments() {
        let post = this.state.post;
        post.commentCount = Number(post.commentCount) + 1;
        this.setState({
            post: post
        });
    }

    private fetchPostDetail() {
        fetch("/posts/get/" + (this.props.match.params as any)["postid"]).then(response=>{
            response.json().then(postdetail=>{
                this.setState({
                    post: postdetail
                });
            });
        });
    }
}