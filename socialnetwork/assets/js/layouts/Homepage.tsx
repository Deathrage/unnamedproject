import * as React from "react";
import PostList from "../components/posts/postlist";
import SideCard from "../components/infocards/SideCard";
import WhoToFollow from "../components/homepage/WhoToFollow";
import WhatToFollow from "../components/homepage/WhatToFollow";
import UserInfoBoard from "../components/homepage/UserInfoBoard";
import OrderBy from "../components/posts/orderby";
import { IPublicPostInfo } from "../../../api/interfaces/responses/IPublicPostInfo";

export default class Homepage extends React.Component {
    constructor(props: any){
        super(props);
    }

    fetchFeed(selectSettings: {orderby: string, category?: number }, posts: any[], callback: (res: IPublicPostInfo[])=>void) {
        let req = new Request(`/posts/feed/${selectSettings.orderby}/10/${posts ? posts.length : 0}`);
        fetch(req).then((stream)=>{
            stream.json().then((res: IPublicPostInfo[])=>{
                callback(res);
            });
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="homepage layout-common">
                    <div className="col-info">
                        <SideCard className="float-right p-4">
                            <WhoToFollow/>
                            <WhatToFollow/>
                        </SideCard>
                    </div>
                    <div className="col-posts">
                        <PostList orderable={true} categorySelector={true} fetchPosts={this.fetchFeed.bind(this)}/>
                    </div>
                    <div className="col-info">
                        <SideCard className="float-left p-0">
                            <UserInfoBoard/>
                        </SideCard>
                    </div>
                </div>
            </div>
        );
    }
}