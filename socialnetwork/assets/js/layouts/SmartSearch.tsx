import * as React from "react";
import SideCard from "../components/infocards/SideCard";
import WhoToFollow from "../components/homepage/WhoToFollow";
import WhatToFollow from "../components/homepage/WhatToFollow";
import UserInfoBoard from "../components/homepage/UserInfoBoard";
import { IWithRouter } from "../Interfaces/IWithRouter";
import {SearchBar} from "../components/header/SearchBar";
import ResultList from "../components/smartsearch/resultlist";

interface IProps extends IWithRouter {

}


export default class SmartSearch extends React.Component<IProps,{
    results: [],
    phrase: string
}> {
    constructor(props: any){
        super(props);
        this.state = {
            results: null,
            phrase: this.searchPhrase
        }
    }

    get searchPhrase(): string {
        return decodeURI(String((this.props.match.params as any)["phrase"]) || "");
    }

    private setSearchBar() {
        if (window.app.searchBar) {
            (window.app.searchBar as SearchBar).setState({
                phrase: this.searchPhrase
            });
        }
    }

    private searchRequest() {
        if (this.searchPhrase.trim().length > 0) {
            let req = new Request("/smartsearch/200/" + (this.state.results != null ? this.state.results.length : 0) + "/" + this.searchPhrase);
            fetch(req).then(response => {
                response.json().then(json=>{
                    this.setState({
                        results: json
                    })
                });
            })
        } else {
            this.props.history.push("/");
        }
    }

    componentDidMount() {
        //this.setSearchBar();
        this.searchRequest();
    }

    componentDidUpdate() {
        if (this.state.phrase != this.searchPhrase) {
            //this.setSearchBar();
            this.setState({
                results: null,
                phrase: this.searchPhrase
            }, ()=>{
                this.searchRequest();
            });
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="category layout-common">
                    <div className="col-info">
                        <SideCard className="float-right p-4">
                            <WhoToFollow/>
                            <WhatToFollow/>
                        </SideCard>
                    </div>
                    <div className="col-posts">
                        <ResultList results={this.state.results} phrase={this.state.phrase}/>
                    </div>
                    <div className="col-info">
                        <SideCard className="float-left p-0">
                            <UserInfoBoard/>
                        </SideCard>
                    </div>
                </div>
                <div className="background-image">
                    <div className="uni"></div>
                    {this.searchPhrase != null ? <div style={{color: "#4a4a4a"}} className="background-title">{this.searchPhrase}</div> : ""}
                </div>
            </div>
        );
    }
}