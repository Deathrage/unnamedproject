import * as React from "react";
import { IWithRouter } from "../Interfaces/IWithRouter";
import SideCard from "../components/infocards/SideCard";
import PublicInfoBoard from "../components/profile/PublicInfoBoard";
import ProfileDetail from "../components/profile/ProfileDetail";
import { IFollowStats } from "../../../api/interfaces/responses/IFollowStats";
import UserServices from "../services/UserServices";
import { IPostStats } from "../../../api/interfaces/responses/IPostStats";
import IPublicUserInfo from "../../../api/interfaces/responses/IPublicUserInfo";
import PostList from "../components/posts/postlist";
import { IPublicPostInfo } from "../../../api/interfaces/responses/IPublicPostInfo";
import { IApiResponse } from "../../../api/interfaces/IApiResponse";

interface IProps extends IWithRouter {

}

export default class Profile extends React.Component<IProps, {
    followStats: IFollowStats,
    postStats: IPostStats,
    userInfo: IPublicUserInfo
}> {
    userInfoPromise: Promise<IPublicUserInfo>;
    boundFunctions: any;
    constructor(props: any){
        super(props);
        this.state = {
            followStats: null,
            postStats: null,
            userInfo: null
        }
        this.boundFunctions = {
            udpateFollowStats: this.udpateFollowStats.bind(this)
        }
    }

    componentDidMount() {
        let username = (this.props.match.params as any)["username"];
        this.userInfoPromise = UserServices.getUserInfo(username);
        this.userInfoPromise.then((res)=>{
            if (res.hasOwnProperty("success") && !(res as any as IApiResponse).success) return;
            this.setState({
                userInfo: res
            });
            if (!res.id) return;
            UserServices.getPostStats(res.id).then((res)=>{
                this.setState({
                    postStats: res
                });
            });
            this.getFollowStats(res.id);
            UserServices.getFollowNumber(res.id).then((res)=>{
                this.setState({
                    followStats: res
                });
            });

            //listen on socket for changes
            io.socket.on("folwupdate" + this.state.userInfo.id, this.boundFunctions.udpateFollowStats);
        });
    }

    componentWillUnmount() {
        io.socket.off("folwupdate" + this.state.userInfo.id, this.boundFunctions.udpateFollowStats);
    }

    private udpateFollowStats(newStats: IFollowStats) {
        this.setState({
            followStats: newStats
        });
    }

    private getFollowStats(id: number) {
        UserServices.getFollowNumber(id).then((res)=>{
            this.setState({
                followStats: res
            });
        });
    }

    fetchPosts(selectSettings: {orderby: string, category?: number }, posts: any[], callback: (res: IPublicPostInfo[])=>void) {
        this.userInfoPromise.then(()=>{
            if (!this.state.userInfo && !this.state.userInfo.id) return;
            let req = new Request(`/posts/list/${selectSettings.orderby}/10/${posts ? posts.length : 0}/${this.state.userInfo.id}`);
            fetch(req).then((stream)=>{
                stream.json().then((res: IPublicPostInfo[])=>{
                    callback(res);
                });
            });
        });
    }

    render() {
        return (
            <div className="container-fluid">
                {this.state.userInfo != null ? (
                    this.state.userInfo.id != null ? 
                        <div className="profile layout-common">
                            <div className="col-info">
                                <SideCard className="float-right p-4">
                                    <ProfileDetail userInfo={this.state.userInfo} />
                                </SideCard>
                            </div>
                            <div className="col-posts">
                                <PostList fetchPosts={this.fetchPosts.bind(this)} orderable={false} />
                            </div>
                            <div className="col-info">
                                <SideCard className="float-left p-0">
                                    <PublicInfoBoard userInfo={this.state.userInfo} postStats={this.state.postStats} followStats={this.state.followStats} />
                                </SideCard>
                            </div>
                        </div> 
                    : <div className="post no-content">There nobody called {(this.props.match.params as any)["username"]} yet.</div>
                ) : <div className="loader page"></div>}
            </div>
        );
    }
}