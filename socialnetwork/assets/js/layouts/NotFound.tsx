import * as React from "react";
import { IWithRouter } from "../Interfaces/IWithRouter";
import { IPublicPostInfo } from "../../../api/interfaces/responses/IPublicPostInfo";

export default class NotFound extends React.Component<IWithRouter,{
    post: IPublicPostInfo
}> {
    constructor(props: any){
        super(props);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="post no-content ml-auto mr-auto">Did you get lost?</div>
            </div>
        );
    }
}