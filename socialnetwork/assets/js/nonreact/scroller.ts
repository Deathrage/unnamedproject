import * as anime from "animejs";
import * as queryString from "query-string";

export default function scroller() {
    let querystrings = queryString.parse(location.search);
    let scrollto = querystrings["scrollto"] as string;
    if (scrollto) {
        scrollTo(scrollto);
    }
}

const scrollTo = (sel: string) =>{
    //get by sel
    let el = document.querySelector("[data-scroll='" + sel + "']") as HTMLElement;
    if (!el) return;

    let top = el.offsetTop;
    let scrollCrods ={
        y: window.pageYOffset
    };
    anime({
        targets: scrollCrods,
        y: (top + window.pageYOffset),
        duration: 1000,
        easing: 'easeInOutCubic',
        update: () => window.scroll(0, scrollCrods.y)
    })
}