import * as anime from "animejs";

export default function teamSection() {
    let clickables = Array.from(document.querySelectorAll(".team-member"));
    if (clickables.length > 0) {
        bindEvents(clickables);
    }
}


const bindEvents = (clickable: Element[]) => {
    clickable.forEach((el)=>{
        el.addEventListener("mouseenter", open);
        el.addEventListener("mouseleave", close);
    });
}


const open = (e: Event) => {
    let seled = getSeled(e) as any;
    if (!seled) return;
    seled.style.opacity = "0";
    seled.style.display = "inline-block";
    if (seled.ongoineanimation) seled.ongoineanimation.pause();
    seled.ongoineanimation = anime({
        targets: seled,
        opacity: 1,
        duration: 2000,
        complete: () => {
            seled.ongoineanimation = null;
        }
    })
}

const close = (e: Event) =>{
    let seled = getSeled(e) as any;
    if (!seled) return;
    if (seled.ongoineanimation) seled.ongoineanimation.pause();
    seled.ongoineanimation = anime({
        targets: seled,
        opacity: 0,
        duration: 2000,
        complete: () => {
            seled.style.display = "none";
            seled.ongoineanimation = null;
        }
    })
}

const getSeled = (e: Event): HTMLElement => {
    let tar = e.target as HTMLElement;
    let sel = tar.dataset.target;
    if (!sel) return null;
    return document.querySelector(sel) as HTMLElement;
}