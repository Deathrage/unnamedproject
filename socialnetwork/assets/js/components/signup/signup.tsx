import * as React from "react";
import { render } from "react-dom";
import Page1 from "./pages/page1"
import Page2 from "./pages/page2"
import Page3 from "./pages/page3"
import EventProvider from "../../utils/EventProvider";
import { IApiResponse } from "../../../../api/interfaces/IApiResponse";
import jsonBeautify from "../../utils/JsonBeautify";

export default class Signup extends React.Component<{}, {
    step: number,
    totalSteps: number,
    input: Object
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            step: 1,
            totalSteps: 3,
            input: new Object()
        }
    }
    private nextStep(incomingState: Object) {
        let newInput = Object.assign(this.state.input, incomingState);
        this.setState({
            step: this.state.step + 1,
            input: newInput
        })
    }
    private prevStep() {
        this.setState({
            step: this.state.step - 1
        })
    }
    private submit(incomingState: Object) {
        let newInput = Object.assign(this.state.input, incomingState);
        this.setState({
            input: newInput
        }, ()=>{
            EventProvider.triggerPopup("loader");
            this.submitRequest().then((res: IApiResponse)=>{
                if (res.success) {
                    window.location.href = "/";
                } else {
                    EventProvider.triggerPopup("popup", jsonBeautify(res));
                }
            }).catch((er: IApiResponse)=>{
                EventProvider.triggerPopup("popup", jsonBeautify(er));
            })
        });
    }

    render() {

        return (
            <form className="row align-items-center justify-content-center signup flex-column centered">
                {this.state.step > 1 ? (<div className="signup-back outer-text col-12" onClick={this.prevStep.bind(this)}>
                    <i className="fas fa-angle-left"></i> Step {this.state.step - 1}
                </div>) : <div className="signup-back invisible"><i className="fas fa-angle-left"></i></div>}
                <div className="col-12 panel limit"> 
                    { this.state.step == 1 ? <Page1 onNext={this.nextStep.bind(this)} values={this.state.input} /> 
                    : (this.state.step == 2 ? <Page2 onNext={this.nextStep.bind(this)} values={this.state.input}/>
                    : (this.state.step == 3 ? <Page3 onNext={this.submit.bind(this)} values={this.state.input}/> : ""))}
                </div>
                <div className="signup-counter outer-text">
                    {this.state.step} of {this.state.totalSteps}
                </div>
            </form>
        );
    }

    private submitRequest(): Promise<IApiResponse> {
        return new Promise((resolve, reject)=>{
            let request = new Request("/user/signup", {
                method: "POST",
                body: this.toFormData(this.state.input)
            });
            try {
                fetch(request).then((res)=>{
                    return res.json();
                }).then((res)=>{
                    resolve(res);
                })
            } catch(er) {
                reject(er);
            }
        });
    }

    private toFormData(json: any) {
        var form_data = new FormData();
        for ( var key in json ) {
            if (key == "adjustedProfilePicture") form_data.append(key, json[key], this.makeid() + ".jpg");
            if (key != "profilePicture" && key != "adjustedProfilePicture") form_data.append(key, json[key]);
        }
        return form_data;
    }

    private makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      
        for (var i = 0; i < 5; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return text;
    }
}

