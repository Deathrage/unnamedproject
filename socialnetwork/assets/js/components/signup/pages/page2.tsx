import * as React from "react";
import InterestSelector from "../../forms/interestselector";
import TextareaTracked from "../../forms/TextareaTracked";
import ProfilePicUploader from "../../forms/ProfilePicUploader";

export default class Page2 extends React.Component<{
    onNext: Function,
    values: any
}, {
    interests: Array<number>,
    description: string,
    profilePicture: File,
    errors: {
        interests: string,
        description: string,
        profilePicture: string
    }
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            interests: this.props.values.interests || [],
            description: this.props.values.description || "",
            profilePicture: this.props.values.profilePicture,
            errors: {
                interests: "",
                description: "",
                profilePicture: ""
            }
        }
    }
    private handleInterest(interests: Array<number>) {
        this.setState({interests: interests});
    }
    private handleTextarea(value: string) {
        this.setState({description: value});
    }
    private handlePicture(file: File) {
        this.setState({profilePicture: file});
    }
    private next(e: Event) {

        e.preventDefault();
        e.stopPropagation();

        if (this.validate()) {
            this.props.onNext({
                interests: this.state.interests,
                description: this.state.description,
                profilePicture: this.state.profilePicture
            });
        }
    }

    private validate() {
        let dupErr = Object.assign(this.state.errors, {});
        let valid = true;
        //interests
        if (this.state.interests.length > 2) {
            dupErr.interests = "";
        } else {
            dupErr.interests = "Choose at least three interests";
            valid = false;
        }
        this.setState({errors: dupErr});
        return valid;
    }

    render() {
        return (  
            <div>
                <div className="form-group ">
                    <label className="font-weight-bold">What are your interests?</label>
                    <InterestSelector onSelect={this.handleInterest.bind(this)} value={this.state.interests} valid={this.state.errors.interests.length > 0 ? false : true}/>
                    {this.state.errors.interests.length > 0 ? <span className="error">{this.state.errors.interests}</span> : ""}
                </div>
                <div className="form-group">
                    <label className="font-weight-bold">Tell us about yourself</label>
                    <TextareaTracked onInput={this.handleTextarea.bind(this)} limit={300} value={this.state.description} />
                </div>
                <div className="d-flex">
                    <ProfilePicUploader onUpload={this.handlePicture.bind(this)} value={this.state.profilePicture} />
                    <input type="button" className="btn btn-primary d-block ml-auto mr-1 align-self-start" onClick={this.next.bind(this) as any} value="Next"/>
                </div>
            </div>
        );
    }
}
