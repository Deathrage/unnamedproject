import * as React from "react";
import Hinter from "../../popups/hinter";
import Validations from "../../../services/Validations";
import DateInput from "../../forms/DateInput";

interface State {
    email: string;
    password: string;
    passwordConfirmation: string;
    fullname: string;
    username: string;
    birthdate: Date;
    valid: {
        email: string;
        password: string;
        passwordConf: string;
        fullname: string;
        username: string;
        birthdate: string;
    }
}

export default class Page1 extends React.Component<{
    onNext: Function,
    values: any
}, State> {
    msg: string;
    constructor(props: any) {
        super(props);
        this.state = {
            email: this.props.values.email || "",
            password: this.props.values.password || "",
            passwordConfirmation: this.props.values.password || "",
            fullname: this.props.values.fullname || "",
            username: this.props.values.username || "",
            birthdate: this.props.values.birthdate || new Date(),
            valid: {
                email: "",
                password: "",
                passwordConf: "",
                fullname: "",
                username: "",
                birthdate: ""
            }
        } as State;

        this.msg = `
            <b class="text-nowrap">Minimal password requirements:</b>
            <ul>
                <li>6 characters min length</li>
                <li>1 number</li>
                <li>1 capital letter</li>
                <li>1 special character</li>
            </ul>
        `;
    }


    private handleEmail(e: Event) {
        let target = e.currentTarget as HTMLInputElement;

        let val = target.value;
        this.validateEmail(val);

        this.setState({
            email: target.value
        })
    }
    private handlePassword(e: Event) {
        let target = e.currentTarget as HTMLInputElement;

        let val = target.value;
        this.validatePassword(val);

        this.setState({
            password: target.value
        })
    }
    private handlePasswordConfirmation(e: Event) {
        let target = e.currentTarget as HTMLInputElement;
        
        let val = target.value;
        this.validatePasswordConf(val);

        this.setState({
            passwordConfirmation: target.value
        })
    }
    private handleFullname(e: Event) {
        let target = e.currentTarget as HTMLInputElement;

        let val = target.value;
        this.validateFullname(val);

        this.setState({
            fullname: target.value
        })
    }
    private handleUsername(e: Event) {
        let target = e.currentTarget as HTMLInputElement;

        let val = target.value;
        this.validateUsername(val);

        this.setState({
            username: target.value
        })
    }
    private hanldeBirthdate(date: Date) {
        if (date != null) {
            let newValid = Object.assign(this.state.valid, {});
            newValid.birthdate = "";
            this.setState({
                birthdate: date,
                valid: newValid
            })
        }
    }

    private validateEmail(val: string) {
        return new Promise((resolve)=>{
            if (val.length > 0) {
                Validations.serverValidate("email", val).then((res)=>{
                    let newValid = Object.assign(this.state.valid, {});
                    if (res.success == false) {
                        newValid.email = res.message;
                    } else {
                        newValid.email = "";
                    }
                    this.setState({valid: newValid}, ()=>{
                        resolve();
                    });
                });
            } else {
                let newValid = Object.assign(this.state.valid, {});
                newValid.email = "Required field";
                this.setState({valid: newValid}, ()=>{
                    resolve();
                });
            }
        });
    }
    private validatePassword(val: string) {
        return new Promise((resolve)=>{
        if (val.length > 0) {

                Validations.serverValidate("password", val).then((res)=>{
                    let newValid = Object.assign(this.state.valid, {});
                    if (res.success == false) {
                        newValid.password = res.message;
                    } else {
                        newValid.password = "";
                    }
                    this.setState({valid: newValid}, resolve);
                });
            } else {
                let newValid = Object.assign(this.state.valid, {});
                newValid.password = "Required field";
                this.setState({valid: newValid}, resolve);
            }
        });
    }
    private validatePasswordConf(val: string) {
        return new Promise((resolve)=>{
            if (this.state.valid.password.length == 0) {
                if (!(val === this.state.password)) {
                    let newValid = Object.assign(this.state.valid, {});
                    newValid.passwordConf = "Passwords do not match";
                    this.setState({valid: newValid}, resolve);
                } else {
                    let newValid = Object.assign(this.state.valid, {});
                    newValid.passwordConf = "";
                    this.setState({valid: newValid}, resolve);
                }
            }
        });
    }
    private validateFullname(val: string) {
        return new Promise((resolve)=>{
            if (val.length > 0) {
                let newValid = Object.assign(this.state.valid, {});
                newValid.fullname = "";
                this.setState({valid: newValid}, resolve);
            } else {
                let newValid = Object.assign(this.state.valid, {});
                newValid.fullname = "Required field";
                this.setState({valid: newValid}, resolve);
            }
        });
    }
    private validateUsername(val: string) {
        return new Promise((resolve)=>{
            if (val.length > 0) {

                Validations.serverValidate("username", val).then((res)=>{
                    let newValid = Object.assign(this.state.valid, {});
                    if (res.success == false) {
                        newValid.username = res.message;
                    } else {
                        newValid.username = "";
                    }
                    this.setState({valid: newValid}, resolve);
                });
            } else {
                let newValid = Object.assign(this.state.valid, {});
                newValid.username = "Required field";
                this.setState({valid: newValid}, resolve);
            }
        });
    }
    private validateBirthdate(val: string) {
        return new Promise((resolve)=>{
            if (val.length > 0) {

                Validations.serverValidate("birthdate", val).then((res)=>{
                    let newValid = Object.assign(this.state.valid, {});
                    if (res.success == false) {
                        newValid.birthdate = res.message;
                    } else {
                        newValid.birthdate = "";
                    }
                    this.setState({valid: newValid}, resolve);
                });
            } else {
                let newValid = Object.assign(this.state.valid, {});
                newValid.birthdate = "Required field";
                this.setState({valid: newValid},resolve);
            }
        });
    }

    private next(e: Event) {

        e.preventDefault();
        e.stopPropagation();

        Promise.all([this.validateEmail(this.state.email),
        this.validatePassword(this.state.password),
        this.validatePasswordConf(this.state.passwordConfirmation),
        this.validateFullname(this.state.fullname),
        this.validateUsername(this.state.username),
        this.validateBirthdate(this.state.birthdate != null ? this.state.birthdate.toISOString() : "")]).then(()=>{
            let valid = this.state.valid.email.length == 0 &&  this.state.valid.birthdate.length == 0 &&  this.state.valid.fullname.length == 0 && this.state.valid.username.length == 0 &&  this.state.valid.password.length == 0;
            if (valid) {
                this.props.onNext({
                    email: this.state.email,
                    password: this.state.password,
                    fullname: this.state.fullname,
                    username: this.state.username,
                    birthdate: this.state.birthdate
                });
            }
        })
    }
    render() {
        return (  
            <div>
                <h1 className="text-center">Sign up</h1>  
                <table className="signup-table">
                    <tbody>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="email">Email</label>
                            </td>
                            <td>
                                <input type="text" name="email" className={"form-control" + (this.state.valid.email.length > 0 ? " has-error" : "")} placeholder="email" onBlur={this.handleEmail.bind(this)} defaultValue={this.state.email}/>
                                {this.state.valid.email.length > 0 ? <span className="error"> {this.state.valid.email} </span> : ""}
                            </td>
                        </tr>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="password">Choose Password</label>
                            </td>
                            <td className="position-relative">
                                <input type="password" name="password" className={"form-control" + (this.state.valid.password.length > 0 ? " has-error" : "")} placeholder="password" onBlur={this.handlePassword.bind(this)} defaultValue={this.state.password}/>
                                <Hinter msg={this.msg}/>
                            </td>
                        </tr>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="passwordconf">Confirm Password</label>
                            </td>
                            <td>
                                <input type="password" name="passwordconf" className={"form-control" + (this.state.valid.password.length > 0 || this.state.valid.passwordConf.length > 0 ? " has-error" : "")} placeholder="confirm passowrd" onBlur={this.handlePasswordConfirmation.bind(this)} defaultValue={this.state.passwordConfirmation}/>
                                {this.state.valid.password.length > 0 ? <span className="error"> {this.state.valid.password} </span> : ""}
                                {this.state.valid.passwordConf.length > 0 ? <span className="error"> {this.state.valid.passwordConf} </span> : ""}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div className="empty"></div></td>
                        </tr>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="fullname">Fullname</label>
                            </td>
                            <td>
                                <input type="text" name="fullname" className={"form-control" + (this.state.valid.fullname.length > 0 ? " has-error" : "")} placeholder="full name" onBlur={this.handleFullname.bind(this)} defaultValue={this.state.fullname}/>
                                {this.state.valid.fullname.length > 0 ? <span className="error"> {this.state.valid.fullname} </span> : ""}
                            </td>
                        </tr>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="username">Choose an Username</label>
                            </td>
                            <td>
                                <input type="text" name="username" className={"form-control" + (this.state.valid.username.length > 0 ? " has-error" : "")} placeholder="nickname" onBlur={this.handleUsername.bind(this)} defaultValue={this.state.username}/>
                                {this.state.valid.username.length > 0 ? <span className="error"> {this.state.valid.username} </span> : ""}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div className="empty"></div></td>
                        </tr>
                        <tr className="form-group">
                            <td>
                                <label htmlFor="birthdate">Birthdate</label>
                            </td>
                            <td>
                                <DateInput onChange={this.hanldeBirthdate.bind(this)} value={this.state.birthdate} error={this.state.valid.birthdate} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="button" className="btn btn-primary d-block ml-auto mr-2" onClick={this.next.bind(this) as any} value="Next"/>
            </div>
        );
    }
}
