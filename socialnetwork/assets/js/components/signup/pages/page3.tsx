import * as React from "react";
import ImageEditor from "../../forms/ImageEditor";
import TermsOfConditions from "../../forms/TermsOfConditions";

export default class Page3 extends React.Component<{
    onNext: Function,
    values: any
}, {
    adjustedProfilePicture: Blob,
    agreedToTerms: boolean,
    error: string
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            adjustedProfilePicture: null,
            agreedToTerms: false,
            error: ""
        } 
    }

    private onAgree(val: boolean) {
        this.setState({
            agreedToTerms: val
        })
    }

    private next() {
        if (this.validate()) {
            if (this.refs.imageEditor) {
                (this.refs.imageEditor as ImageEditor).exportCavnas().then((image: Blob)=>{
                    this.props.onNext({
                        adjustedProfilePicture: image
                    })
                });
            } else {
                this.props.onNext();
            }
        }
    }

    private validate():boolean {
        if (!this.state.agreedToTerms) {
            this.setState({
                error: "You need to agree with terms and condition"
            })
        }
        return this.state.agreedToTerms;
    }

    render() {
        return (  
            <div className="signup-sumup row">
                <div className="col-5">
                    <div className="image-editor-wrap">
                        {this.props.values.profilePicture instanceof File ? <ImageEditor ref="imageEditor" height={312} width={234} file={this.props.values.profilePicture} /> : 
                        <img src="/images/image-placeholder.png" className="image-placeholder"></img>}
                    </div>
                </div>
                <div className="col-7">
                    <h2 className="fullname mb-0 pb-0">{this.props.values.fullname}</h2>
                    <div className="text-weight-semibold">@{this.props.values.username}</div>
                    <br/>
                    <div className="text-weight-semibold">Email</div>        
                    <div className="text-weight-regular">{this.props.values.email}</div>
                    <br/>
                    <div className="text-weight-semibold">Interests</div>        
                    <div className="text-weight-regular">{this.props.values.interests instanceof Array ? this.props.values.interests.map((interest: Number, index: Number)=>{
                        if (window.app.categories) {
                            let category = window.app.categories.find((category: any)=>{
                                return category.id == interest;
                            })
                            return String(category.categoryName).toLocaleLowerCase()
                        }
                    }).join(", ") : ""}</div>
                    <br/>
                    <div className="text-weight-semibold">Bio</div>        
                    <div className="text-weight-regular word-break-all">{this.props.values.description}</div>
                </div>
                <div className="col-12 pt-5">
                    <TermsOfConditions checked={this.state.agreedToTerms} onChange={this.onAgree.bind(this)}/>
                    {this.state.error.length > 0 ? <span className="error text-left pl-3">{this.state.error}</span> : ""}
                    <input type="button" className="btn btn-primary d-block ml-auto mr-1 mt-5 align-self-start" onClick={this.next.bind(this)} value="Create profile"/>
                </div>
            </div>
        );
    }
}
