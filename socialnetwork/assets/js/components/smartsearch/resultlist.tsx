import * as React from "react";
import IPublicResult from "../../../../api/interfaces/responses/smartsearch/IPublicResult";
import UserResult from "./userresult";
import IPublicUserResult from "../../../../api/interfaces/responses/smartsearch/IPublicUserResult";
import PostResult from "./postresult";
import IPublicPostResult from "../../../../api/interfaces/responses/smartsearch/IPublicPostResult";

export default class ResultList extends React.Component<{
    results: IPublicResult[],
    phrase: string
},{

}> {
    constructor(props: any) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="search-results">
                {this.props.results != null ? (
                    this.props.results.length > 0 ? 
                        this.props.results.map((result)=>{
                            if (result.type == 'user') return <UserResult user={result as IPublicUserResult} key={(result as IPublicUserResult).id} />;
                            else if (result.type == 'post') return <PostResult post={result as IPublicPostResult} key={(result as IPublicPostResult).id} />;
                            else return null;
                        })
                    : <div className="post no-content ml-auto mr-auto">No reasults for phrase {this.props.phrase}.</div>
                ) : <div className="loader page"></div>}
            </div>
        );
    }
}