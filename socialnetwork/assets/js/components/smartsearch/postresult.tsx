import * as React from "react";
import IPublicPostResult from "../../../../api/interfaces/responses/smartsearch/IPublicPostResult";
import MediaHelper from "../../utils/MediaHelper";
import { Link } from "react-router-dom";

export default class PostResult extends React.Component<{
    post: IPublicPostResult
},any> {
    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <div className="search-result">
                <Link to={"/post/" + this.props.post.id} className="card search-result-post">
                    {this.props.post.attachedFile > 0 ?
                        <div className="card-img">
                            {this.props.post.mime.indexOf("image") > -1 ? 
                                <img src={this.props.post.attachedFileUrl} alt={this.props.post.name} />
                            : <div className="card-file" dangerouslySetInnerHTML={{__html: MediaHelper.getFileIcon(this.props.post.extension) }}></div>}
                        </div>
                    : ""}
                    <div className="card-body">
                        <div className="card-header">
                            <h5 className="card-title">{this.props.post.title}</h5>
                            <div className="comment-button">
                                <i className="far fa-comment"></i>
                                <span className="count">{this.props.post.commentCount || 0}</span>
                            </div>
                            <div className="like-button">
                                <i className="far fa-circle"></i>
                                <span className="count">{this.props.post.likeCount || 0}</span>
                            </div>
                        </div>
                        <p>{this.props.post.description.length > 50 ? this.props.post.description.substring(0, 50) + "..." : this.props.post.description}</p>
                    </div>
                </Link>
            </div>
        );
    }
}