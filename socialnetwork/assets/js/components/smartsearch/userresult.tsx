import * as React from "react";
import IPublicUserResult from "../../../../api/interfaces/responses/smartsearch/IPublicUserResult";
import FollowButton, { followState } from "../buttons/Follow";
import { Link } from "react-router-dom";

export default class UserResult extends React.Component<{
    user: IPublicUserResult
},{
    followers: number
}> {
    constructor(props:any) {
        super(props);
        this.state = {
            followers: this.props.user.followers
        }
    }

    private adjustFollowers(newstatus: number) {
        if (newstatus == followState.FOLLOWING) {
            this.props.user.followState = 1;
            this.setState({
                followers: Number(this.state.followers) + 1
            })
        } else if (newstatus == followState.NOT_FOLLOWING) {
            this.props.user.followState = 0;
            this.setState({
                followers: Number(this.state.followers) - 1
            })
        }
    }

    private showDetail(e: Event) {
        if ((e.target as HTMLElement).tagName == "BUTTON") {
            e.preventDefault();
        }
    }

    render() {
        return (
            <div className="search-result">
                <Link to={encodeURI("/profile/" + this.props.user.username)} onClick={this.showDetail.bind(this)} className="card search-result-user">
                    <div className="card-img">
                        <img src={this.props.user.profilePicture > 0 ? this.props.user.profilePictureUrl : "/images/image-placeholder.png"}/>
                    </div>
                    <div className="card-body">
                        <div className="card-header">
                            <h5 className="card-title">{this.props.user.fullName}</h5>
                            <p className="card-username">@{this.props.user.username}</p>
                            <p>{this.state.followers} followers</p>
                        </div>
                        <FollowButton userId={this.props.user.id} onChange={this.adjustFollowers.bind(this)} state={this.props.user.followState == 0 ? followState.NOT_FOLLOWING : followState.FOLLOWING} />
                    </div>
                </Link>
            </div>
        );
    }
}