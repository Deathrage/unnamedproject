import * as React from "react";
import Post from "./post";
import OrderBy, { orderEnum } from "./orderby";
import { IPublicPostInfo } from "../../../../api/interfaces/responses/IPublicPostInfo";
import PostInfo from "../../../../api/classes/PostInfo";
import EventProvider from "../../utils/EventProvider";
import CategorySelector from "./categoryselector";

export default class PostList extends React.Component<{
    fetchPosts: (selectSettings: {
        orderby: string,
        category?: number
    }, posts: any[], callback: (res: IPublicPostInfo[])=>void)=>void,

    categoryId?: number,
    orderable?: boolean,
    categorySelector?: boolean,
    defaultOrderby?: string
},{
    orderBy: any,
    posts: IPublicPostInfo[]
}> {
    private boundFunctions: any;
    private scrollLimit = 200;
    private postsAreFetched: boolean;
    constructor(props:any) {
        super(props);
        this.state = {
            orderBy: this.props.defaultOrderby || orderEnum.NEWEST,
            posts: null
        }
        this.boundFunctions = {
            scroll: this.onScroll.bind(this)
        }
        this.postsAreFetched = false;
    }

    componentDidMount() {
        if (window.components.postList) throw new Error("There can be only one post list component present at once");
        window.components.postList = this;
        this.fetchPosts();

        window.addEventListener("scroll", this.boundFunctions.scroll);
    }

    componentWillUnmount() {
        window.components.postList = null;

        window.removeEventListener("scroll", this.boundFunctions.scroll)
    }

    private onOrderChange(val: any) {
        this.setState({
            posts: [],
            orderBy: val
        })
    }

    componentDidUpdate() {
        if (window.innerHeight > this.documentHeight) {
            this.fetchPosts();
        }   
    }

    get documentHeight() {
        return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
    }
    get curScroll() {
        return (document.documentElement.scrollTop + window.innerHeight);
    }

    private onScroll(e: Event) {
        if ((this.curScroll + 200) > this.documentHeight) {
            if (this.postsAreFetched == false) {
                this.postsAreFetched = true;
                this.fetchPosts();
            }
        }
    }

    fetchPosts() {
        if (this.state.posts && (this.state.posts.length % 10) != 0) {
            this.postsAreFetched = false
            return;
        }
        this.props.fetchPosts({
            orderby: this.state.orderBy,
            category: this.props.categoryId
        }, this.state.posts, (res)=>{
            let postRes = this.state.posts || [];
            postRes = postRes.concat(res);
            this.setState({
                posts: postRes
            }, ()=>{
                this.postsAreFetched = false;
            })
        });
    }

    pushPost(post: IPublicPostInfo) {
        let postAr = this.state.posts;
        postAr.unshift(post);
        this.setState({
            posts: postAr
        })
    }

    render() {
        return (
            <div className="posts">
                <div className="d-flex justify-content-between" style={{
                    minHeight: "1.5rem"
                }}>
                    {this.props.orderable == true ?
                        <OrderBy onChange={this.onOrderChange.bind(this)} value={this.state.orderBy}/> 
                    : ""}
                    {this.props.categorySelector == true ?
                        <CategorySelector value={this.props.categoryId} />
                    : ""}
                </div>
                {this.state.posts == null ? 
                    <Post loader={true}/>
                : (this.state.posts.length > 0 ? this.state.posts.map((el)=>{
                    return <Post key={el.id} post={el}/>
                }) : 
                    <Post/>
                )}
            </div>
        );
    }
}

