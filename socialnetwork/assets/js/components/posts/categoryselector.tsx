import * as React from "react";
import { Link } from "react-router-dom";

export default class CategorySelector extends React.Component<{
    onChange?: Function,
    value?: any
},{
    openned: boolean,
    categories: Array<any>,
}> {
    private options: {
        value: any,
        label: any
    }[];
    private boundFunctions: any;
    constructor(props:any) {
        super(props);
        this.state = {
            openned: false,
            categories: []
        }
        this.boundFunctions = {
            handleClick: this.handleClickElseWhere.bind(this)
        }
    }

    componentDidMount() {
        document.addEventListener("click", this.boundFunctions.handleClick);
        if (this.state.categories.length == 0) {
            fetch("/categories/list")
            .then(res => res.json())
            .then(
              (result: any[]) => {
                result.unshift({
                    id: 0,
                    categoryName: "My feed"
                });
                this.setState({
                    categories: result
                });
              },
              (error) => {
                this.setState({
                    categories: []
                });
              }
            )
        }
    }

    private toggleOpen() {
        this.setState({
            openned: !this.state.openned
        })
    }

    private select(e: any) {
        this.props.onChange(e);
        this.toggleOpen();
    }

    private handleClickElseWhere(e: Event) {
        let tar = e.target as HTMLElement;
        if (!tar.classList.contains("list-filter-selected") && !tar.closest(".list-filter-selected")) {
            this.setState({
                openned: false
            });
        }
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.boundFunctions.handleClick);
    }


    render() {
        return (
            this.state.categories.length > 0 ?
                <div className="list-filter categoryselector reversed">
                    <div className="list-filter-selected" onClick={this.toggleOpen.bind(this)}>{this.props.value ? this.state.categories.find(x=> this.props.value == x.id).categoryName : this.state.categories[0].categoryName}<i className="fas fa-angle-down"></i></div>
                    {this.state.openned ? 
                        [
                        // <div key="category-curtain" className="popup-curtain" onClick={this.toggleOpen.bind(this)}></div>,
                        <div key="category-selector" className="list-filter-selector">
                            {this.state.categories.map((el)=>{
                                return (
                                    <Link onClick={this.toggleOpen.bind(this)} className="list-filter-selector-item" key={el.id} to={encodeURI(el.id == 0 ? "/" : "/category/" + el.categoryName)}>{el.categoryName}</Link>
                                );
                            })}
                        </div>    
                        ] 
                    : ""}
                </div>
            : <div></div>
        );
    }
}


export const orderEnum = {
    NEWEST: '\"createdAt\" DESC',
    ACTIVE: '\"createdAt\" DESC'
}