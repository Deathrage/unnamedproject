import * as React from "react";
import Post from "./post";

export default class OrderBy extends React.Component<{
    onChange?: Function,
    value?: any
},{
    openned: boolean
}> {
    private options: {
        value: any,
        label: any
    }[];
    private boundFunctions: any;
    constructor(props:any) {
        super(props);
        this.state = {
            openned: false
        }
        this.options = [
            {
                value: orderEnum.NEWEST, label: "most recent"
            },
            {
                value: orderEnum.ACTIVE, label: "most relevnat"
            }
        ];
        this.boundFunctions = {
            handleClick: this.handleClickElseWhere.bind(this)
        }
    }

    private handleClickElseWhere(e: Event) {
        let tar = e.target as HTMLElement;
        if (!tar.classList.contains("list-filter-selected") && !tar.closest(".list-filter-selected")) {
            this.setState({
                openned: false
            });
        }
    }

    componentDidMount() {
        document.addEventListener("click", this.boundFunctions.handleClick);
    }
    componentWillUnmount() {
        document.removeEventListener("click", this.boundFunctions.handleClick);
    }

    private toggleOpen() {
        this.setState({
            openned: !this.state.openned
        })
    }

    private select(e: any) {
        this.props.onChange(e);
        this.toggleOpen();
    }

    render() {
        return (
            <div className="list-filter orderby"> 
                <div className="list-filter-selected" onClick={this.toggleOpen.bind(this)}>{this.options.find(x => x.value == this.props.value).label}<i className="fas fa-angle-down"></i></div>
                {this.state.openned ? 
                    [
                    // <div key="orderby-curtain" className="popup-curtain" onClick={this.toggleOpen.bind(this)}></div>,
                    <div key="orderby-selector" className="list-filter-selector">
                        {this.options.map((el)=>{
                            return (
                                <div className="list-filter-selector-item" key={el.value} onClick={this.select.bind(this, el.value)}>{el.label}</div>
                            );
                        })}
                    </div>    
                    ] 
                : ""}
            </div>
        );
    }
}


export const orderEnum = {
    NEWEST: '\"createdAt\" DESC',
    ACTIVE: '\"id\" DESC'
}