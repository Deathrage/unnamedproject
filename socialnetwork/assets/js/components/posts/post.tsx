import * as React from "react";
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";
import { IPublicPostInfo } from "../../../../api/interfaces/responses/IPublicPostInfo";
import MediaHelper from "../../utils/MediaHelper";
import { Link, withRouter } from "react-router-dom";
import { IWithRouter } from "../../Interfaces/IWithRouter";
import Stylings from "../../utils/Stylings";
import LikeButton from "./likes/LikeButton";


interface IProps extends IWithRouter {
    loader?: boolean,
    post?: IPublicPostInfo
}

export class Post extends React.Component<IProps,{
    attachment: Blob,
    attachmentUrl: string
}> {
    constructor(props:any) {
        super(props);
    }

    private toProfileDetail(e: Event) {
        e.stopPropagation();
        if (this.props.post && this.props.post.username) {
            this.props.history.push("/profile/" + this.props.post.username);
            return false;
        }
    }

    private toPostDetail(e: Event) {
        e.stopPropagation();
        if (this.props.post && this.props.post.id && !(e.target as HTMLElement).classList.contains("card-file") && !(e.target as HTMLElement).closest(".card-file")) {
            this.props.history.push("/post/" + this.props.post.id); 
            return false;
        }
    }

    private toggleHover(e: Event) {
        let tar = (e.currentTarget as HTMLElement);
        if (tar.classList.contains("hover")) {
            tar.classList.remove("hover");
        } else {
            tar.classList.add("hover");
        }
    }

    private togglePostblockHover() {
        let tar = this.refs.postblock as HTMLElement;
        if (tar) {
            if (tar.classList.contains("hover")) {
                tar.classList.remove("hover");
            } else {
                tar.classList.add("hover");
            }
        }
    }

    render() {
        return (
            this.props.loader ? 
                <div className="card post loader"></div> :

                (this.props.post ? 
                    <div onClick={this.toPostDetail.bind(this)} ref="postblock" onMouseEnter={this.toggleHover.bind(this)} onMouseLeave={this.toggleHover.bind(this)} className="card post">
                        {this.props.post.attachedFile > 0 ?
                            <div className="card-img">
                                {this.props.post.mime.indexOf("image") > -1 ? 
                                    <img src={this.props.post.attachedFileUrl} alt={this.props.post.name} />
                                : <a download href={this.props.post.attachedFileUrl} onMouseEnter={this.togglePostblockHover.bind(this)} onMouseLeave={this.togglePostblockHover.bind(this)} className="card-file" dangerouslySetInnerHTML={{__html: MediaHelper.getFileIcon(this.props.post.extension) + "<div>" + this.props.post.name + "." + this.props.post.extension + "</div>"}}></a>}
                            </div>
                        : ""}
                        <div className="card-header row">
                            <div className="col-6">
                                <div onClick={this.toProfileDetail.bind(this)} onMouseEnter={this.togglePostblockHover.bind(this)} onMouseLeave={this.togglePostblockHover.bind(this)} className="cursor-pointer profile d-inline-flex">
                                    {this.props.post.profilePicture.length > 0 ?<div className="img"> <img src={this.props.post.profilePicture.split("/").pop() != "" ? this.props.post.profilePicture : "/images/image-placeholder.png"}/> </div>: ""} 
                                    <div className="info">
                                        <span className="text-weight-semibold username">@{this.props.post.username}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 d-flex justify-content-end flex-wrap">
                                <h5 className="card-title cursor-pointer">{this.props.post.title}</h5>
                                <div className="text-right card-date w-100">{Stylings.formatDate(new Date(Number(this.props.post.createdAt)))}</div>
                            </div>
                        </div>
                        <div className="card-body">
                            <p>{this.props.post.description}</p>
                        </div>
                        <div className="card-footer d-flex">
                            <div className="comment-button">
                                <i className="far fa-comment"></i>
                                <span className="count">{this.props.post.commentCount || 0}</span>
                            </div>
                            <div className="share-button">
                                <i className="fas fa-share"></i>
                            </div>
                            <LikeButton post={this.props.post}/>
                        </div>
                    </div> 
                : <div className="post no-content">So quiet in here.</div> )
        );
    }
}

export default withRouter(Post);