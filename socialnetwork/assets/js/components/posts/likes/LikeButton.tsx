import * as React from "react";
import { IPublicPostInfo } from "../../../../../api/interfaces/responses/IPublicPostInfo";

export default class LikeButton extends React.Component<{
    post: IPublicPostInfo,
    className?: string
}, {
    enabled: boolean
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled: false
        }
    }

    componentDidMount() {
        window.context.currentUser.then((curus)=>{
            if (curus.id != this.props.post.userId) {
                this.setState({
                    enabled: true
                })
            }
        }); 
    }

    private like() {
        if (!this.state.enabled) return;
    }

    render() {
        return (
            <div className={"like-button " + (this.state.enabled ? "enabled " : "") + (this.props.className || "")}>
                <span className="circle"></span>
                <span className="count">{this.props.post.likeCount || 0}</span>
            </div>
        )
    }
}