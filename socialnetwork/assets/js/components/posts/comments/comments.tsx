import * as React from "react";
import AddCommentR, {AddComment} from "./addcomment";
import CommentListR, {CommentList} from "./commentlist";
import { IApiResponse } from "../../../../../api/interfaces/IApiResponse";

export default class Comments extends React.Component<{},{
    openAdd: boolean;
    addDefaultText: string;
}> {
    private addComment: AddComment;
    private commentList: CommentList;
    private boundFuncntion: {
        handleNewComment: Function
    }
    constructor(props: any){
        super(props);
        
        this.state = {
            openAdd: false,
            addDefaultText: ""
        }

        this.boundFuncntion = {
            handleNewComment: this.handleNewComment.bind(this)
        }
    }

    componentDidMount() {
        io.socket.on("commentadd", this.boundFuncntion.handleNewComment);
    }
    componentWillUnmount() {
        io.socket.off("commentadd", this.boundFuncntion.handleNewComment);
    }

    toggleOpen(defaultText = "") { 
        this.setState({
            openAdd: !this.state.openAdd,
            addDefaultText: defaultText
        })
    }

    private handleNewComment(res: IApiResponse) {
        if (res.success) {
            this.setState({
                openAdd: false
            })
        }
    }

    render() {
        return (
            <div>
                {this.state.openAdd ? <AddCommentR defaultText={this.state.addDefaultText} className="card" parent={this}/> : ""}
                <CommentListR className="card" parent={this}/>
            </div>
        );
    }
}