import * as React from "react";
import Comments from "./comments";
import { withRouter } from "react-router";
import { IPublicComment } from "../../../../../api/interfaces/responses/IPublicComment";
import Comment from "./comment";
import { IApiResponse } from "../../../../../api/interfaces/IApiResponse";
import { IWithRouter } from "../../../Interfaces/IWithRouter";

interface IProps extends IWithRouter {
    className?: string,
    parent?: Comments
}

export class CommentList extends React.Component<IProps,{
    comments: IPublicComment[]
}> {
    private boundFunctions: {
        pushComment: Function,
        onScroll: any
    }
    private commentsAreBeingFetched: boolean;
    constructor(props: any){
        super(props);
        this.state = {
            comments: []
        }
        this.boundFunctions = {
            pushComment: this.pushComment.bind(this),
            onScroll: this.onScroll.bind(this)
        }
    }
    

    componentDidMount() {
        io.socket.on("commentadd", this.boundFunctions.pushComment);
        window.addEventListener("scroll", this.boundFunctions.onScroll);
        this.fetchComments();
    }
    componentWillUnmount() {
        window.removeEventListener("scroll", this.boundFunctions.onScroll)
        io.socket.off("commentadd", this.boundFunctions.pushComment);
    }
    
    componentDidUpdate() {
        if (window.innerHeight > this.documentHeight && this.commentsAreBeingFetched == false) {
            this.fetchComments();
        }   
    }

    private commentReply(username: string) {
        if (this.props.parent) {
            this.props.parent.toggleOpen("@" + username + " ");
        }
    }

    render() {
        return (
            <div className={"post-comments" + (this.state.comments.length > 0 ? " card" : "") + (this.props.className ? " " + this.props.className : "")}>
                {this.state.comments.length > 0 ? this.state.comments.map((comment)=>{
                    return comment != null ? <Comment key={comment.id} comment={comment} onReply={this.commentReply.bind(this)} /> : ""
                }) : ""}
            </div>
        );
    }

    private pushComment(res: IApiResponse) {
        if (res.success) {
            this.state.comments.unshift(res.data as IPublicComment)
            this.setState({
                comments:this.state.comments
            });
        }
    }

    private fetchComments() {
        if (this.state.comments.length > 0 && (this.state.comments.length % 10) != 0) {
            this.commentsAreBeingFetched = false
            return;
        }
        this.commentsAreBeingFetched = true;
        fetch("/comments/post/createdAt/10/"+ this.state.comments.length + "/" + Number((this.props.match as any).params["postid"])).then(res=>{
            res.json().then((json: IApiResponse)=>{ 
                if (json.success) {
                    let nAr = this.state.comments.concat(json.data as IPublicComment[]);
                    this.setState({
                        comments: nAr
                    }, ()=>{
                        this.commentsAreBeingFetched = false;
                    });
                }
            });
        });
    }

    get documentHeight() {
        return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
    }
    get curScroll() {
        return (document.documentElement.scrollTop + window.innerHeight);
    }

    private onScroll(e: Event) {
        if ((this.curScroll + 200) > this.documentHeight) {
            if (this.commentsAreBeingFetched == false) {
                this.commentsAreBeingFetched = true;
                this.fetchComments();
            }
        }
    }

}

export default withRouter(CommentList);