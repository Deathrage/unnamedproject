import * as React from "react";
import { IPublicComment } from "../../../../../api/interfaces/responses/IPublicComment";
import Stylings from "../../../utils/Stylings";
import { Link } from "react-router-dom";

export default class Comment extends React.Component<{
    comment: IPublicComment,
    onReply?: (username: string)=>void
},{}> {
    constructor(props: any) {
        super(props);
    }

    private doReply() {
        if (this.props.onReply) {
            let username = this.props.comment.username;
            this.props.onReply(username);
        }
    }

    render() {
        return (
            <div className="comment">
                <div className="d-flex">
                    {this.props.comment.profilePicture > 0 ? 
                        <div className="comment-image">
                            <img src={this.props.comment.profilePictureUrl} alt=""/>
                        </div>
                    : ""}
                    <div className="comment-body">
                        <div className="comment-head d-flex justify-content-between w-100">
                            <Link to={encodeURI("/profile/" + this.props.comment.username)} className="username">@{this.props.comment.username}</Link>
                            <span className="time">{Stylings.formatDate(new Date(Number(this.props.comment.createdAt)))}</span>
                        </div>
                        <p className="comment-text w-100">{
                            (()=>{
                                let matches = this.props.comment.text.match(/(@[^\s]+)/g);
                                if (!matches || matches.length == 0) return this.props.comment.text;
                                let split = this.props.comment.text.split(" ");
                                let ns = split.map((part, i)=>{
                                    if ((/(@[^\s]+)/g).test(part)) {
                                        return <Link className="user" to={`/profile/${part.replace("@", "")}`}>{part}</Link>
                                    } else {
                                        return <span>{(i == 0 ? "" : " ") + part + ((i+1) == split.length ? "" : " ")}</span>
                                    }
                                });
                                return ns;
                            })()
                        }</p>
                    </div>
                </div>
                <div className="comment-footer row w-100">
                    <div className="col-6">
                    </div>
                    <div className="col-3">
                        <div className="reply" onClick={this.doReply.bind(this)}>
                            <i className="fas fa-reply"></i>
                        </div>
                    </div>
                    <div className="col-3">

                    </div>
                </div>
                {this.props.comment.attachedFile > 0 ? 
                    <div className="comment-attachedimage">
                        <img src={this.props.comment.attachedFileUrl} alt=""/>
                    </div>
                : ""}
            </div>
        )
    }
}