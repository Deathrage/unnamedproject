import * as React from "react";
import Comments from "./comments";
import TextAreaTracked from "../../forms/TextareaTracked";
import { withRouter } from "react-router";
import { IWithRouter } from "../../../Interfaces/IWithRouter";
import RequestHelper from "../../../utils/RequestHelper";
import { SocketRequest } from "typed-sails";
import { ICommentCreate } from "../../../../../api/interfaces/requests/ICommentCreate";
import { IApiResponse } from "../../../../../api/interfaces/IApiResponse";
import FileUpload from "../../popups/popups/post/FileUpload";

interface IProps extends IWithRouter {
    className?: string,
    parent?: Comments,
    defaultText?: string
}

export class AddComment extends React.Component<IProps,{
    comment: string
    error: string,
    file: File
}> {
    private boundFuncntion: {
        handleNewComment: Function
    }
    constructor(props: any){
        super(props);
        this.state = {
            file: null,
            comment: "",
            error: null
        }
        this.boundFuncntion = {
            handleNewComment: this.handleNewComment.bind(this)
        }
    }

    componentDidMount() {
        io.socket.on("commentadd", this.boundFuncntion.handleNewComment);
    }
    componentWillUnmount() {
        io.socket.off("commentadd", this.boundFuncntion.handleNewComment);
    }

    private scanInput() {

    }

    private handleNewComment(res: IApiResponse) {
        if (!res.success) {
            this.setState({
                error: res.message
            })
        }
    }

    private handleFile(file: File) {
        this.setState({
            file: file
        });
    }

    render() {
        return (
            <div className={"post-comments-add" + (this.props.className ? " " + this.props.className : "")}>
                <TextAreaTracked acceptHtml={false} error={this.state.error} onInput={this.handleInput.bind(this)} value={this.state.comment ? this.state.comment : this.props.defaultText} limit={300} placeholder="Type your message" />
                <div className="d-flex flex-wrap align-items-center justify-content-between mt-3">
                    <FileUpload onUpload={this.handleFile.bind(this)} icoClass="fas fa-camera" buttonText="Add an image" />
                    <input type="submit" className="btn btn-primary ml-auto" value="reply" onClick={this.postComment.bind(this)}/>
                </div>
            </div>
        );
    }

    private handleInput(val: string) {
        this.setState({
            comment: val
        });
    }

    private postComment() {
        if (this.state.comment.length == 0) {
            this.setState({
                error: "Type in your opinion"
            })
            return;
        } else {
            this.setState({
                error: null
            })
        }
        let comment = {
            comment: this.state.comment,
            postId: Number((this.props.match as any).params["postid"])
        } as ICommentCreate;
        if (this.state.file) {
            comment.file = this.state.file;
            comment.fileMeta = {
                filename: this.state.file.name,
                mime: this.state.file.type,
                size: this.state.file.size
            }
        }
        RequestHelper.fetch({
            url: "/comments/create",
            method: "POST",
            data: comment
        } as SocketRequest).then((res)=>{
            
        });
    }
}

export default withRouter(AddComment);