import * as React from "react";
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";
import SuggestedBoard from "../infocards/SuggestedBoard";

export default class WhatToFollow extends React.Component<{
    className?: string
},{
    postList: any[]
}> {
    constructor(props:any) {
        super(props);
        this.state = {
            postList: []
        }
    }

    private parseList() {
        return this.state.postList.map((item)=>{
            return {
                id: 0,
                name: "POST",
                value: 0,
                image: null
            }
        });
    }

    render() {
        return (
            <SuggestedBoard headline="What to follow" className={this.props.className} items={this.parseList()}/>
        );
    }
}