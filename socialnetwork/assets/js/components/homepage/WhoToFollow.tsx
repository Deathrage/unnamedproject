import * as React from "react";
import IUserSuggest from "../../../../api/interfaces/responses/IUserSuggest";
import SuggestedBoard from "../infocards/SuggestedBoard";

export default class WhoToFollow extends React.Component<{
    className?: string
},{
    userList: IUserSuggest[]
}> {
    constructor(props:any) {
        super(props);
        this.state = {
            userList: []
        }
    }

    componentDidMount() {
        fetch("/user/follows/suggest").then(stream=>{
            stream.json().then(data=>{
                this.setState({
                    userList: data
                })
            });
        })
    }

    private parseList() {
        return this.state.userList.map((item)=>{
            return {
                id: item.id,
                name: item.username,
                value: item.followers,
                image: item.hasProfilePicture > 0 ? item.profilePictureUrl : "/images/image-placeholder.png",
                link: "/profile/" + item.username
            }
        });
    }

    private adjustFollowers(fols: number) {
        if (fols >= 10000) {
            fols = (10000 / 1000);
            fols = Math.floor(fols);
        }
        return String(fols) + "K";
    }


    render() {
        return (
            <SuggestedBoard headline="Who to follow" namePrefix="@" valueSuffix=" followers" className={this.props.className} items={this.parseList()}/>
        );
    }
}