import * as React from "react";
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";
import { IFollowStats } from "../../../../api/interfaces/responses/IFollowStats";
import { IPostStats } from "../../../../api/interfaces/responses/IPostStats";

export default class UserInfoBoard extends React.Component<{
    className?: string
},{
    username: string;
    balance?: number;
    followStats: IFollowStats;
    postStats: IPostStats;
}> {
    private boundFuncs: any;
    constructor(props:any) {
        super(props);
        this.state = {
            username: null,
            balance: null,
            followStats: null,
            postStats: null
        }
        this.boundFuncs = {
            balance: this.updateBalance.bind(this),
            followers: this.updateFolStats.bind(this),
            posts: this.updatePostStats.bind(this)
        }
    }

    componentDidMount() {
        window.context.currentUser.then((data)=>{
            this.setState({
                username: data.username
            });
        });
        window.context.balance.then((val: any)=>{
            this.setState({
                balance: val
            });
        });
        window.context.followStats.then((fols)=>{
            this.setState({
                followStats: fols
            });
        });
        window.context.postStats.then((stats)=>{
            this.setState({
                postStats: stats
            });
        });

        io.socket.on("folwupdatecur", this.boundFuncs.followers);
        io.socket.on("postupdatecur", this.boundFuncs.posts);
        io.socket.on("balancecur", this.boundFuncs.balance);
    }

    componentWillUnmount() {
        io.socket.off("folwupdatecur", this.boundFuncs.followers);
        io.socket.off("postupdatecur", this.boundFuncs.posts);
        io.socket.off("balancecur", this.boundFuncs.balance);
    }

    private updateBalance(balance: number) {
        this.setState({
            balance: balance
        });
    }
    private updatePostStats(stats: IPostStats) {
        this.setState({
            postStats: stats
        });
    }
    private updateFolStats(stats: IFollowStats) {
        this.setState({
            followStats: stats
        });
    }

    render() {
        return (
            // <div>
            //     {this.state.username && this.state.balance ? 
                    <div className={"user-info-board" + (this.props.className || "")}>
                        <div className="head-block d-block">
                            <span className="username">{this.state.username != null ? this.state.username : ""}</span>
                            <span className="balance">{this.state.balance != null ? this.state.balance : 0}</span>
                        </div>
                        {this.state.followStats != null ? 
                            <div className="block">
                                <div className="block-row">
                                    <span className="block-label">Following</span>
                                    <span className="block-value">{this.state.followStats.followings}</span>
                                </div>
                                <div className="block-row">
                                    <span className="block-label">Followers</span>
                                    <span className="block-value">{this.state.followStats.followers}</span>
                                </div>
                            </div>
                        : "" }
                        {this.state.postStats != null ? 
                            <div className="block">
                                <div className="block-row">
                                    <span className="block-label">Posts</span>
                                    <span className="block-value">{this.state.postStats.postCount}</span>
                                </div>
                                <div className="block-row">
                                    <span className="block-label">Likes</span>
                                    <span className="block-value">{this.state.postStats.likeSum || 0}</span>
                                </div>
                            </div>
                        : ""}
                    </div>
            //     : ""}
            // </div>
        );
    }
}