import * as React from "react";
import EventProvider from "../../utils/EventProvider";

export default class Logout extends React.Component<{
    className?: string;
},{}> {
    constructor(props: any) {
        super(props);
    }

    private logOut(e: Event) {
        EventProvider.triggerPopup("loader");
        let req = new Request("/user/signout", {
            method: "POST"
        });
        fetch(req).then((res)=>{
            res.json().then(json=>{
                if (json.success == true) {
                    window.location.href = "/";
                }
            })
        });
    }

    render() {
        return (
            <a className={this.props.className || ""} onClick={this.logOut.bind(this)}>{this.props.children}</a>
        )
    }
}