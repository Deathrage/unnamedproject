import * as React from "react";
import EventProvider from "../../utils/EventProvider";

export default class PostButton extends React.Component<{
    className?: string
}, any> {
    constructor(props:any){
        super(props);
    }

    private openPostPopup(e: Event) {
        e.preventDefault();
        EventProvider.triggerPopup("postpopup");
    }
    
    render() {
        return (
            <a className={this.props.className + " post"} onClick={this.openPostPopup.bind(this)}>
                <i className="fas fa-pencil-alt"></i>
            </a>
        );
    }
}