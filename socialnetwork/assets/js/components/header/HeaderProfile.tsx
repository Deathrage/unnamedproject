import * as React from "react";
import { Link } from 'react-router-dom'
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";
import { URL } from "../../ulrs";

export default class HeaderProfile extends React.Component<{
    className?: string
},{
    publicUser: IPublicUserInfo,
    balance: number
}> {
    constructor(props:any){
        super(props)
        this.state = {
            publicUser: null,
            balance: 0
        }
    }

    componentDidMount() {
        window.context.currentUser.then((user: IPublicUserInfo)=>{
            this.setState({
                publicUser: user
            })
        });
        window.context.balance.then((val: any)=>{
            this.setState({
                balance: val
            });
        });
    }
    
    render() {
        return (
            <div  className={"header-profile " + this.props.className || ""} >
                {this.state.publicUser != null ? 
                    <Link to={URL.PROFILE} className="header-profile-link d-inline-flex align-items-center text-decoration-none">
                        <div className="profile-picture">
                            {this.state.publicUser.profilePictureUrl && this.state.publicUser.profilePictureUrl.split("/").pop() != "" ? 
                                <img src={this.state.publicUser.profilePictureUrl} alt=""/>
                            : <img src="/images/image-placeholder.png" alt=""/>}
                        </div>
                        <div className="profile-detail">
                            <span className="nickname d-block text-nowrap">{this.state.publicUser.username}</span>
                            <span className="balance d-block text-nowrap">{this.state.balance}</span>
                        </div>
                    </Link>
                : ""}
            </div>
        );
    }
}