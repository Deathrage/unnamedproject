import * as React from "react";
import { Link } from "react-router-dom";
import Logout from "./Logout";

export default class Menu extends React.Component<{},{
    opened: boolean;
}> {
    private boundFunctions: any;
    constructor(props: any) {
        super(props);
        this.state = {
            opened: false
        }
        this.boundFunctions = {
            closeMenu: this.closeMenu.bind(this)
        }
    }

    private toggleOpen(e: Event) {
        this.setState({
            opened: !this.state.opened
        })
    }

    private closeMenu(e: Event) {
        if (!(this.refs.wrapper as HTMLElement).contains(e.target as HTMLElement)) {
            this.setState({
                opened: false
            })
        }
    }

    componentDidMount() {
        document.addEventListener("click", this.boundFunctions.closeMenu);
    }
    componentWillUnmount() {
        document.removeEventListener("click", this.boundFunctions.closeMenu);
    }

    render() {
        return(
            <div ref="wrapper" className="dropdown col-1 position-relative text-right">
                <div className="dropdown-toggle" onClick={this.toggleOpen.bind(this)}>
                    <i className="fas fa-bars"></i>
                </div>
                {this.state.opened == true ? 
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <Link to="/profile" className="dropdown-item">Profile</Link>
                        <Logout className="dropdown-item">Log out</Logout>
                    </div>
                : ""}
            </div>
        );
    }
}