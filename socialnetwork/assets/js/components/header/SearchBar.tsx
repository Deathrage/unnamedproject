import * as React from "react";
import { Redirect, withRouter } from "react-router";
import { History } from "history";
import { IWithRouter } from "../../Interfaces/IWithRouter";

export class SearchBar extends React.Component<IWithRouter, {
    phrase: string
}> {
    historyUnlisten: Function;
    constructor(props:any){
        super(props);
        this.state = {
            phrase: ""
        }
    }

    componentDidMount() {
        //window.app.searchBar = this;
        this.checkForPhrase();
        this.historyUnlisten = this.props.history.listen(this.checkForPhrase.bind(this));
    }

    private checkForPhrase() {
        if (window.location.pathname.indexOf('/search/') > -1) {
            this.setState({
                phrase: decodeURI(window.location.pathname.split("/").pop())
            });
        } else if (window.location.pathname.indexOf('/profile/') > -1) {
            this.setState({
                phrase: "@" + window.location.pathname.split("/").pop()
            });
        } else {
            this.setState({
                phrase: ""
            })
        }
    }

    componentWillUnmount() {
        if (this.historyUnlisten) {
            this.historyUnlisten();
            this.historyUnlisten = null;
        }
        //window.app.searchBar = null;
    }

    private onChange(e: React.FormEvent) {
        let target = e.target as HTMLInputElement;
        let val = target.value;
        this.setState({
            phrase: val
        })
    }

    private search() {
        let phrase = this.state.phrase.trim();
        if (phrase.indexOf("%") >= 0) {
           phrase = phrase.replace(/\%/, "");
        }
        if (phrase.length > 0) {
            if (phrase.indexOf("@") == 0) {
                (this.props.history as History).push("/profile/" + encodeURI(phrase.replace("@", "")));
            } else {
                (this.props.history as History).push("/search/" + encodeURI(phrase), {
                    clear: true
                });
            }
        }
    }

    private pressEnter(e: KeyboardEvent) {
        if (e.keyCode == 13) {
            this.search();
        }
    }
    
    render() {
        return (
            <div className="searchbar">
                <input type="text" value={this.state.phrase} placeholder="search" onChange={this.onChange.bind(this)} onKeyDown={this.pressEnter.bind(this)}/>
                <button className="searchbar-button" onClick={this.search.bind(this)}>
                    <i className="fas fa-search"></i>
                </button>
            </div>
        );
    }
}

export default withRouter(SearchBar);