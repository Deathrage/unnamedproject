import * as React from "react";
import FollowServices from "../../services/FollowServices";
import EventProvider from "../../utils/EventProvider";
import { SocketRequest } from "typed-sails";
import RequestHelper from "../../utils/RequestHelper";

export default class FollowButton extends React.Component<{
    className?: string,
    userId: number,
    state?: number,
    onChange?: (newstate: number)=>void
},{
    followState: number
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            followState: this.props.state || followState.UNSET
        }
    }

    componentDidMount() {
        if (!this.props.userId) return;
        if (this.props.state) return;
        this.checkState();
    }

    private checkState() {
        FollowServices.followState(this.props.userId).then(state=>{
            this.setState({
                followState: state
            });
        });
    }

    private changeFollow(e: Event) { //TODO: SOCKETS
        if (this.state.followState == followState.UNSET) return;
        let url = this.state.followState == followState.FOLLOWING ? "/follows/unfollow" : "/follows/follow";

        let req = {
            url: url,
            method: "POST",
            data : {
                userId: this.props.userId
            }
        } as SocketRequest;

        RequestHelper.fetch(req).then((state: number)=>{
            this.setState({
                followState: Number(state)
            }, ()=>{
                if (this.props.onChange) this.props.onChange(this.state.followState);
            });
        });
    }

    render() {
        return(
            <div className={this.props.className || ""}>
                <button className="btn btn-primary" onClick={this.changeFollow.bind(this)} disabled={this.state.followState == followState.UNSET}>
                    {this.state.followState == followState.FOLLOWING ? "Unfollow" : "Follow"}
                </button>
            </div>
        )
    }
}

export const followState = {
    FOLLOWING: 2,
    NOT_FOLLOWING: 1,
    UNSET: 0
}