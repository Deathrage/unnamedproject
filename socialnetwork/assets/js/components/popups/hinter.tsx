import * as React from "react";

export default class Hinter extends React.Component<{
    msg: string
}, any> {
    constructor(props: any) {
        super(props);
    }
    render() {
        let self = this;
        return (
            <div className="ml-auto hinter position-absolute">
                <i className="fas fa-question-circle"></i>
                <div className="message position-absolute text-left" dangerouslySetInnerHTML={{__html: self.props.msg}}></div>
            </div>
        );
    }
}