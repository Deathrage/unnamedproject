import * as React from "react";

export default class Terms extends React.Component<{
    close: Function
},{
    innertText: string;
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            innertText: null
        }
    }

    componentDidMount() {
        if (this.state.innertText == null) {
            fetch("/terms.txt")
            .then(res => res.text())
            .then(
              (result) => {
                this.setState({
                    innertText: result
                });
              }
            )
        }
    }


    private onClose() {
        this.props.close();
    }

    render() {
        return (
            <div className="popup terms">
                <div className="popup-header">
                    <h2 className="popup-header-title">Terms of use</h2>
                    <div className="popup-header-close" onClick={this.onClose.bind(this)}><i className="fas fa-times"></i></div>
                </div>
                <div className="popup-body loader">
                    {this.state.innertText != null ? this.state.innertText : ""}
                </div>
            </div>
        );
    }
}