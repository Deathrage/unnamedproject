import * as React from "react";

export default class Popup extends React.Component<{
    innertText: string
},{
    
}> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div className="popup terms">
                <div className="popup-body loader">
                    {this.props.innertText != null ? this.props.innertText : ""}
                </div>
            </div>
        );
    }
}