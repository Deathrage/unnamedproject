import * as React from "react";
import PostFileUpload from "./FileUpload";
import PostCategorySelector from "./CategorySelector";
import TextAreaTracked from "../../../forms/TextareaTracked";
import IPostCreate from "../../../../../../api/interfaces/requests/IPostCreate";
import { IApiResponse } from "../../../../../../api/interfaces/IApiResponse";
import EventProvider from "../../../../utils/EventProvider";
import PostList from "../../../posts/postlist";
import { IPublicPostInfo } from "../../../../../../api/interfaces/responses/IPublicPostInfo";
import { SocketRequest } from "typed-sails";
import RequestHelper from "../../../../utils/RequestHelper";

export default class PostPopup extends React.Component<{},{
    description: string,
    title: string,
    attachment: File,
    categories: number[],
    valid?: any,
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            description: "",
            title: "",
            attachment: null,
            categories: [],
            valid: new Object()
        }
    }

    private onDescription(val: string) {
        this.setState({
            description: val
        });
    }

    private onCategory(vals: any[]) {
        this.setState({
            categories: vals.map((el)=>{
                return el.value
            })
        })
    }

    private onTitle(e: Event) {
        let tar = e.currentTarget as HTMLInputElement;
        this.setState({
            title: tar.value
        })
    }

    private onFile(val: File) {
        this.setState({
            attachment: val
        })
    }

    private validate(): boolean {
        let valO = new Object() as any;
        let valid = true;
        if (this.state.title.length < 4) {
            valO["title"] = "Must be at least 4 characters long";
            valid = false;
        }
        if (this.state.description.length < 4) {
            valO["description"] = "Must be at least 4 characters long";
            valid = false;
        }
        if (this.state.categories.length < 1) {
            valO["category"] = "Choose at least 1 category";
            valid = false;
        }
        if (!valid) this.setState({valid: valO});
        return valid;
    }

    private onSubmit(e: Event) {
        if (!this.validate()) return;
        let body = new Object() as IPostCreate;
        body.title = this.state.title;
        body.description = this.state.description;
        body.categories = this.state.categories;
        body.attachment = this.state.attachment;
        if (body.attachment) {
            body.attachmentMeta = {
                mime: body.attachment.type,
                filename: body.attachment.name,
                size: body.attachment.size
            }
        }

        let req = {
            url: "/createpost",
            method: "POST",
            data: body
        } as SocketRequest;

        RequestHelper.fetch(req).then((response: IApiResponse)=>{
            if (!response.success) throw new Error(response.message);
            else this.finalise(response);
        }).catch(er=>{
            throw new Error(er);
        });

        EventProvider.triggerPopup("loader");
    }

    private finalise(response: IApiResponse) {
        EventProvider.triggerPopup("");
        if (window.components.postList instanceof PostList) {
            window.components.postList.pushPost(response.data as IPublicPostInfo);
        }
    }

    private buildFormData(body: any) {
        let fD = new FormData();
        for ( var key in body ) {
            fD.append(key, body[key]);
        }
        return fD;
    }

    render() {
        return (
            <div className="popup post-popup">
                <div className="form-group">
                    <TextAreaTracked value={this.state.description} error={this.state.valid["description"]} limit={300} onInput={this.onDescription.bind(this)} placeholder="What's the idea?"/>
                </div>
                <div className="form-group">
                    <input type="text" className={"form-control" + (this.state.valid["title"] && this.state.valid["title"].length > 0 ? " has-error" : "")} name="title" value={this.state.title} onChange={this.onTitle.bind(this)} placeholder="Give it a name"/>
                    {this.state.valid["title"] && this.state.valid["title"].length > 0 ? <div className="error">{this.state.valid["title"]}</div> : ""}
                </div>
                <div className="form-group d-flex flex-wrap">
                    <PostFileUpload icoClass="fas fa-camera" onUpload={this.onFile.bind(this)} listenToDrag={true} buttonText="Add an image" />
                    <PostCategorySelector error={this.state.valid["category"]} onChange={this.onCategory.bind(this)}/>
                    <div className="submit-button">
                        <input type="button" onClick={this.onSubmit.bind(this)} className="btn btn-reversed align-self-start" value="Post"/>
                    </div>
                </div>
            </div>
        );
    }
}