import * as React from "react";
import Select from "react-select";
import { Styles, StylesConfig } from "react-select/lib/styles";

export default class PostCategorySelector extends React.Component<{
    onChange?: (vals: any[]) => void,
    error?: string,
}, {
    categories: Object[]
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            categories: []
        }
    }

    private customStyles: StylesConfig = {
        option: (base, { data, isDisabled, isFocused, isSelected }) => ({
            ...base,
            backgroundColor: (isSelected || isFocused ? "#50b6e3" : "transparent"),
            color: (isSelected || isFocused ? "#fff" : "#000000"),
            cursor: "pointer",
            fontWeight: 400,
            fontSize: "1rem",
            paddingTop: 0,
            paddingBottom: 0
        }),
        control: (base, state) => ({
            ...base,
            height: "50px",
            //boxShadow: "0 1px 1px rgba(0, 0, 0, 0.5)",
            
            border: "1px solid #e4e4e4",
            backgroundColor: "#ffffff",
            color: "#000000",
            fontWeight: 400,
            fontSize: "1rem",
            borderRadius: 0
        }),
        placeholder: (base, state) => ({
            ...base,
            color: "#50b6e3"
        }),
        menuList: (base, state) => ({
            ...base,
            backgroundColor: "#ffffff",
            //boxShadow: "0 1px 1px rgba(0, 0, 0, 0.5)",
            border: "1px solid #e4e4e4"
        }),
        dropdownIndicator: (base, state) => ({
            ...base
        }),
        indicatorSeparator: (base, state) => ({

        }),
        multiValue: (base, state) => ({
            ...base,
            backgroundColor: "#50b6e3"
        }),
        multiValueLabel: (base, { data }) => ({
            ...base,
            color: "#ffffff",
        }),
        multiValueRemove: (base, { data }) => ({
            ...base,
            color: "#ffffff",
            cursor: "pointer",
            ':hover': {
                backgroundColor: "#3880a0",
            }
        }),
        clearIndicator: (base) => ({
            ...base,
            cursor: "pointer"
        }),
        valueContainer: (base) => ({
            ...base,
            flexWrap: "nowrap",
            
        })
    }

    componentDidMount() {
        let req = new Request("/categories/list");
        fetch(req).then((data) => {
            data.json().then(json => {
                let opts = (json as any[]).map((el) => {
                    let o = new Object() as Option;
                    o.label = el.categoryName;
                    o.value = el.id;
                    return o;
                });
                this.setState({
                    categories: opts
                });
            })
        })
    }

    private handleChange(newValue: any, actionMeta: any) {
        if (this.props.onChange) {
            this.props.onChange(newValue);
        }
    }

    render() {
        return (
            <div className="category-selector">
                <Select placeholder="Select a cetegory" styles={this.customStyles} options={this.state.categories} onChange={this.handleChange.bind(this)} isMulti />
                {this.props.error && this.props.error.length > 0 ? <div className="error">{this.props.error}</div> : ""}
            </div>
        );
    }
}

interface Option {
    value: number,
    label: string
}