import * as React from "react";
import MediaHelper, { icoEnums } from "../../../../utils/MediaHelper";

export default class FileUpload extends React.Component<{
    onUpload?: (file: File) => void,
    icoClass?: string,
    buttonText?: string,
    listenToDrag?: boolean
},{
    fileUrl: string,
    file: File,
    message: string,
    dragged: boolean,
    error: string
}> {
    _boundFunctions: any;
    constructor(props: any) {
        super(props);
        this.state = {
            fileUrl: null,
            file: null,
            message: "drop file here",
            dragged: false,
            error: null
        }
        this._boundFunctions = {
            startDragOver: this.startDragOver.bind(this),
            endDragOver: this.endDragOver.bind(this)
        }
    }

    componentDidMount() {
        if (this.props.listenToDrag) {
            window.addEventListener("dragover", this._boundFunctions.startDragOver);
            window.addEventListener("drop", this._boundFunctions.endDragOver);
        } 
    }

    componentWillUnmount() {
        if (this.state.fileUrl) window.URL.revokeObjectURL(this.state.fileUrl);
        if (this.props.listenToDrag) {
            window.removeEventListener("dragover", this._boundFunctions.startDragOver);
            window.removeEventListener("drop", this._boundFunctions.endDragOver);
        }
    }

    private onUpload(e: Event) {
        let target = e.currentTarget as HTMLInputElement;
        if (target.files.length == 0) return;
        let file = target.files[0];
        this.update(file);
    }

    private changeFile() {
        (this.refs.file as HTMLInputElement).click();
    }

    private dropFile(e: DragEvent) {
        e.preventDefault();

        if (e.dataTransfer.items) {
            var file = e.dataTransfer.items[0].getAsFile();
            this.update(file);
        } else {
            file = (e.dataTransfer.items[0] as any as File);
            this.update(file);
        }

        if (e.dataTransfer.items) {
            e.dataTransfer.items.clear();
        } else {
            e.dataTransfer.clearData();
        }
    }

    private update(file: File) {
        if (this.state.fileUrl) window.URL.revokeObjectURL(this.state.fileUrl);
        
        if ((file.size / (1024*1024)) > 2) {
            let error = "File must be under 2MB";
            this.setState({
                file: null,
                error: error
            }, ()=>{
                if (this.props.onUpload) this.props.onUpload(null);
            })
        } else if (file.type.indexOf("image/") < 0) {
            let error = "Only images are accepted";
            this.setState({
                file: null,
                error: error
            }, ()=>{
                if (this.props.onUpload) this.props.onUpload(null);
            })
        }else {
            var url = window.URL.createObjectURL(file);
            this.setState({
                error: null,
                file: file,
                fileUrl: url
            }, ()=>{
                if (this.props.onUpload) this.props.onUpload(this.state.file);
            })
        }
    }

    private startDragOver(e: DragEvent) {
        e.preventDefault();
        this.setState({
            dragged: true
        });
    }

    private endDragOver(e: DragEvent) {
        e.preventDefault();
        this.setState({
            dragged: false
        });
    }

    render() {
        return (
            <div className={"file-upload" + (this.state.file || this.state.dragged ? " opened" : "")}>
                <div className="label position-relative w-100 text-center">
                    <input className="file-selector" ref="file" type="file" onChange={this.onUpload.bind(this)}/>
                    {this.props.icoClass ? <i className={this.props.icoClass}></i> : ""}<span>{this.props.buttonText || "Add an image"}</span>
                </div>
                {this.state.error && this.state.error.length > 0 ? <div className="error">{this.state.error}</div> : ""}
                {this.state.file || this.state.dragged ?
                    <div className="preview" onDrop={this.dropFile.bind(this)} onClick={this.changeFile.bind(this)}>
                        {this.state.dragged ? this.state.message : 
                            (this.state.file ? 
                                (this.state.file.type.indexOf("image/") > -1? 
                                    <img src={this.state.fileUrl} alt={this.state.file.name}/> 
                                : <span dangerouslySetInnerHTML={{__html: MediaHelper.getFileIcon(this.state.file) + "<div>" + this.state.file.name + "</div>"}}></span>)  
                            : "")
                        }
                    </div>
                : ""}
            </div>
        );
    }
}