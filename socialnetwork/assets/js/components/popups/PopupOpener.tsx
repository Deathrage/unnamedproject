import * as React from "react";
import EventProvider from "../../utils/EventProvider";

export default class PopupOpener extends React.Component<{
    innerMarkup: any,
    popupId: string,
    className?: string
},{}> {
    constructor(props: any) {
        super(props);


    }

    private openPopup() {
        EventProvider.triggerPopup(this.props.popupId);
    }

    render() {
        return (
            <span className={this.props.className} onClick={this.openPopup.bind(this)}>{this.props.innerMarkup}</span>
        );
    }
}