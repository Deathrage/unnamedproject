import * as React from "react";
import Terms from "./popups/Terms";
import Loader from "./popups/Loader";
import Popup from "./popups/Popup";
import PostPopup from "./popups/post/Post";

export default class PopupManager extends React.Component<{
    
},{
    currentPopupId: string,
    innerText: string
}> {
    popupCollection: Array<any>
    constructor(props: any) {
        super(props);
        this.state = {
            currentPopupId: null,
            innerText: null
        }

        this.popupCollection = [
            <Terms close={this.closePopup.bind(this)}/>,
            <Loader/>,
            <Popup innertText={this.state.innerText}/>,
            <PostPopup/>
        ];

        document.addEventListener("wopener",this.onOpenPopup.bind(this));
    }

    private onOpenPopup(e: CustomEvent) {
        document.body.removeAttribute("style");
        if (e.detail instanceof Object) {
            this.setState({
                currentPopupId: e.detail.id,
                innerText: e.detail.innertText
            });
        } else {
            this.setState({currentPopupId: e.detail});
        }
    }

    private closePopup(e: Event) {
        if ((e.target as HTMLElement).classList.contains("popup-curtain")) {
            this.setState({currentPopupId: null});
            document.body.removeAttribute("style");
        }
    }

    private filterPopup() {
        let popup = this.popupCollection.find((el: React.ReactElement<any>)=>{
            let clas = el.type as any;
            return String(clas.name).toLowerCase() === String(this.state.currentPopupId).toLowerCase()
        });
        if (popup == null) {
            document.body.removeAttribute("style");
        } else {
            let scrollT = document.documentElement.scrollTop;
            document.body.setAttribute("style", `
                overflow: hidden;
                position: fixed;
                top: ${-scrollT}px;
            `);
        }
        return popup;
    }

    render() {
        return (
            <div className="popups">
                {this.filterPopup() != null ? 
                    <div className="popup-curtain" onClick={this.state.currentPopupId != "loader" ? this.closePopup.bind(this) : null}>
                        {this.filterPopup()}
                    </div>    
                : ""}
            </div>
        );
    }
}