import * as React from "react";
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";

export default class ProfileDetail extends React.Component<{
    className?: string,
    userInfo?: IPublicUserInfo
},{
}> {
    private listeners: Array<symbol>;
    constructor(props:any) {
        super(props);
    }

    componentDidMount() {
       
    }

    componentWillUnmount() {

    }

    render() {
        return (
            this.props.userInfo != null ?
                <div className={"profile-detail " + (this.props.className || "")}>
                    <div className="block">
                        <div className="profile-picture">
                            <img src={this.props.userInfo.profilePictureUrl && this.props.userInfo.profilePictureUrl.split("/").pop() != "" ? this.props.userInfo.profilePictureUrl : "/images/image-placeholder.png"} alt=""/>
                        </div>
                        <h5 className="fullname">{this.props.userInfo.username}</h5>
                    </div>
                    {this.props.userInfo.description && this.props.userInfo.description.length > 0 ?
                        <div className="block">
                            <p className="p-0 m-0 description">{this.props.userInfo.description}</p>
                        </div>
                    : ""}
                </div>
            : <div></div>
        );
    }
}