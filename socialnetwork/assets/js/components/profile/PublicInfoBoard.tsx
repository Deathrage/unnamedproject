import * as React from "react";
import IPublicUserInfo from "../../../../api/interfaces/responses/IPublicUserInfo";
import { IFollowStats } from "../../../../api/interfaces/responses/IFollowStats";
import { IPostStats } from "../../../../api/interfaces/responses/IPostStats";
import FollowButton from "../buttons/Follow";

export default class PublicInfoBoard extends React.Component<{
    className?: string,
    userInfo?: IPublicUserInfo,
    followStats?: IFollowStats,
    postStats?: IPostStats
},{}> {
    private listeners: Array<symbol>;
    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <div className={"user-info-board" + (this.props.className || "")}>
                <div className="head-block d-block">
                    {this.props.userInfo != null ? 
                        <span className="username pr-5">{this.props.userInfo.username != null ? this.props.userInfo.username : ""}</span>
                    : ""}
                </div>
                {this.props.followStats != null ? 
                    <div className="block">
                        <div className="block-row">
                            <span className="block-label">Following</span>
                            <span className="block-value">{this.props.followStats.followings}</span>
                        </div>
                        <div className="block-row">
                            <span className="block-label">Followers</span>
                            <span className="block-value">{this.props.followStats.followers}</span>
                        </div>
                    </div>
                : "" }
                {this.props.postStats != null ? 
                    <div className="block">
                        <div className="block-row">
                            <span className="block-label">Posts</span>
                            <span className="block-value">{this.props.postStats.postCount}</span>
                        </div>
                        <div className="block-row">
                            <span className="block-label">Likes</span>
                            <span className="block-value">{this.props.postStats.likeSum || 0}</span>
                        </div>
                    </div>
                : ""}
                {this.props.userInfo != null ? 
                    <FollowButton className="profile-detail unrestricted" userId={this.props.userInfo.id} />
                : ""}
            </div>
        );
    }
}