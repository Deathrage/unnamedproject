import * as React from "react";
import EventProvider from "../../utils/EventProvider";
import { IApiResponse } from "../../../../api/interfaces/IApiResponse";

export default class Signin extends React.Component<{}, {
    valid: boolean,
    message: string
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            valid: true,
            message: ""
        }
    }

    private onSubmit(e: React.FormEvent) {
        e.preventDefault();
        EventProvider.triggerPopup("loader");
        let form = this.refs.login as HTMLFormElement;
        if (!form) return;
        let formData = new FormData(form);
        this.tryLogin(formData).then((res: IApiResponse) => {
            if (res.success == true) {
                window.location.href = "/";
            } else {
                this.setState({
                    valid: false,
                    message: res.message
                })
            }
        }).catch(()=>{
            EventProvider.triggerPopup("");
        });;
    }

    private onLostPassword(e: Event) {
        e.preventDefault();
        EventProvider.triggerPopup("lostpassword");
    }

    private tryLogin(formData: any) {
        return new Promise((resolve, reject) => {
            let request = new Request("/user/signin", {
                method: "POST",
                body: formData
            })
            fetch(request).then((res) => {
                if (res.status == 200) {
                    res.json().then(resDjson => {
                        resolve(resDjson);
                    });
                } else if (res.status == 404) {
                    this.setState({
                        valid: false,
                        message: "Invalid email or password"
                    })
                    reject();
                } else {
                    this.setState({
                        valid: false,
                        message: "Server error occured, please try again"
                    })
                    reject();
                }
            }).catch((reason)=>{
                console.log(reason);
            });
        });
    }

    render() {
        return (
            <form ref="login" className="row align-items-center justify-content-center signin flex-column centered" onSubmit={this.onSubmit.bind(this)}>
                <div className="col-12 panel">
                    <h1>Sign in</h1>
                    <div className="row">
                        <div className="col-6 inner-box alter-signin flex-direciton-column flex-wrap justify-content-center d-flex">
                            <a href="" className="alter-signin-button fb"><i className="fab fa-facebook-f"></i><span>Login with Facebook</span></a>
                            <a href="" className="alter-signin-button twitter"><i className="fab fa-twitter"></i><span>Login with Twitter</span></a>
                            <a href="" className="alter-signin-button google"><i className="fab fa-google"></i><span>Login with Google</span></a>
                        </div>
                        <div className="col-6 inner-box">
                            <div className="form-group">
                                <input type="text" name="username" className={"form-control" + (this.state.valid == false ? " has-error" : "")} placeholder="Email" />
                                <input type="password" name="password" className={"form-control" + (this.state.valid == false ? " has-error" : "")} placeholder="Password" />
                                {this.state.valid == false ? <span className="error">{this.state.message}</span> : ""}
                            </div>
                            <div className="d-flex align-items-center">
                                <a href="" className="text-muted font-italic" onClick={this.onLostPassword.bind(this)}>Forgot your password?</a>
                                <input type="submit" className="btn btn-primary d-inline-block ml-auto" value="sign in" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}