import * as React from "react";

export default class InterestSelector extends React.Component<{
    onSelect: Function,
    value: any,
    valid?: boolean,
    errorMessage?: string
}, {
    fetched: Boolean,
    categories: Array<Object>,
    selectedValues: Array<number>
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            fetched: false,
            categories: [],
            selectedValues: this.props.value || []
        }
    }

    componentDidMount() {
        if (this.state.fetched == false) {
            fetch("/categories/list")
            .then(res => res.json())
            .then(
              (result) => {
                if (!window.app) window.app = new Object();
                window.app.categories = result;
                this.setState({
                    fetched: true,
                    categories: result
                });
              },
              (error) => {
                throw new Error(error);
                this.setState({
                    fetched: false
                });
              }
            )
        }
    }

    private handleSelect(id: number) {
        let newValues = this.state.selectedValues;
        let index = newValues.indexOf(id);

        if (index > -1) {
            newValues.splice(index, 1);
        } else {
            newValues.push(id);
        }

        this.setState({
            selectedValues: newValues
        }, ()=>{
            this.props.onSelect(this.state.selectedValues);
        });
    }
  
    render() {
        return (
            <div className="row interest-selector">
                {this.state.fetched == true ? 
                    this.state.categories.map((item: any, index)=>{
                        return (
                            <div className="col-4 interest-box" key={item.id}>
                                <div className={"interest" + (this.state.selectedValues.indexOf(item.id) > -1 ? " selected" : "") + (this.props.valid == false ? " has-error" : "")} onClick={this.handleSelect.bind(this, item.id)}>
                                    <i className={"fas " + item.iconClass}></i>
                                    <span className="d-block interest-label">{String(item.categoryName).toLocaleLowerCase()}</span>
                                </div>
                            </div>
                        )
                    })
                : "" }
            </div>
        );
    }
}