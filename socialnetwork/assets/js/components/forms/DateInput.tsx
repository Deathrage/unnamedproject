import * as React from "react";

export default class DateInput extends React.Component<{
    error?: string,
    value?: Date,
    onChange?: (value: Date)=>void,
    className?: string
},{
    //value: Date,
    day: number,
    month: number,
    year: number
}> {
    value: Date;
    focused: boolean;
    timer: any;
    constructor(props: any) {
        super(props);
        this.focused = false;
        this.value = this.props.value || new Date();
        this.state = {
            day: this.value.getDate(),
            month: this.value.getMonth() + 1,
            year: this.value.getFullYear()
        }
    }

    // private onFocus() {
    //     this.focused = true;
    //     clearTimeout(this.timer);
    // }

    // private onBlur() {
    //     this.focused = false;
    //     this.timer = setTimeout(()=>{
    //         let value = new Date(this.state.year, this.state.month - 1, this.state.day);
    //         if (this.props.onBlur) {
    //             this.props.onBlur(value);
    //         }
    //     }, 500);
    // }

    private onDay(e: Event) {
        let day = Number((e.target as HTMLInputElement).value);
        if (isNaN(day) || day < 1) day = 1;
        if (day > 31) day = 31;
        this.setState({
            day: day
        }, ()=>{
            if (this.props.onChange) {
                let value = new Date(this.state.year, this.state.month - 1, this.state.day);
                this.props.onChange(value);
            }
        });
    }
    private onMonth(e: Event) {
        let month = Number((e.target as HTMLInputElement).value);
        if (isNaN(month) || month < 1) month = 1;
        if (month > 12) month = 12;
        this.setState({
            month: month
        }, ()=>{
            if (this.props.onChange) {
                let value = new Date(this.state.year, this.state.month - 1, this.state.day);
                this.props.onChange(value);
            }
        });
    }
    private onYear(e: Event) {
        let year = Number((e.target as HTMLInputElement).value);
        if (isNaN(year) || year < 1) year = 1;
        this.setState({
            year: year
        }, ()=>{
            if (this.props.onChange) {
                let value = new Date(this.state.year, this.state.month - 1, this.state.day);
                this.props.onChange(value);
            }
        });
    }
    
    render() {
        return (
            <div className={this.props.className || ""}>
                <div className="input-date-row">
                    <input type="number" name="day" onChange={this.onDay.bind(this)} value={this.state.day}   className={"form-control" + (this.props.error.length > 0 ? " has-error" : "")} placeholder="dd" />
                    <input type="number" name="month" onChange={this.onMonth.bind(this)} value={this.state.month}  className={"form-control" + (this.props.error.length > 0 ? " has-error" : "")} placeholder="MM" />
                    <input type="number" name="year" onChange={this.onYear.bind(this)} value={this.state.year} className={"form-control" + (this.props.error.length > 0 ? " has-error" : "")} placeholder="yyyy" />
                </div>
                {this.props.error.length > 0 ? <span className="error"> {this.props.error} </span> : ""}
            </div>
        );
    }
}