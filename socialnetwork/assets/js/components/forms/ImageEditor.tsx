import * as React from "react";

export default class ImageEditor extends React.Component<{
    width: Number;
    height: Number;
    file: File;
}, {
    imageUrl: string
}> {
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    image: HTMLImageElement;
    editorData: {
        x: number,
        y: number,
        h: number,
        w: number,
        lastX: number,
        lastY: number,
        zoomQ: 1
    };
    editorMode: Boolean;
    grabed: Boolean;

    constructor(props: any) {
        super(props);
        this.state = {
            imageUrl: ""
        }

        this.ctx = null;
        this.image = new Image();
        this.editorData = {
            x: 0,
            y: 0,
            h: 0,
            w: 0,
            lastX: 0,
            lastY: 0,
            zoomQ: 1
        }
        this.editorMode = false;
        this.grabed = false;
    }

    componentDidUpdate() {
        if (this.state.imageUrl.length > 0) {
            this.updateCanvas();
            if (this.editorMode == false) {
                this.fitImage();
            }
            this.draw();
        }
    }

    componentDidMount() {
        this.loadImage();
    }

    public exportCavnas(): Promise<Blob> {
        return new Promise((resolve)=>{
            if (this.canvas) {
                this.canvas.toBlob((result:Blob)=>{
                    resolve(result);
                });
            } else {
                resolve(null);
            }
        })
    }

    render() {
        return (
            <div className="image-editor position-absolute">
                { this.state.imageUrl.length > 0 ?
                <div style={{height: "100%", width: "100%"}}>
                    <canvas ref="canvas" onMouseDown={this.onMouseDown.bind(this)} onMouseLeave={this.onMouseUp.bind(this)} onMouseMove={this.onMouseMove.bind(this)} onMouseUp={this.onMouseUp.bind(this)} width={String(this.props.width)} height={String(this.props.height)}></canvas>
                    <div className="image-editor-controls">
                        <div className="image-editor-control" onClick={this.zoomIn.bind(this)}>
                            <i className="fas fa-search-plus"></i>
                        </div>
                        <div className="image-editor-control" onClick={this.zoomOut.bind(this)}>
                            <i className="fas fa-search-minus"></i>
                        </div>
                    </div>
                </div>
                : "" }
            </div>
        );
    }

    private zoomIn() {
        this.editorData.zoomQ += 0.1;
        this.draw();
    }

    private zoomOut() {
        if (this.editorData.zoomQ > 1) {
            this.editorData.zoomQ -= 0.1;
            this.correctPan();
            this.draw();
        }
    }

    private correctPan() {
        let newH = this.editorData.h * this.editorData.zoomQ;
        let newW = this.editorData.w * this.editorData.zoomQ;

        if ((this.editorData.x + newW) <= this.canvas.width) {
            let cor = this.canvas.width - (this.editorData.x + newW);
            this.editorData.x += cor;
        }
        if ((this.editorData.y + newH) <= this.canvas.height ) {
            let cor = this.canvas.height - (this.editorData.y + newH);
            this.editorData.y += cor;
        }

    }

    private loadImage() {
        let url = window.URL.createObjectURL(this.props.file);
        this.image.onload = () => {
            this.setState({imageUrl: url});
        }
        this.image.src = url;
    }

    private fitImage() {
        if (this.editorData.zoomQ == 1) {
            let iW = this.image.naturalWidth;
            let iH = this.image.naturalHeight;
            let wRatio = iW / this.canvas.width;
            let hRatio = iH / this.canvas.height;
    
            let tRatio = Math.min(wRatio, hRatio);
    
            this.editorData.h = (iH / tRatio);
            this.editorData.w = (iW / tRatio);
            this.editorData.x = (-(this.editorData.w / 2) + (this.canvas.width / 2));
            this.editorData.y = (-(this.editorData.h / 2) + (this.canvas.height / 2 ));
        }
    }

    private updateCanvas() {
        this.canvas = this.refs.canvas as HTMLCanvasElement;
        this.ctx = this.canvas.getContext("2d");
    }

    private draw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.drawImage(this.image, this.editorData.x, this.editorData.y, this.editorData.w  * this.editorData.zoomQ, this.editorData.h  * this.editorData.zoomQ);
    }

    private onMouseDown(e: React.MouseEvent<HTMLCanvasElement>) {
        e.preventDefault();
        this.grabed = true;
        this.editorData.lastX = e.clientX;
        this.editorData.lastY = e.clientY;
    }
    private onMouseUp(e: React.MouseEvent<HTMLCanvasElement>) {
        e.preventDefault();
        this.grabed = false;
    }
    private onMouseMove(e: React.MouseEvent<HTMLCanvasElement>) {
        e.preventDefault();
        if (this.grabed) {
            let distanceX = e.clientX - this.editorData.lastX;
            let distanceY = e.clientY - this.editorData.lastY;

            let newX = this.editorData.x + distanceX;
            let newY = this.editorData.y + distanceY;

            if (!(newX >= 0 || (newX + this.editorData.w * this.editorData.zoomQ) <= this.canvas.width)) {
                this.editorData.x = newX;
            }
            if (!(newY >= 0 || (newY + this.editorData.h * this.editorData.zoomQ) <= this.canvas.height )) {
                this.editorData.y = newY;
            }


            this.draw();

            this.editorData.lastX = e.clientX;
            this.editorData.lastY = e.clientY;
        }
    }
}