import * as React from "react";

export default class TextAreaTracked extends React.Component<{
    onInput: Function,
    limit: number,
    value: string,
    placeholder?: string,
    error?: string,
    acceptHtml?: boolean
}, {
    inputLength: number,
    input: string
}> {
    counterClass: string;
    constructor(props: any) {
        super(props);
        this.state = {
            inputLength: this.props.value ? this.props.value.replace(/<(div|\/div|br|p|\/p|a|\/a)[^>]{0,}>/g, "").length : 0,
            input: this.props.value || ""
        }
        this.counterClass = "counter";
    }
    private handleInput(e: Event) {
        let target = e.currentTarget as HTMLTextAreaElement;
        let val = this.props.acceptHtml ? target.textContent : target.value;
        let strippedVal = val.replace(/<(div|\/div|br|p|\/p|a|\/a)[^>]{0,}>/g, "");
        this.counterClass = this.counterClass.replace(" text-danger", "");
        if (strippedVal.length >= this.props.limit) {
            let ovChar = strippedVal.length - this.props.limit;
            this.counterClass += " text-danger"
            val = val.substring(0, ovChar);
        } 
        
        this.setState({
            inputLength: strippedVal.length,
            input: val
        }, ()=>{
            this.props.onInput(this.state.input);
        });
    }

    render() {
        return (
            <div className="position-relative textarea-trakced">
                {this.props.acceptHtml ? 
                    <div contentEditable={true} className={"form-control" + (this.props.error && this.props.error.length > 0 ? " has-error" : "")} onKeyUp={this.handleInput.bind(this) as any} data-placeholder={this.props.placeholder || ""} dangerouslySetInnerHTML={{__html: this.state.input}}></div>
                : <textarea name="desc" className={"form-control" + (this.props.error && this.props.error.length > 0 ? " has-error" : "")} onChange={this.handleInput.bind(this) as any} placeholder={this.props.placeholder || ""} value={this.state.input}></textarea>}
                <span className={this.counterClass}>{this.props.limit - this.state.inputLength}</span>
                {this.props.error && this.props.error.length > 0 ? <div className="error">{this.props.error}</div> : ""}
            </div>
        );
    }

}