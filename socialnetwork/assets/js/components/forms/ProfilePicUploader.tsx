import * as React from "react";

export default class ProfilePicUplodader extends React.Component<{
    onUpload: Function,
    value: File
},{
    file: File
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            file: this.props.value ||null
        }
    }

    private hanldeUpload(e: Event) {
        let target = e.target as HTMLInputElement;
        let files = target.files;
        let url = "";
        let name = "";
        if (files.length > 0) {
            this.setState({
                file: files[0]
            },()=>{
                target.value = null;
                this.props.onUpload(this.state.file);
            });
        } else {
            this.setState({
                file: null
            });
        }
    }

    render() {
        return (  
            <div className="profile-pic-uploader">
                <input type="file" onChange={this.hanldeUpload.bind(this)}/>
                <div className="label d-flex">
                    <i className="fas fa-camera"></i>
                    <span>{this.state.file != null ? this.state.file.name : "Upload photo"}</span>
                </div>
            </div>
        );
    }
}