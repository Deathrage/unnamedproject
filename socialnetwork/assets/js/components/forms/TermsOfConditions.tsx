import * as React from "react";
import PopupOpener from "../popups/PopupOpener";

export default class TermsOfConditions extends React.Component<{
    checked: boolean,
    onChange: Function
},{
    checked: boolean
}> {
    constructor(props: any) {
        super(props); 
        this.state = {
            checked: this.props.checked || false
        }
    }

    get innerHtml() {
        return (
            <a href="#">terms and conditions</a>
        );
    }

    private onChange(e: Event) {
        let target = e.currentTarget as HTMLInputElement;
        this.setState({
            checked: target.checked
        }, ()=>{
            this.props.onChange(this.state.checked);
        });
    }

    render() {
        return (
            <label className="form-group form-check pl-0 ml-5 mt-5 font-weight-bold">
                I agree to these <PopupOpener innerMarkup={this.innerHtml} popupId="terms"/>
                <input type="checkbox" name="terms" className="form-check-input" onChange={this.onChange.bind(this)} checked={this.state.checked}/>
                <span className="checkmark"></span>
            </label>
        );
    }
}