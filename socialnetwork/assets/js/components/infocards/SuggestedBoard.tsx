import * as React from "react";
import { Link } from "react-router-dom";

export default class SuggestedBoard extends React.Component<{
    className?: string,
    headline?: string,
    namePrefix?: string,
    nameSuffix?: string,
    valuePreffix?: any,
    valueSuffix?: any,
    items: {
        id: number,
        name: string,
        value: any,
        image: string,
        link?: string
    }[]
},any> {
    constructor(props:any) {
        super(props);
    }

    render() {
        return this.props.items.length > 0 ? (
            <div className={"suggested-board" + (this.props.className || "")}>
                {this.props.headline != null ? 
                    <div className="suggested-board-headline">
                        <span>{this.props.headline}</span>
                    </div>
                : ""}
                {this.props.items.length > 0 ?
                    <div className="suggested-board-list">
                        {this.props.items.map((item)=>{
                            return (
                                <Link to={encodeURI(item.link)} key={item.id} className="suggested-board-item">
                                    {item.image != null && item.image != "" ?
                                        <div className="suggested-board-image">
                                            <img src={item.image}/>
                                        </div>
                                     : ""}
                                    <div className="suggested-board-body">
                                        <div className="name">{String(this.props.namePrefix || "") + String(item.name || "") + String(this.props.nameSuffix || "")}</div>
                                        <div className="info-value">{String(this.props.valuePreffix || "") + String(item.value || "") + String(this.props.valueSuffix || "")}</div>
                                    </div>
                                </Link>
                            );
                        })}
                    </div>
                : ""}
            </div>
        ) : ("");
    }
}