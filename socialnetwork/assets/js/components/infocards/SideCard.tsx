import * as React from "react";

export default class SideCard extends React.Component<{
    className?: string
},any> {
    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <div className={"card info " + this.props.className || ""}>
                {this.props.children}
            </div>
        );
    }
}