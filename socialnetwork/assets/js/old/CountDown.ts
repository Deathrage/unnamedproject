import * as $ from "jquery";

export function countdown($el: JQuery, to: Date): void {
    var x = setInterval(function () {

        // Get todays date and time
        var countDownDate = to.getTime();
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        $el.html("<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>");

        // If the count down is finished, write some text 
        if (distance < 0) {
            clearInterval(x);
            $el.html("");
        }
    }, 1000);
}