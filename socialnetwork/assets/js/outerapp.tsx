import * as React from "react";
import { render } from "react-dom";
import Signup from "./components/signup/signup"
import Signin from "./components/signin/signin"

import PopupManager from "./components/popups/PopupManager";
import applyPolyfills from "./utils/Polyfills";
import OuterSlider from "./outercomponents/OuterSlider";
import teamSection from "./nonreact/teamsection";
import scroller from "./nonreact/scroller";
import AlphaForm from "./outercomponents/AlphaRegistration";

applyPolyfills();

declare global {
    interface Window { app: any; }
}

if (window.document.getElementById("signin")) {
    render(<Signin/>, window.document.getElementById("signin"));
}
if (window.document.getElementById("signup")) {
    render(<Signup/>, window.document.getElementById("signup"));
}
if (window.document.getElementById("popupmanager")) {
    render(<PopupManager/>, window.document.getElementById("popupmanager"));
}
if (window.document.getElementById("js-slider")) {
    render(<OuterSlider slidesCount={6} whRation={1980/960}/>, window.document.getElementById("js-slider"));
}
if (window.document.querySelector(".alpha-form-up")) {
    let cont = window.document.querySelector(".alpha-form-up");
    render(<AlphaForm className=" d-md-flex flex-wrap justify-content-md-center justify-content-xl-start" buttonText="notify me" placeholder="you@your-email.com"/>, cont);
    let child = cont.children[0];
    cont.parentNode.replaceChild(child, cont);
}
if (window.document.querySelector(".alpha-form-down")) {
    let cont = window.document.querySelector(".alpha-form-down");
    render(<AlphaForm className=" d-md-flex flex-wrap justify-content-center" buttonText="notify me" btnClassName="btn-secondary" placeholder="you@your-email.com"/>, cont);
    let child = cont.children[0];
    cont.parentNode.replaceChild(child, cont);
}

document.addEventListener("DOMContentLoaded", function(event) { 
    teamSection();
    scroller();
});
