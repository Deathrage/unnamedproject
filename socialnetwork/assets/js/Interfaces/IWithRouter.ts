import { match } from "react-router";
import { History, Location } from "history";

export interface IWithRouter {
    match: match,
    history: History,
    location: Location
}