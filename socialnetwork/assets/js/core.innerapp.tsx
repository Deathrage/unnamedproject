import * as React from "react";
import { render } from "react-dom";
import { BrowserRouter, match } from "react-router-dom";
import InnerApp from "./layouts/Innerapp";
import createBrowserHistory from 'history/createBrowserHistory'

export class InnerAppCore extends React.Component {
      
    constructor(props: any) {
        super(props)
    }

    render() {
        return (
            <BrowserRouter>
                <InnerApp />
            </BrowserRouter>
        );
    }
}

export const layoutInit = (element: HTMLElement) => {
    if (element) {
        render(<InnerAppCore />, element);
    }
}