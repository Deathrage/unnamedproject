export default function jsonBeautify(json: Object) {
    return JSON.stringify(json, null, "\t"); // stringify with tabs inserted at each level
}