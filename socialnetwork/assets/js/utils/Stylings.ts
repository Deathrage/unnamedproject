export default class Stylings {
    static formatDate(date: Date): string {
        let diff = Math.abs(new Date().getTime() - date.getTime());
        let mins = diff/(1000 * 60);
        if (Math.floor(mins) <= 59) return Math.floor(mins) + "m";
        let hours = mins/60;
        if (Math.floor(hours) <= 23) return Math.floor(hours) + "h";
        let days = hours/24;
        if (Math.floor(days) <= 30) return Math.floor(days) + "d";
        let months = days / 30;
        if (Math.floor(months) <= 11) return Math.floor(months) + "m";
        let yers = days / 365;
        return Math.floor(yers) + "y";
    }
}