export default class MediaHelper {
    static getFileIcon (extension: string): string
    static getFileIcon(file: File): string
    static getFileIcon(arg: File|string): string {
        if (arg instanceof File) {
            var extension = arg.name.split('.').pop();
        } else {
            extension = arg;
        }

        let types = Object.keys(icoEnums);
        let foundType: string;
        types.forEach((type)=>{
            let exs = icoEnums[type].extensions;
            if (exs.indexOf(extension) > -1) {
                foundType = type;
                return false;
            }
        });
        if (foundType == null) foundType = "GENERIC";
        return icoEnums[foundType].markup;
    }
}

export const icoEnums = {
    GENERIC: {
        markup: '<i class="far fa-file"></i>',
        extensions: []
    },
    TEXT: {
        markup: '<i class="far fa-file-alt"></i>',
        extensions: ["rtf", "tex", "txt"]
    },
    WORD: {
        markup: '<i class="far fa-file-word"></i>',
        extensions: ["doc", "docx", "odt", "wks", "wpd", "wps"]
    },
    POWERPOINT: {
        markup: '<i class="far fa-file-powerpoint"></i>',
        extensions: ["key", "odp", "pps", "ppt", "pptx"]
    },
    EXCEL: {
        markup: '<i class="far fa-file-excel"></i>',
        extensions: ["ods", "xlr", "xls", "xlsx"]
    },
    PDF: {
        markup: '<i class="far fa-file-pdf"></i>',
        extensions: ["pdf"]
    },
    VIDEO: {
        markup: '<i class="far fa-file-video"></i>',
        extensions: ["3g2", "3gp", "avi", "flv", "h264", "m4v", "mkv", "mov", "mp4", "mpg", "mpeg", "rm", "swf", "vob", "wmv"]
    },
    CODE: {
        markup: '<i class="far fa-file-excel"></i>',
        extensions: ["html", "php", "js", "c", "class", "cpp", "cs", "h", "java", "sh", "swift", "vb", "fs"]
    },
    AUDIO: {
        markup: '<i class="far fa-file-audio"></i>',
        extensions: ["aif", "cda", "mid", "midi", "mp3", "mpa", "ogg", "wav", "wma", "wpl"]
    },
    ARCHIVE: {
        markup: '<i class="far fa-file-archive"></i>',
        extensions: ["7z", "arj", "deb", "pkg", "rar", "rpm", "tar", "gz", "z", "zip"]
    }
} as IcoEnums

interface IcoEnums {
    [index: string]: IcoInfo;
}

interface IcoInfo {
    markup: string;
    extensions: string[];
}

