export default class EventProvider {
    static trigger(el: HTMLElement, name: string, customInit?: CustomEventInit) {
        var event = new CustomEvent(name, customInit);
        el.dispatchEvent(event);
    }
    static triggerGlobal(name: string, customInit?: CustomEventInit) {
        var event = new CustomEvent(name, customInit);
        document.dispatchEvent(event);
    }
    static triggerPopup(popupId: string, innerText?:string) {
        EventProvider.triggerGlobal("wopener", innerText == null ? {detail: popupId} : {detail: {
            id: popupId,
            innertText: innerText
        }});
    }
}