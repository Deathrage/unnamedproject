
export default class RestartablePromise<T> {

    private chainedThen: Array<Function>;
    private chainedCatch: Array<Function>;
    private chainedListeners: any;
    private state: number;
    private executor: Function;
    private innerVal: any;

    constructor(executor: (resolve: Function, reject: Function) => void) {
        this.executor = executor;
        this.chainedThen = [];
        this.chainedCatch = [];
        this.chainedListeners = new Object();
        this.state = stateEnum.PENDING;
        this.run();
    }

    private resolve(val: any) {
        this.innerVal = val;
        for (let key in this.chainedThen) {
            let func = this.chainedThen[key];
            if (func) func(this.innerVal as T);
        }
        
        let symbols = Object.getOwnPropertySymbols(this.chainedListeners);
        symbols.forEach((symbol: symbol)=>{
            let func = this.chainedListeners[symbol];
            if (func)  func(null, this.innerVal as T);
        });

        this.state = stateEnum.RESOLVED;
    }
    private reject(reason: any) {
        this.innerVal = reason;
        for (let key in this.chainedCatch) {
            let func = this.chainedCatch[key];
            if (func) func(this.innerVal as any);
        }

        let symbols = Object.getOwnPropertySymbols(this.chainedListeners);
        symbols.forEach((symbol: symbol)=>{
            let func = this.chainedListeners[symbol];
            if (func) func(this.innerVal as any, null);
        });

        this.state = stateEnum.REJECTED;
    }

    private run() {
        try {
            this.executor(this.resolve.bind(this), this.reject.bind(this));
        } catch(err) {
            this.reject(err);
        }
    }

    then(fun: (value: any)=>void) {
        this.chainedThen.push(fun);
        if (this.state == stateEnum.RESOLVED) {
            fun(this.innerVal);
        }
    }

    catch(fun: (err: any)=>void) {
        this.chainedCatch.push(fun);
        if (this.state == stateEnum.REJECTED) {
            fun(this.innerVal);
        }
    }

    listen(fun: (err: any, val: any)=>void) {
        let identifier = Symbol();
        this.chainedListeners[identifier] = fun;
        if (this.state == stateEnum.RESOLVED) {
            fun(null, this.innerVal);
        } else if (this.state = stateEnum.REJECTED) {
            fun(this.innerVal, null);
        }
        return identifier;
    }

    unlisten(identifier: symbol) {
        delete this.chainedListeners[identifier];
    }

    restart() {
        this.state = stateEnum.PENDING;
        this.run();
    }
}

const stateEnum = {
    RESOLVED: 1,
    REJECTED: 2,
    PENDING: 3
}