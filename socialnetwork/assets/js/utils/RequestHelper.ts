import { SocketRequest } from "typed-sails";

export default class RequestHelper {
    static get socketStatus() {
        return io.socket.isConnected();
    }

    static fetch(request: SocketRequest) {
        return new Promise((resolve,reject)=>{
            io.socket.request(request, (resData: any, jwres: any) => {
                resolve(resData);
            });
        });
    }
}
