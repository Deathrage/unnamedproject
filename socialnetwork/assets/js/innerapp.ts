import { layoutInit } from "./core.innerapp";
import UserServices from "./services/UserServices";
import IPublicUserInfo from "../../api/interfaces/responses/IPublicUserInfo";
import RestartablePromise from "./utils/RestartablePromise";
import { IFollowStats } from "../../api/interfaces/responses/IFollowStats";
import { IPostStats } from "../../api/interfaces/responses/IPostStats";
import PostList from "./components/posts/postlist";
import applyPolyfills from "./utils/Polyfills";

//first things first - load polyfills
applyPolyfills();

//prepare global object to contain all global data as promises
declare global {
     interface  Window {
        context: IAppContext,
        components: IExposedComponents
    }
    const io: any
}
declare interface IAppContext {
    currentUser: Promise<IPublicUserInfo>
    queryString: Object,
    balance: Promise<number>,
    followStats: Promise<IFollowStats>,
    postStats: Promise<IPostStats>
}
declare interface IExposedComponents {
    postList: PostList
}
window.components = new Object() as IExposedComponents;
window.context = new Object() as IAppContext;

//load contexts
window.context.currentUser = UserServices.getCurrentUserInfo();
window.context.balance = UserServices.getCurrentUserBalance();
window.context.followStats = UserServices.getCurrentUserFollowNumber();
window.context.postStats = UserServices.getCurrentPostStats();
//listeners for changes
io.socket.on('connect', () => {
    io.socket.on("folwupdatecur", (stats: IFollowStats)=>{
        debugger;
        window.context.followStats = new Promise((resolve, reject)=>{resolve(stats)});
    });
    io.socket.on("postupdatecur", (stats: IPostStats)=>{
        window.context.postStats = new Promise((resolve, reject)=>{resolve(stats)});
    });
    io.socket.on("balancecur", (balance: number)=>{
        window.context.balance = new Promise((resolve, reject)=>{resolve(Number(balance))});
    });
});

//start the layout
layoutInit(document.getElementById("app"));

