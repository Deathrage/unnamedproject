export default class FollowServices {
    static followState(userId: number): Promise<number> {
        return new Promise((resolve, reject)=>{
            let req = new Request("/follows/state/" + userId, {method: "GET"});
            fetch(req).then((stream)=>{
                stream.text().then((text)=>{
                    resolve(Number(text));
                }).catch(reject);
            }).catch(reject);
        });
    }
}