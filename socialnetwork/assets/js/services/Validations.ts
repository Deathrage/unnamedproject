import { IApiResponse } from "../../../api/interfaces/IApiResponse";

export default class Validations {

    static async serverValidate(rule: string, value: string): Promise<IApiResponse> {
        let myRequest = new Request("/validate/" + rule, {
            method: "POST",
            body: JSON.stringify({val:value})
        });
        let data;
        await fetch(myRequest).then(async (res)=>{
            await res.json().then((json)=>{
                data = json;
            });
        });
        return data;
    };


}
