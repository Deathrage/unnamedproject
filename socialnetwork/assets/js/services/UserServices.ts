import IPublicUserInfo from "../../../api/interfaces/responses/IPublicUserInfo";
import RestartablePromise from "../utils/RestartablePromise";
import { IFollowStats } from "../../../api/interfaces/responses/IFollowStats";
import { IPostStats } from "../../../api/interfaces/responses/IPostStats";

export default class UserServices {
    static getCurrentUserInfo(): Promise<IPublicUserInfo> {
        return new Promise((resolve, reject)=>{
            let req = new Request("/user/current", {method: "GET"});
            fetch(req).then((noJson)=>{
                noJson.json().then((json)=>{
                    resolve(json);
                }).catch(reject);
            }).catch(reject);
        });
    }
    static getUserInfo(usId: number): Promise<IPublicUserInfo> {
        return new Promise((resolve, reject)=>{
            let req = new Request("/user/" + usId, {method: "GET"});
            fetch(req).then((noJson)=>{
                noJson.json().then((json)=>{
                    resolve(json);
                }).catch(reject);
            }).catch(reject);
        });
    }

    static getCurrentUserBalance(): Promise<number> {
        return new Promise((resolve, reject)=>{
            resolve(0);
        });
    }
    static getUserBalance(usdId: number): Promise<number> {
        return new Promise((resolve, reject)=>{
            resolve(0);
        });
    }
    //follow stats
    static getCurrentUserFollowNumber(): Promise<IFollowStats>  {
        return new Promise((resolve,reject:any)=>{
            let req = new Request("/user/follows");
            fetch(req).then((stream)=>{
                stream.json().then((data: IFollowStats)=>{
                    resolve(data);
                }).catch(reject);
            }).catch(reject);
        });
    }
    static getFollowNumber(usId?: number): Promise<IFollowStats> {
        return new Promise((resolve,reject)=>{
            let req = new Request("/user/follows/" + usId);
            fetch(req).then((stream)=>{
                stream.json().then((data: IFollowStats)=>{
                    resolve(data);
                }).catch(reject);
            }).catch(reject);
        });
    }
    //post stats
    static getCurrentPostStats(): Promise<IPostStats> {
        return new Promise((resolve,reject: any)=>{
            let req = new Request("/posts/stats");
            fetch(req).then((stream)=>{
                stream.json().then((data: IPostStats)=>{
                    resolve(data);
                }).catch(reject);
            }).catch(reject);
        });
    }
    static getPostStats(usId: number): Promise<IPostStats> {
        return new Promise((resolve,reject)=>{
            let req = new Request("/posts/stats/" + usId);
            fetch(req).then((stream)=>{
                stream.json().then((data: IPostStats)=>{
                    resolve(data);
                }).catch(reject);
            }).catch(reject);
        });
    }
    
}