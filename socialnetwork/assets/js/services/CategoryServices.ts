export default class CategoryServices {
    static getCategory(categoryname: string): Promise<object> {
        return new Promise((resolve, reject)=>{
            let req = new Request("/categories/" + categoryname, {
                method: "GET"
            });
            fetch(req).then(response=>{
                response.json().then(resolve);
            })
        });
    }
}