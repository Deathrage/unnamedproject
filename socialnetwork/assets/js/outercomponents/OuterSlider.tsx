import * as React from "react";
import * as anime from "animejs";

export default class OuterSlider extends React.Component<{
    slidesCount: number
    whRation: number
},{
    slides: HTMLImageElement[],
    loadedImages: number,
    width: number,
    displayIndex: number
}> {
    boundFunctions: any;
    interval: number;
    constructor(props: any) {
        super(props);
        this.state = {
            slides: [],
            loadedImages: 0,
            width: 0,
            displayIndex: 0
        }

        this.boundFunctions = {
            onResize: this.onResize.bind(this)
        }
    }

    componentDidMount() {
        this.setState({
            width: (this.refs["wrapper"] as HTMLElement).offsetWidth
        });
        this.fetchSlides();
        window.addEventListener("resize", this.boundFunctions.onResize);
        this.interval = (setInterval(this.slideNext.bind(this), 8000) as any); 
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.boundFunctions.onResize);
        clearInterval(this.interval);
    }

    componentDidUpdate() {
        if (this.refs["slides"]) {
            this.state.slides.forEach((image, i)=>{
                if (i == this.state.displayIndex) image.style.display = "block";
                else image.style.display = "none";
                (this.refs["slides"] as HTMLElement).appendChild(image);
            })
        }
    }

    render() {
        return (
            <div className="slider" ref="wrapper" style={{
                height: this.height,
            }}>
                {this.state.slides.length != 0 && this.props.slidesCount == this.state.loadedImages ? 
                    <div ref="slides" className="slider-slides"></div> 
                : <div className="loader"></div>}
            </div>
        );
    }

    private onResize(e: Event) {
        this.setState({
            width: (this.refs["wrapper"] as HTMLElement).offsetWidth
        });
    }

    private get height() {
        return this.state.width / this.props.whRation;
    }

    private fetchSlides() {
        for (let i = 0; i < this.props.slidesCount; i++) {
            let img = new Image();
            img.src = `/images/slides/${i + 1}.png`;
            img.onload = () => {
                this.setState({
                    loadedImages: this.state.loadedImages + 1
                });
            };
            this.state.slides.push(img);
            this.setState({
                slides: this.state.slides
            });
        }
    }

    private slideNext() { 
        let cuSlide = this.state.slides[this.state.displayIndex];
        //get next index
        let newI = this.state.displayIndex + 1;
        if (newI >= this.props.slidesCount) newI = 0;
        //get next slide
        let nSlide = this.state.slides[newI];
        //begin the show
        let hide = anime({
            targets: cuSlide,
            opacity: 0,
            duration: 1000,
            easing: 'easeInOutQuad'
        });
        hide.finished.then(()=>{
            cuSlide.style.display = "none";
            cuSlide.style.opacity = "1";
            //switch slides
            nSlide.style.opacity = "0";
            nSlide.style.display = "block";
            //show new
            let show = anime({
                targets: nSlide,
                opacity: 1,
                duration: 1000,
                easing: 'easeInOutQuad'
            });
            //set it
            show.finished.then(()=>{
                this.setState({
                    displayIndex: newI
                })
            })
        });
    }
    
}