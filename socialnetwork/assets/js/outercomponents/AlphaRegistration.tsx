import * as React from "react";

export default class AlphaForm extends React.Component<{
    buttonText?: string,
    placeholder?: string,
    className?: string,
    btnClassName?: string
},{
    email: string,
    message: string
}> {
    constructor(props: any) {
        super(props);
        this.state = {
            email: "",
            message: null,
        };
    }

    private hanldeInput(e: Event) {
        let tar = e.target as HTMLInputElement;
        this.setState({
            email: tar.value
        });
    }

    private enterSubmit(e: KeyboardEvent) {
        e.preventDefault();
        if (e.keyCode === 13) {
            this.handleSubmit();
        }
    }

    private handleSubmit(e?: Event) {
        if (e) e.preventDefault();
        let req = new Request("/alpha/signup",{
            method: "POST",
            body: JSON.stringify({
                email: this.state.email   
            })
        });

        fetch(req).then(res=>{
            res.text().then(text => {
                this.setState({
                    message: text
                });
            });
        });
    }

    render() {
        return (
            <form className={"alpha-form " + (this.props.className ? this.props.className : "")}>
                {this.state.message ? <div style={{width: "100%"}}>{this.state.message.replace(/\"/g, "")}</div> : ""}
                <input type="email" name="email" placeholder={this.props.placeholder} value={this.state.email} onChange={this.hanldeInput.bind(this)} onKeyUp={this.enterSubmit.bind(this)}/>
                <input className={"btn " + (this.props.btnClassName || "btn-primary")} type="submit" value={this.props.buttonText} onClick={this.handleSubmit.bind(this)} />
            </form>
        );
    }
}