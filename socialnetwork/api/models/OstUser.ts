import { Record } from "typed-sails";

/**
 * OstUser.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    userId: {
      type: "number", required: true
    },
    ostId: {
      type: "string", required: true
    }

  },

};

export interface OstUser extends Record {
  userId: number,
  ostId: string
}

