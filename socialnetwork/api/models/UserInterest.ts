import { Record } from "typed-sails";

/**
 * UserInterest.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //system
    updatedAt: false,
    userId: {
      type: "number", required: true
    },
    categoryId: {
      type: "number", required: true
    }

  },

};

export interface UserInterest extends Record {
  userId: number;
  categoryId: number;
}