import { Record } from "typed-sails";

/**
 * File.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    binary: {
      type: "ref",
      columnType: "bytea",
      required: true
    }

  },

};

export interface File extends Record {
  binary: Buffer;
}