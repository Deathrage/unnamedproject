import { Record } from "typed-sails";

/**
 * Category.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    categoryName: {
      type: "string", required: true
    },
    iconClass: {
      type: "string"
    },
    associatedColor: {
      type: "string"
    }

  },

};

export interface Category extends Record {
  categoryName: string;
  iconClass: string;
}