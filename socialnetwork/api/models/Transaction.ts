import { Record } from "typed-sails";

/**
 * Transaction.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    actionId: {
      type: "Number",
      required: true
    },
    transactionId: {
      type: "String",
      required: true
    },
    transactionObject: {
      type: "json",
      required: true
    }

  },

};

export interface Transaction extends Record {
  actionId: Number;
  transactionId: String,
  transactionObject: JSON
}