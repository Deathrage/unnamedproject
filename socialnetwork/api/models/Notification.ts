/**
 * Notification.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    
    //who is should see it
    notificationUserId: {
      type: "number", 
      required: true
    },

    //user that triggered id
    triggererUserId: {
      type: "number", 
      required: true
    },
    //type of action
    type: {
      type: "string", 
      required: true
    },
    //what was targeted
    target: {
      type: "number",
      required: true
    },
    //optional data
    data: {
      type: "string"
    },

    //seen at
    seenAt: {
      type: "ref",
      columnType: "timestamptz"
    }

  },

};

