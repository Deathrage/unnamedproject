import { Record } from "typed-sails";

/**
 * Like.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //input
    value: {
      type: "number", 
      required: true
    },
    //system
    userId: {
      type: "number", 
      required: true
    },
    postId: {
      type: "number", 
      required: true
    },
    transactionId: {
      type: "string", 
      allowNull: true
    }

  },

};

export interface Like extends Record {
  value: Number;
  userId: Number;
  postId: Number;
  transactionId: String;
}