import { Record } from "typed-sails";

/**
 * Alpha.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
declare const module: any;
module.exports = {

  attributes: {

    email: {
      type: "string",
      required: true
    }

  },

};

export interface Alpha extends Record {
  email: string;
}


