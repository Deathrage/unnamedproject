import { Record } from "typed-sails";

/**
 * Following.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {


    //system
    updatedAt: false,
    userId: {
      type: "number", 
      required: true
    },
    followsId: {
      type: "number", 
      required: true
    }

  },

};

export interface Following extends Record {
  userId: Number;
  followsId: Number;
}

