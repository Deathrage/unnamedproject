import { Record } from "typed-sails";

/**
 * Verification.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    user: {
        type: "number"
    },
    verificationToken: {
        type: "string"
    },
    tokenEmited: {
        type: "number", defaultsTo: (new Date()).getTime()
    },
    tokenDeadline: {
        type: "number"
    }
  },

};

