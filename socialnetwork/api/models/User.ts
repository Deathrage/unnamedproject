import { Record } from "typed-sails";

/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  
  attributes: {
    username: {
      type: "string", required: true, unique: true
    },
    password: {
      type: "string", required: true
    },
    passwordEncryption: {
      type: "string", allowNull: true
    },
    email: {
      type: "string", required: true, unique: true
    },
    phone: {
      type: "string", allowNull: true
    },
    fullName: {
      type: "string", allowNull: true
    },
    profilePicture: {
      type: "number", allowNull: true
    },
    description: {
      type: "string", allowNull: true
    },
    birthdate: {
      type: "number", allowNull: true
    },
    verified: {
      type: "boolean", defaultsTo: false
    }

  },

};

export interface User extends Record {
  username: string
  password: string
  passwordEncryption: string
  email: string
  phone: string
  fullName: string,
  profilePicture: Number,
  description: string,
  birthdate: number
}

