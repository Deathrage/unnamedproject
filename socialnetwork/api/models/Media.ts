import { Model, Record } from "typed-sails";

/**
 * Media.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
declare const Media: Model;
module.exports = {

  attributes: {

    //system
    userId: {
      type: "number", 
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    mime: {
      type: "string",
      required: true
    },
    size: {
      type: "number",
      required: true
    },
    extension: {
      type: "string"
    },
    fileId: {
      type: "number",
      required: true
    }
  }

};

export interface Media extends Record {
  userId: Number;
  name: string;
  mime: string;
  size: Number;
  extension: String;
  fileId: Number;
}