import { Record } from "typed-sails";

/**
 * Post.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    
    //user input
    title: {
      type: "string", 
      required: true, 
      maxLength: 100,
      minLength: 4
    },
    description: {
      type: "string", 
      required: true, 
      maxLength: 300,
      minLength: 4
    },
    attachedFile: {
      type: "number", 
      allowNull: true
    },
    //system
    userId: {
      type: "number", 
      required: true
    },
    categoryId: {
      type: "ref",
      columnType: "integer[]",
      required: true
    },
    transactionId: {
      type: "string", 
      allowNull: true
    }

  },

};

export interface Post extends Record {
  [key: string] :any;
  
  title: String;
  description: String;
  attachedFile: Number;
  userId: Number;
  categoryId: number[];
  transactionId: String;
}