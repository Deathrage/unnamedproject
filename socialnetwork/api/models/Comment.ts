import { Record } from "typed-sails";

/**
 * Comment.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {


    //input
    text: {
      type: "string", 
      required: true,
      maxLength: 300,
      minLength: 4
    },
    //system
    userId: {
      type: "number", 
      required: true
    },
    postId: {
      type: "number", 
      required: true
    },
    attachedFile: {
      type: "number", 
      allowNull: true
    },
    transactionId: {
      type: "string", 
      allowNull: true
    }

  },

};

export interface Comment extends Record {
  text: String;
  userId: Number;
  postId: Number;
  transactionId: String;
  attachedFile: number;
}
