export default class FollowerQueries {
    static suggestedFollowers(userid: number) {
        return `
        SELECT u.id,
            u.username,
            (SELECT COUNT(id) from following f where f."followsId" = u.id) as "followers",
            CONCAT('/media/get/', m.id, '/', m.name) as "profilePictureUrl",
            COALESCE(m.id, 0) as "hasProfilePicture"
            FROM "user" u
            LEFT JOIN media m ON u."profilePicture" = m.id
            WHERE  u.id != ${userid} AND u.id NOT IN (
            SELECT mf."followsId" from following mf WHERE mf."userId" = ${userid}
            )
            ORDER BY RANDOM()
            LIMIT 3
        `;
    }
}