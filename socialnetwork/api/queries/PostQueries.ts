export default class PostQueries {
    static postStats = `
        SELECT 
        (SELECT COUNT(p.id) from "post" p WHERE p."userId" = u.id) as "postCount",
        (SELECT SUM(value)
        from "like" l
        JOIN post p on l."postId" = p.id
        WHERE p."userId" = u.id
        ) as "likeSum"
        FROM "user" u
        WHERE id = $1
    `;

    static postFeed(user: number, skip: number, limit: number, orderby: string) {
        return `SELECT *
                FROM post_joined
                WHERE (("categoryId") && ARRAY(SELECT ui."categoryId" from userinterest ui where ui."userId" = ${user})::int[] OR "userId" = ${user})
                OR ("userId" IN (SELECT f."followsId" from following f WHERE "f"."userId" = ${user}))
                ORDER BY ${orderby}
                LIMIT ${limit}
                OFFSET ${skip}`;
    }
    static singlePost(postId: number) {
        return `SELECT *
                FROM post_joined
                WHERE id = ${postId}
                `;
    }
    static userPosts(userid: number, skip: number, limit: number, orderby: string) {
        return `SELECT *
                FROM post_joined
                WHERE "userId" = ${userid}
                ORDER BY ${orderby}
                LIMIT ${limit}
                OFFSET ${skip}
                `;
    }
    static categoryPosts(categoryid: number, skip: number, limit: number, orderby: string) {
        return `SELECT *
                FROM post_joined
                WHERE ${categoryid} = ANY("categoryId")
                ORDER BY ${orderby}
                LIMIT ${limit}
                OFFSET ${skip}
                `;
    }
}