export default class CommentQueries {
    static singleComment(commentid: number) {
        return `
            SELECT c.*,
            us.username,
            us."profilePicture",
            CONCAT('/media/get/', m.id, '/', m.name) as "profilePictureUrl",
            CONCAT('/media/get/', mC.id, '/', mC.name) as "attachedFileUrl"
            FROM comment c
            LEFT JOIN "public".user us ON us.id = c."userId"
            LEFT JOIN media m ON us."profilePicture" = m.id
            LEFT JOIN media mC ON c."attachedFile" = mC.id
            WHERE c.id = ${commentid}
        `;
    }
    static commentsByPost(postid: number, skip: number, limit: number, orderby: string) {
        return `
            SELECT c.*,
            us.username,
            us."profilePicture",
            CONCAT('/media/get/', m.id, '/', m.name) as "profilePictureUrl",
            CONCAT('/media/get/', mC.id, '/', mC.name) as "attachedFileUrl"
            FROM comment c
            LEFT JOIN "public".user us ON us.id = c."userId"
            LEFT JOIN media m ON us."profilePicture" = m.id
            LEFT JOIN media mC ON c."attachedFile" = mC.id
            WHERE c."postId" = ${postid}
            ORDER BY "${orderby}" DESC
            LIMIT ${limit}
            OFFSET ${skip}
        `;
    }
}