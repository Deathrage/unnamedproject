import { isNumber } from "util";

export default class SearchQueries {
    static smartSearch(tsquery: string, userid?: number, skip: number = 0, limit: number = 10, orderby: string = 'score DESC, "result"."createdAt" DESC') {
        return `
        SELECT result.*,
        CASE type WHEN 'user' THEN CONCAT('/media/get/', mu.id, '/', mu.name) ELSE null END as "profilePictureUrl" ,
        CASE type WHEN 'post' THEN CONCAT('/media/get/', mp.id, '/', mp.name) ELSE null END as "attachedFileUrl", 
        mp.extension,
        mp.name,
        mp.mime,
        CASE type WHEN 'post' THEN
        (SELECT COUNT(cm.id) from public.comment cm WHERE cm."postId" = result.id)
        ELSE null END as "commentCount",
        CASE type WHEN 'post' THEN
        (SELECT SUM(lk.value) from public.like lk WHERE lk."postId" = result.id)
        ELSE null END as "likeCount",
        CASE type WHEN 'user' THEN
        (SELECT COUNT(fw.id) from public.following fw WHERE fw."followsId" = result.id)
        ELSE null END as "followers" ${isNumber(userid) ? `,
        CASE type WHEN 'user' THEN
        (SELECT COUNT(fw.id) from public.following fw WHERE fw."userId" = ${userid} AND fw."followsId" = result.id)
        ELSE null END as "followState"` : ""}

        FROM (
            SELECT 
            'post' as type,
            "createdAt", 
            "updatedAt", 
            id,

            title, 
            description, 
            "attachedFile",
            "userId", 
            "categoryId", 
            "transactionId",
            titlescore,
            descriptionscore,
            
            null as username,
            null as "fullName",
            null as"profilePicture",
            null as usernamescore,
            null as fullnamescore,

            postscore as score
            FROM post_scored('${tsquery}')

            UNION ALL

            SELECT 
            'user' as type,
            "createdAt", 
            "updatedAt", 
            id,

            null as title, 
            null as description, 
            null as "attachedFile",
            null as "userId", 
            null as "categoryId", 
            null as "transactionId",
            null as titlescore,
            null as descriptionscore,

            username,
            "fullName",
            "profilePicture",
            usernamescore,
            fullnamescore,

            userscore as score
            FROM user_scored('${tsquery}')
        ) result
        
        LEFT JOIN public.user pu ON result.id = pu.id AND result.type = 'user'
        LEFT JOIN public.media mu ON pu."profilePicture" = mu.id
        LEFT JOIN public.media mp ON result."attachedFile" = mp.id AND result.type = 'post'

        WHERE score > 0

        ORDER BY ${orderby}
        LIMIT ${limit}
        OFFSET ${skip}
        `;
    }
}