import { Post } from "../../models/Post";

export interface IPublicPostInfo extends Post {
    commentCount: number,
    likeCount: number,
    attachedFileUrl: string,
    mime: string,
    name: string,
    extension: string,
    fullName: string,
    username: string,
    profilePicture: string
}