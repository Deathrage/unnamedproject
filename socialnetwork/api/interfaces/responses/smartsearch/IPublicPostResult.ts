import IPublicResult from "./IPublicResult";

export default interface IPublicPostResult extends IPublicResult {
    id: number;
    title: String;
    description: String;
    attachedFile: Number;
    userId: Number;
    categoryId: number[];
    transactionId: String;

    commentCount: number;
    likeCount: number;
    attachedFileUrl: string;
    mime: string;
    name: string;
    extension: string;
}