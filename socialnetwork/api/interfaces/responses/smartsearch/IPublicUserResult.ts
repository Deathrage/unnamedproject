import IPublicResult from "./IPublicResult";

export default interface IPublicUserResult extends IPublicResult{
    id: number;
    fullName: string;
    username: string;
    profilePicture: number;
    profilePictureUrl: string;
    followers: number;
    followState: number;
}