
export default interface IUserSuggest {
    id: number;
    username: string;
    followers: number;
    profilePictureUrl: string;
    hasProfilePicture: number;
}