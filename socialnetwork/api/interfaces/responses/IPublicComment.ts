import { Comment } from "../../models/Comment";

export interface IPublicComment extends Comment {
    username: string,
    profilePicture: number,
    profilePictureUrl: string
    attachedFileUrl: string
}