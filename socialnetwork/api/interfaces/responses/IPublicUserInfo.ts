import { IPostUser } from "../../classes/PostInfo";

export default interface IPublicUserInfo extends IPostUser{
    id: number
    username: string
    description: string
    email: string
    phone: string
    fullName: string
    ostId: string
    profilePictureUrl: string
}