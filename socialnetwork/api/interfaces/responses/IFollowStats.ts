export interface IFollowStats {
    followers: number;
    followings: number;
}