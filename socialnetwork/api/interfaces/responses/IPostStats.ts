export interface IPostStats {
    postCount: number;
    likeSum: number;
}