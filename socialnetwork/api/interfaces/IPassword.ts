export interface IPassword {
    password: string;
    passwordEncryption: string;
}