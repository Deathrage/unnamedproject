import { AttachmentMeta } from "./IPostCreate";

export interface ICommentCreate {
    comment: string,
    postId: number,
    file: File|Buffer,
    fileMeta: AttachmentMeta
}