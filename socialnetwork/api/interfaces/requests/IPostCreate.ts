export default interface IPostCreate {
    description: string,
    title: string,
    attachment: File,
    attachmentMeta?: AttachmentMeta
    categories: number[]
}

export interface AttachmentMeta {
    filename: string,
    mime: string,
    size: number
}