export interface IBalance {
    available_balance: Number;
    airdropped_balance: Number;
    token_balance: Number;
}