export interface ITransaction {
    id: String;
    from_user_id: String;
    to_user_id: String;
    transaction_hash: String;
    action_id: Number;
    timestamp: Number;
    status: String;
    gas_price: Number;
    gas_used: Number;
    transaction_fee: Number;
    block_number: Number;
    amount: Number;
    commission_amount: Number;
    airdropped_amount: Number;
}