export interface IAction {
    id: Number;
    name: string;
    kind: string;
    currency: String;
    amount: Number;
    arbitrary_amount: boolean;
    commission_percent: number;
    arbitrary_commission: boolean;
}