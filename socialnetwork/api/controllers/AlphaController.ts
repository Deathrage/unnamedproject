import { Model, Sails, Request, Response } from "typed-sails";
import { Validators } from "../utilities/Validators";
import { IApiResponse } from "../interfaces/IApiResponse";
import Message, { MailComposer } from "../utilities/Mailer";

/**
 * AlphaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
declare const Alpha: Model;
declare const sails: Sails;
module.exports = {
  signup: async function(req: Request, res: Response): Promise<void> {
    let body = req.body;

    if (!body.email || body.email.length == 0) {
        return res.ok("Please fill in your email");
    }

    if (!Validators.emailGeneric(body.email)) {
        return res.ok("Please fill in valid email");
    }
    try {
        let emailRec = await Alpha.findOne({email: body.email});
        if (emailRec) {
            return res.ok("This email is already registered");
        } else {
            await Alpha.create({email: body.email});
        }
    } catch(err) {
        return res.serverError({
            success: false,
            data: err
        } as IApiResponse);
    }
    res.ok("Thank you for registering to alpha.");

    //end mail
    let msg = new Message(MailComposer.alphaReigstrion(body.email));
    try {
        await msg.send();
    } catch(err) {
        console.log(err);
    }
  }

};

