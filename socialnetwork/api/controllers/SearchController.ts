import { Sails, Request, Response } from "typed-sails";
import SmartSearch from "../utilities/SmartSearch";

/**
 * SearchController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

declare const sails: Sails;
module.exports = {
  
    smartsearch: async (req: Request, res: Response) => {
        try {
            let phrase = decodeURI(req.params["phrase"]);
            let skip = req.params["skip"];
            let topn = req.params["topn"];

            let search = new SmartSearch();
            search.parsePhrase(phrase);
            search.skip = skip;
            search.limit = topn;
            if (req.user) {
                search.userid = req.user.id;
            }
            let result = await search.execute();
    
            res.ok(result || []);
        } catch(er) {

        }
    }

};

