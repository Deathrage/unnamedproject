import {Sails, Request, Response, Model} from "typed-sails";
import {IBody} from "../interfaces/IBody";
import { IApiResponse } from "../interfaces/IApiResponse";
import { Validators } from "../utilities/Validators";

declare const Like: Model;
module.exports = {

    validate: async function(req: Request, res: Response): Promise<void> {
        let rule = req.params["rule"];

        if (rule == "email") {
            return res.ok(await Validators.email(req.body.val));
        } else if (rule == "password") {
            return res.ok(await Validators.password(req.body.val));
        } else if (rule == "username") {
            return res.ok(await Validators.username(req.body.val));
        } else if (rule == "birthdate") {
            return res.ok(await Validators.birthdate(new Date(req.body.val)));
        }

        return res.ok();
    }

};

