import {Sails, Request, Response, Model} from "typed-sails";
import {IBody} from "../interfaces/IBody";
import { Post } from "../models/Post";
import fetch from 'node-fetch';
import { IPostStats } from "../interfaces/responses/IPostStats";
import PostQueries from "../queries/PostQueries";
import IPostCreate from "../interfaces/requests/IPostCreate";
import { IApiResponse } from "../interfaces/IApiResponse";
import { MediaHelper } from "../utilities/helpers/MediaHelper";
import PostInfo from "../classes/PostInfo";
import Query from "../utilities/Query";
import PostHelper from "../utilities/helpers/PostHelper";
import { UploadedFile } from "../classes/responses/UploadedFiles";
var toString = require('stream-to-string')


/**
 * PostController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
declare const sails: Sails;
declare const Post: Model;
declare const Like: Model;
module.exports = {

    getfeed: async function(req: Request, res: Response): Promise<void> {
        let userid = req.user.id;
        let orderby = req.params["orderby"];
        let topn = req.params["topn"];
        let skip = req.params["skip"];

        let posts = await PostInfo.getFeed(userid, skip, topn, orderby);
        return res.ok(posts);
    },

    getpostsbyuser: async function(req: Request, res: Response): Promise<void>{
        let userid = req.params["userid"];
        if (!(userid > 0)) userid = req.user.id;
        let orderby = req.params["orderby"];
        let topn = req.params["topn"];
        let skip = req.params["skip"];

        let posts = await PostInfo.getUserPosts(userid, skip, topn, orderby);
        return res.ok(posts);
    },

    getpostsbycategory: async function(req: Request, res: Response): Promise<void> {
        let categoryid = req.params["categoryid"];
        if (!(categoryid > 0)) return res.ok([]);
        let orderby = req.params["orderby"];
        let topn = req.params["topn"];
        let skip = req.params["skip"];

        let posts = await PostInfo.getCategoryPosts(categoryid, skip, topn, orderby);
        return res.ok(posts);
    },

    get: async function(req: Request, res: Response): Promise<void> {
        try {
            let postId = req.params["postid"];
            let post = await PostInfo.fetch(postId);
            if (!post) post = new Object() as PostInfo;
            return res.ok(post);
        } catch(err) {
            return res.serverError(err);
        }
    },

    spam: async function(req: Request, res: Response): Promise<void> {
        return res.notFound();
        for(let i = 0; i <100; i++) {
            
        }

        res.ok();
    },

    create: async function(req: Request, res: Response): Promise<void> {
        
        let body = req.body as IPostCreate;
        let usid = req.isSocket ? (req.session as any).passport.user : req.user.id;

        if (typeof(body.categories) == "string") body.categories = String(body.categories).split(",") as any[];
        if (!(body.categories instanceof Array)) body.categories = [body.categories];

        //create the post
        let model = new Object() as Post;
        model.title = body.title;
        model.description = body.description;
        model.categoryId = body.categories;
        model.userId = usid;

        try {
            var post = await Post.create(model).fetch() as any as Post;
        } catch(er) {
            return res.serverError({
                success: false,
                message: er
            } as IApiResponse);
        }

        let attachment: UploadedFile[] = [];
        if (req.isSocket) {
           attachment = await MediaHelper.fromBuffer((body.attachment as any as Buffer), body.attachmentMeta, usid);
        } else {
            attachment = await MediaHelper.fromRequest("attachment", req);
        }

        if (attachment.length > 0){
            let id = attachment[0].mediaId;
            post = await Post.update(post.id, {
                attachedFile: id
            }).fetch() as any as Post;
            if (post instanceof Array) post = post[0];
        }

        let createdPostInfo = await PostInfo.fetch(post.id);

        res.ok({
            success: true,
            data: createdPostInfo
        } as IApiResponse);

        //emit post stats update
        if (req.isSocket) {
            await PostHelper.postStats(usid).then((stats)=>{
                req.socket.emit("postupdatecur", stats);
            });
        }
        return;
    },

    edit: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },

    delete: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },

    getpoststats: async function(req:Request, res:Response): Promise<void> {
        let usId = req.params["userid"];
        if (usId == null) {
            usId = req.user.id;
        }

        PostHelper.postStats(usId).then((stats)=>{
            return res.ok(stats);
        }).catch((reason)=>{
            return res.serverError(reason);
        });
    }
};

