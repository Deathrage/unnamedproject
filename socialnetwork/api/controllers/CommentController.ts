/**
 * CommentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
import {Sails, Request, Response, Model} from "typed-sails";
import {IBody} from "../interfaces/IBody";
import { IApiResponse } from "../interfaces/IApiResponse";
import { ICommentCreate } from "../interfaces/requests/ICommentCreate";
import CommentInfo from "../classes/CommentInfo";
import { MediaHelper } from "../utilities/helpers/MediaHelper";
declare const Comment: Model;
module.exports = {
  
    create: async function(req: Request, res: Response): Promise<void> {
        if (!req.isSocket) return res.badRequest({success: false, message: "Comments are avaiable via sockets only"} as IApiResponse);
        let body = req.body as ICommentCreate;
        let userid = req.isSocket ? (req.session as any).passport.user : req.user.id;
        if (!body.postId) {
            return res.badRequest({
                success: false,
                message: "Comment needs to have post id"
            } as IApiResponse);
        }
        try {
            var newComment = await Comment.create({ 
                text: body.comment,
                userId: userid,
                postId: body.postId
            }).fetch();
            res.ok();
            
            if (body.file) {
                let uploadedFiles = await MediaHelper.fromBuffer(body.file as Buffer, body.fileMeta, userid);
                if (uploadedFiles && uploadedFiles.length > 0) {
                    let id = uploadedFiles[0].mediaId;
                    await Comment.update(newComment.id, {
                        attachedFile: id
                    })
                }
            }

            let commentInfo = await CommentInfo.fetch(newComment.id);
            req.socket.emit("commentadd", {
                success: true,
                data: commentInfo
            } as IApiResponse);
            return;
        } catch(er) {
            return res.serverError({
                success: false,
                message: "Something went wrong, please try again later",
                data: er
            } as IApiResponse)
        }
    },
    edit: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },
    delete: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },

    getcomments: async function(req: Request, res: Response): Promise<void> {
        let orderby = req.params["orderby"];
        let limit = Number(req.params["topn"]);
        let skip = Number(req.params["skip"]);
        let postid = Number(req.params["postid"]);
        try {
            let comments = await CommentInfo.listByPost(postid, skip, limit, orderby);
            return res.ok({
                success: true,
                data: comments
            } as IApiResponse);
        } catch(er) {
            return res.ok({
                success: false,
                data: er
            } as IApiResponse);
        }
    }
};

