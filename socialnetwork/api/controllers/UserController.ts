import { Sails, Request, Response, Model } from "typed-sails";
import { OstService } from "../utilities/OstService";
import { IBody } from "../interfaces/IBody";
import * as passport from "passport";

import crypto = require("crypto");
import { User } from "../models/User";
import IPublicUser from "../interfaces/responses/IPublicUserInfo";
import { UserInfo } from "../classes/UserInfo";
import { IPassword } from "../interfaces/IPassword";
import { MediaHelper } from "../utilities/helpers/MediaHelper";
import { IApiResponse } from "../interfaces/IApiResponse";
import { UserInterest } from "../models/UserInterest";
import { Validators } from "../utilities/Validators";
/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
declare const User: Model;
declare const UserInterest: Model;
declare const sails: Sails;
module.exports = {
  signup: async function (req: Request, res: Response): Promise<void> {

    let body: IBody = req.body;

    //encrypt password if not done
    let pass: IPassword = new Object() as IPassword;
    if (!body["passwordEncryption"]) {
      pass.password = crypto.createHash("sha256").update(body["password"]).digest("hex");
      pass.passwordEncryption = "sha256";
    }
    body = body;
    //validate
    let valid = await Validators.email(body["email"]);
    if (!valid.success) return res.badRequest();
    //create user
    try {
      var createdUser = await User.create({
        email: body["email"],
        username: body["username"],
        password: pass.password,
        passwordEncryption: pass.passwordEncryption,
        fullName: body["fullname"],
        description: body["description"],
        birthdate: new Date(body["birthdate"]).getTime()
      }).fetch() as any as User;
    } catch (err) {
      return res.serverError(err);
    }
    let files = await MediaHelper.fromRequest("adjustedProfilePicture", req);
    if (files.length > 0){
      let id = files[0].mediaId;
      await User.update(createdUser.id, {
        profilePicture: id
      })
    }

    let interests = body["interests"] as string;
    let interestsAr = String(interests).split(",");
    for (let intI in interestsAr) {
      let interest = Number(interestsAr[intI]);
      await UserInterest.create({
        categoryId: interest,
        userId: createdUser.id
      } as UserInterest);
    }

    let resp: IApiResponse = new Object() as IApiResponse;
    resp.success = true;

    req.logIn(createdUser, (err)=>{
      if (err) return res.serverError(err);
      return res.ok(resp);
    })
  },

  signin: async function (req: Request, res: Response): Promise<void> {
    passport.authenticate("local", (err, user, info) => {
      //check if user exists
      if (err) {
        return res.serverError(err);
      }
      if (!user) {
        return res.notFound({
          message: info.message
        });
      }
      //log user in
      req.logIn(user, async (err) => {
        if (err) return  res.serverError(err)
        let response = new Object() as IApiResponse;
        response.success = true;
        return res.ok(response);
      });

    })(req, res, () => { res.notFound() });
  },

  signout: async function (req: Request, res: Response): Promise<void> {
    req.logOut();
    return res.ok({success: true});
  },

  currentuser: async function (req: Request, res: Response): Promise<void> {
    let loggedUserInfo = await UserInfo.fromUser(req.user);
    return res.ok(await loggedUserInfo.toPublicUser());
  },

  getuserinfo: async function (req: Request, res: Response): Promise<void> {
    if (!req.params["username"]) {
      return res.badRequest({
        success: false,
        message: "No username specified"
      } as IApiResponse)
    }
    let userInfo = await UserInfo.fetch(req.params["username"]);
    return res.ok(await userInfo.toPublicUser());
  },

  setUserInfo: async function (req: Request, res: Response): Promise<void> {
    res.notFound();
  }

};

