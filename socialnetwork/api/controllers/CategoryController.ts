import { Model, Sails, Request, Response } from "typed-sails";
import { IBody } from "../interfaces/IBody";

/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
declare const Category: Model;
declare const sails: Sails;

module.exports = {
    create: async function(req: Request, res: Response): Promise<void> {
        let body: IBody = req.body;

        let cat = await Category.create({
            categoryName:body["name"],
            iconClass:body["class"]
        });
        return res.ok(cat);
    },
    getcategoryinfo: async function(req: Request, res: Response): Promise<void> {
        let categoryname = req.params["categoryname"];

        let catInfo = await Category.findOne({
            categoryName: categoryname
        });
        if ((catInfo instanceof Array) && catInfo.length > 0) catInfo = catInfo[0];
        res.ok(catInfo || new Object());
    },

    list: async function(req: Request, res: Response): Promise<void> {
        let result = await Category.find().sort("id ASC");
        return res.ok(result);
    }

};

