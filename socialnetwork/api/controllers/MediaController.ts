/**
 * MediaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
import { Sails, Request, Response, Model } from "typed-sails";
import { IBody } from "../interfaces/IBody";
import { MediaHelper } from "../utilities/helpers/MediaHelper";
import { Readable } from "stream";

declare const sails: Sails;
module.exports = {

  create: async function (req: Request, res: Response): Promise<void> {
    
    let files = await MediaHelper.fromRequest("file", req);
    res.ok(files);
  },
  delete: async function (req: Request, res: Response): Promise<void> {
    res.notFound();
  },
  get: async function (req: Request, res: Response): Promise<void> {
    //req.setTimeout(120000, null);
    //lookup our file
    let id = req.params["id"];
    let name = req.params["name"];

    let fileInfo = await  MediaHelper.getFileInfo({
      id: id,
      name: name
    })

    if (!fileInfo) return res.notFound();

    let filestream = new Readable()
    filestream.push(fileInfo.file.binary, "binary")
    filestream.push(null);

    res.setHeader("content-type", fileInfo.media.mime);
    filestream.pipe(res);
    filestream.on('end', res.end);
    
    // var range = req.headers.range;
    // if (range) {
    //   var parts = range.replace(/bytes=/, "").split("-");
    //   var partialstart = parts[0];
    //   var partialend = parts[1];
    //   var start: number = parseInt(partialstart, 10);
    //   var end: number = partialend ? parseInt(partialend, 10) : fileInfo.media.size as number;
    //   var chunksize: number = end - start;
    // }

    // let head = {
    //   'Content-Type': fileInfo.media.mime,
    //   'Content-Length': chunksize ||fileInfo.media.size
    // } as any;

    // if (range) head['Content-Range'] = 'bytes ' + start + '-' + end + '/' + fileInfo.media.size;

    //parse range

    //if found serve it
    //res.writeHead(200, head);

    //let binary = range ? fileInfo.file.binary.slice(start, end) : fileInfo.file.binary;
    //res.end(binary, "binary");
  },

};

