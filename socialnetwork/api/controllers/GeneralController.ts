import { Model, Sails, Response, Request } from "typed-sails";

/**
 * GeneralController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
declare const User: Model;
declare const sails: Sails;
module.exports = {
  
    homepage: async function(req: Request, res: Response) {

        if (req.isAuthenticated()) {
            return res.view("pages/inapp", {
                layout: "layouts/app.masterpage"
            });
        } else {
            return res.view("pages/homepage");
        }

    }

};

