/**
 * LikeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
import {Sails, Request, Response, Model} from "typed-sails";
import {IBody} from "../interfaces/IBody";
import { Like } from "../models/Like";
import { IApiResponse } from "../interfaces/IApiResponse";

declare const Like: Model;
module.exports = {

    like: async function(req: Request, res: Response): Promise<void> {
        let postId = Number(req.body["postId"]);
        let value = Number(req.body["value"]);
        if (!postId) return res.badRequest();

        //TODO: implement transaction
        let transactionId = null;

        let like = new Object() as Like;
        like.postId = postId;
        like.value = value;
        like.userId = req.user.id;
        like.transactionId = transactionId;

        Like.create(like).then(res.ok).catch(res.serverError);
        return;
    }

};

