/**
 * FollowingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
import {Sails, Request, Response, Model} from "typed-sails";
import {IBody} from "../interfaces/IBody";
import { IFollowStats } from "../interfaces/responses/IFollowStats";
import FollowerQueries from "../queries/FollowerQueries";
import { IApiResponse } from "../interfaces/IApiResponse";
import IUserSuggest from "../interfaces/responses/IUserSuggest";
import Query from "../utilities/Query";
import FollowHelper from "../utilities/helpers/FollowHelper";

declare const sails: Sails;
declare const Following: Model;
module.exports = {

    follow: async function(req: Request, res: Response): Promise<void> {
        let curUsId = req.isSocket ? (req.session as any).passport.user : req.user.id;
        let usId = req.body["userId"];
        if (curUsId == usId) return res.ok(0);
        await Following.findOrCreate({
            userId: curUsId,
            followsId: usId
        },{
            userId: curUsId,
            followsId: usId
        });
        res.ok(2);
        //send updating info
        if (req.isSocket) {
            await FollowHelper.countFollows(usId).then((newState) => {
                req.socket.emit("folwupdate" + usId, newState);
            });
            await FollowHelper.countFollows(curUsId).then((newState) => {
                req.socket.emit("folwupdatecur", newState);
            });
        }
        return;
    },
    unfollow: async function(req: Request, res: Response): Promise<void> {
        let curUsId = req.isSocket ? (req.session as any).passport.user : req.user.id;
        let usId = req.body["userId"];
        if (curUsId == usId) return res.ok(0);
        await Following.destroy({
            userId: curUsId,
            followsId: usId
        });
        res.ok(1);
           //send updating info
           if (req.isSocket) {
            await FollowHelper.countFollows(usId).then((newState) => {
                req.socket.emit("folwupdate" + usId, newState);
            });
            await FollowHelper.countFollows(curUsId).then((newState) => {
                req.socket.emit("folwupdatecur", newState);
            });
        }
        return;
    },

    state: async function(req: Request, res: Response): Promise<void> {
        let who = req.params["who"];

        if (who == req.user.id) {
            return res.ok(0);
        }

        return res.ok(await FollowHelper.followState(req.user.id, who));
    },

    listFollowers: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },

    listFollowings: async function(req: Request, res: Response): Promise<void> {
        res.notFound();
    },

    suggestfollowers: async function(req: Request, res: Response): Promise<void> {
        if (!req.user.id) return res.badRequest({
            success: false,
            message: "no user is logged"
        } as IApiResponse);

        let query = new Query(FollowerQueries.suggestedFollowers(req.user.id));
        try {
            let result = await query.execute();
            if (!result) return res.ok([]);
            let rows = (result as any).rows as Array<IUserSuggest>;
            return res.ok(rows);
        } catch(err) {
            return res.serverError({
                success: false,
                data: err
            });
        }
    },

    countfollows: async function(req: Request, res: Response): Promise<void> {
        let usId = req.params["userid"];
        if (usId == null) {
            usId = req.user.id;
        }

        FollowHelper.countFollows(usId).then((data: IFollowStats)=>{
            return res.ok(data);
        }).catch((reason)=>{
            return res.serverError(reason);

        });
    }
};

