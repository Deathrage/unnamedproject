import { Sails } from "typed-sails";

declare const sails: Sails;
export default class Query {
    private queryText: string;
    private valueToEscape: string[];
    constructor(query: string, valuesToEscape?: string[]) {
        this.queryText = query;
        this.valueToEscape = valuesToEscape;
    }
    async execute() {
        return await sails.getDatastore().sendNativeQuery(this.queryText, this.valueToEscape);
    }
}