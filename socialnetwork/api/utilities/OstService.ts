import {Sails} from "typed-sails";
import { IBalance } from "../interfaces/IBalance";
import { ITransaction } from "../interfaces/ITransaction";
import { IAction } from "../interfaces/IAction";

const OSTSDK: any = require("@ostdotcom/ost-sdk-js");


declare const sails: Sails;

const ostObj: any = new OSTSDK({
  apiKey: sails.config.ost.apiKey,
  apiSecret: sails.config.ost.secretKey,
  apiEndpoint: "https://sandboxapi.ost.com/v1.1/"
});

export const userService: any = ostObj.services.users;
export const airdropService: any = ostObj.services.airdrops;
export const tokenService: any = ostObj.services.token;
export const actionService: any = ostObj.services.actions;
export const transactionService: any = ostObj.services.transactions;
export const balanceService: any = ostObj.services.balances;
export const ledgerService: any = ostObj.services.ledger;

export class OstService {

  /// USER SERVICES
  
  /**
   * @returns {String} - user id 
   */
  public static async createUser(userName: string): Promise<string> {
    let createdId: string;

    await userService.create({name: userName}).then((res:any) => { 
      createdId = res["data"]["user"]["id"];
    }).catch(handleError);

    return createdId;
  }
  /**
   * @param  {Number} userId
   * @returns {Object} - returns ost user object
   */
  public static async getUser(userId: Number): Promise<object> {
    let fetchedUser: object; 

    await userService.get({id: userId}).then((res: any) => { 
      fetchedUser = res["data"]["user"];
    }).catch(handleError);

    return fetchedUser;
  }
  /**
   * @returns {Array<object>} - return array of all ost users
   */
  public static async listUsers(): Promise<Array<object>> {
    let userList: Array<object>;

    await userService.list({}).then((res: any) => { 
      userList = res["data"]["users"];
    }).catch(handleError);

    return userList;
  }

  ///AIRDROP SERVICES
  
  /**
   * @param  {Number} amount - amount to be aidroped
   * @param  {String} userId - user id of target
   * @returns {String} - id of the airdrop
   */
  public static async airdrop(amount: Number, userId: String): Promise<String> {
    let aidropId: String;

    await airdropService.execute({amount: amount, user_ids: userId}).then((res: any) => { 
      aidropId = res["data"]["airdrop"]["id"];
    }).catch(handleError);

    return aidropId;
  }
  /**
   * @param {String} currentStatus - status code to filter by status
   * @returns {Array<object>} - array of airdrops
   */
  public static async listAidrops(currentStatus: String): Promise<Array<object>> {
    let airdropList: Array<object>;

    await airdropService.list({current_status: currentStatus}).then((res: any) => { 
      airdropList = res["data"]["airdrops"];
    }).catch(handleError);

    return airdropList;
  }



  //TOKEN SERVICES

  /**
   * @param  {String} userId
   * @returns {IBalance} - returns balance object
   */
  public static async userBalance(userId: String): Promise<IBalance> {
    let balance: IBalance;

    await balanceService.get({id: userId}).then((res:any) => { 
      balance = res["data"]["balance"];
    }).catch(handleError);

    return balance;
  }
  /**
   * @param  {String} userId
   * @returns {Array<ITransaction>} - returns transaction objects
   */
  public static async listUserTransactions(userId: String):Promise<Array<ITransaction>> {
    let transactions: Array<ITransaction>;

    await ledgerService.get({id: userId}).then((res: any) => { 
      transactions = res["data"]["transactions"];
    }).catch(handleError);

    return transactions;
  }

  /**
   * @returns Object
   * @description Returns token description object
   */
  public static async tokenDescription(): Promise<Object> {
    let response: Object;

    await tokenService.get({}).then((res: any) => { 
      response = res["data"];
    }).catch((err: any) => { 
      Promise.reject(["err"]);
    });

    return response;
  }

  //ACTION SERVICES
  /**
   * @returns {Array<IAction>} - returns actions array
   */
  public static async listActions(): Promise<Array<IAction>> {
    let actions: Array<IAction>;

    await actionService.list({}).then((res: any) => {
      actions = res["data"]["actions"];
    }).catch(handleError);

    return actions;
  }

  //TRANSACTION SERVICES

  /**
   * @param  {string} from - from user id
   * @param  {string} to - to user id
   * @param  {number} action - action id 
   * @param  {string} amount - optional amount when no arbitrary is set
   * @returns {string} - id of transaction
   */
  public static async executeTransaction(from: string, to: string, action: number, amount: Number = null):Promise<String> {
    let id: String;

    let paramO = {from_user_id: from, to_user_id: to, action_id: action} as any;
    if (amount) paramO.amount = amount;

    transactionService.execute(paramO).then((res: any) => { 
      id = res["data"]["transactions"]["id"]; 
    }).catch(handleError);

    return id;
  }
  /**
   * @param  {String} transactionId
   * @returns {ITransaction} - returns transaction object
   */
  public static async getTransaction(transactionId: String): Promise<ITransaction> {
    let transaction: ITransaction;

    await transactionService.get({id: transactionId}).then((res: any) => { 
      transaction = res["data"]["transactions"]; 
    }).catch(handleError);

    return transaction;
  }

  public static async listTransactions(): Promise<Array<ITransaction>> {
    let transaction: Array<ITransaction>;

    await transactionService.list().then((res: any)  => { 
      transaction = res["data"]["transactions"]; 
    }).catch(handleError);

    return transaction;
  }

}


//module.exports = OstService;

function handleError(err: any) {
  Promise.reject(err["err"]);
}