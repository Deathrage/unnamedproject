import * as nodemailer from "nodemailer";

export const smtpConfig = {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'hello@ahaapp.io',
        pass: 'AhaMoment1!'
    }
};

export const transporter = nodemailer.createTransport(smtpConfig, {
    from: `Aha App <${smtpConfig.auth.user}>`
} as nodemailer.SendMailOptions);


export default class Message  {
    body: nodemailer.SendMailOptions;
    constructor(message?: nodemailer.SendMailOptions) {
        if (message) {
            this.body = message;
        } else {
            this.body = {};
        }
    }
    send(): Promise<void> {
        return new Promise((resolve, reject)=>{
            transporter.sendMail(this.body, (err, info)=>{
                if (err) reject(err);
                resolve()
            });
        });
    }
}

export class MailComposer {
    static alphaReigstrion(recipient: string): nodemailer.SendMailOptions {
        return {
            to: recipient,
            subject: "Aha App alpha registration",
            text: "Thank you for your interest in Aha App! We will let you know as soon as you are selected for alpha!"
        } as nodemailer.SendMailOptions;
    }
}