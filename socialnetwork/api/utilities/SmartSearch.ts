import SearchQueries from "../queries/SearchQueries";
import Query from "./Query";
import IPublicUserResult from "../interfaces/responses/smartsearch/IPublicUserResult";
import IPublicPostResut from "../interfaces/responses/smartsearch/IPublicPostResult";

export default class SmartSearch {
    public parsedPhrase: string;
    public orderBy: string;
    public limit: number;
    public skip: number;
    public userid: number;
    constructor() {
        
    }

    parsePhrase(phrase: string) {
        let fullMAr = this.extractFullMatch(phrase);
        fullMAr = fullMAr.filter((el)=>{
            return el != null && el.trim() != "";
        })  
        let fullM = fullMAr.map((match)=> {
            return "( " + match.split(" ").join(" & ") + " )";
        });

        fullMAr.forEach((match)=>{
            phrase = phrase.replace("\"" + match + "\"", " ");
        }); 
        
        let fs = fullM.length > 0 ? " | " + fullM.join(" | ") : "";
        let ps = phrase.split(" ").filter((el)=>{
            return el != null && el.trim() != ""
        }).join(" | ") + fs;
        
        this.parsedPhrase = ps;
    }

    private extractFullMatch(phrase: string) {
        const re = /"(.*?)"/g;
        const result = [];
        let current;
        while (current = re.exec(phrase)) {
          result.push(current.pop());
        }
        return result;
    }

    async execute(): Promise<Array<IPublicPostResut|IPublicUserResult>> {
        let query = new Query(SearchQueries.smartSearch(this.parsedPhrase, this.userid, this.skip, this.limit, this.orderBy));
        try  {
            let result = await query.execute();
            if (!result) return [];
            let rows = (result as any).rows as Array<Object>;
            return this.parseResults(rows);
        } catch (err) {

        }
    }

    private parseResults(rows: any[]): Array<IPublicUserResult|IPublicPostResut> {
        let parsedResult = rows.map((row) => {
            if (row.type == 'post') {
                return {
                    type: 'post',
                    id: row.id,
                    title: row.title,
                    description: row.description,
                    attachedFile: row.attachedFile,
                    userId: row.userId,
                    categoryId: row.categoryId,
                    transactionId: row.transactionId,
                    commentCount: row.commentCount,
                    likeCount: row.likeCount,
                    attachedFileUrl: row.attachedFileUrl,
                    mime: row.mime,
                    name: row.name,
                    extension: row.extension
                } as IPublicPostResut;
            } else if (row.type == 'user') {
                return {
                    type: 'user',
                    id: row.id,
                    fullName: row.fullName,
                    username: row.username,
                    profilePicture: row.profilePicture,
                    profilePictureUrl: row.profilePictureUrl,
                    followers: row.followers,
                    followState: row.followState
                } as IPublicUserResult;
            } else {
                return null;
            }
        });

        parsedResult = parsedResult.filter((res)=>{
            return res != null;
        });

        return parsedResult;
    }
}