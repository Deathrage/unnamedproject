import { IApiResponse } from "../interfaces/IApiResponse";
import { Model } from "typed-sails";

declare const User: Model;
declare const Whitelist: Model;
export class Validators {

    //must be unique, at least 3 characters, only letter and numbers
    public static async username(input: string): Promise<IApiResponse> {
        let res = new Object() as IApiResponse;
        res.success = /([a-zA-Z0-9!?_\-.+,]+){3}/.test(input);

        if (!res.success) {
            res.message = "Username must be at least 3 characters long containing only letters, number and special characters from ?!+-_,.";
        } else {
            let found = await User.findOne({username: input});
            if (found) {
                res.success = false;
                res.message = "Username already taken";
            }
        }

        return res;
    }
    public static emailGeneric(input: string): Boolean {
        return /.+\@.+\..+/.test(input);
    }
    public static async email(input: string): Promise<IApiResponse> {
        let res = new Object() as IApiResponse;
        res.success = /.+\@.+\..+/.test(input);
        if (!res.success) {
            res.message = "Email is not formated properly";
        } else {
            //alpha limitation
            if (process.env.NODE_ENV == 'production') {
                let item = await Whitelist.findOne({
                    email: input
                });
                if (!item){
                    res.success = false;
                    res.message = "This email is not whitelisted to be part of alpha";
                }
                return res;
            }
            let found = await User.findOne({email: input});
            if (found) {
                res.success = false;
                res.message = "This email is already taken";
            }
        }
        return res;
    }
    public static async password(input: string): Promise<IApiResponse> {
        let res = new Object() as IApiResponse;
        res.success = /^.*(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).*.$/.test(input);
        if (!res.success) {
            res.message = "Password must be at least 6 characters long, contain a letter, a number and special character";
        }
        return res;
    }
    public static async phone(input: string): Promise<IApiResponse> {
        let res = new Object() as IApiResponse;


        return res;
    }
    public static async birthdate(input: Date): Promise<IApiResponse> {
        let res = new Object() as IApiResponse;
        res.success = meetsMinimumAge(input, 12);
        if (!res.success) {
            res.message = "Must be at least 12 years old";
        }
        return res;
    }
}

function meetsMinimumAge(birthDate: Date, minAge: number) {
    var tempDate = new Date(birthDate.getFullYear() + minAge, birthDate.getMonth(), birthDate.getDate());
    return (tempDate <= new Date());
}

export const reason = {
    TAKEN: "taken",
    WRONG_FORMAT: "format",
    REQUIRED: "required"
};

export const regex = {
    username: /(.*[a-z]|\d+){3}/,
    email: /.+\@.+\..+/,
    password: /^(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).*$/
};