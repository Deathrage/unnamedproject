import { IFollowStats } from "../../interfaces/responses/IFollowStats";
import { Model } from "typed-sails";

declare const Following: Model;
export default class FollowHelper {

    static async followState(curUs: number, target: number) {
        let f = await Following.findOne({
            userId: curUs,
            followsId: target
        });
        if (f) {
            return 2;
        } else {
            return 1;
        }
    }

    static countFollows(usId: number): Promise<IFollowStats> {
        return new Promise((resolve, reject)=>{
            let follows = new Object() as IFollowStats;
            let followers = Following.count({followsId: usId}).toPromise();
            let followings = Following.count({userId: usId}).toPromise();
    
            Promise.all([followers, followings]).then((vals: any[]) => {
                follows.followers = vals[0];
                follows.followings = vals[1];
                resolve(follows);
            }).catch((reason)=>{
                reject(reason);
            });
        });
    }
}