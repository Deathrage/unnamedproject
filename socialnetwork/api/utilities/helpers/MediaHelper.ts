import { Request, Sails, Model } from "typed-sails";
import { Stream, Writable, Readable, PassThrough } from "stream";
import { Media } from "../../models/Media";
import { File } from "../../models/File";
import { UploadedFiles, UploadedFile } from "../../classes/responses/UploadedFiles";
import { FileInfo } from "../../classes/FileInfo";
import IPostCreate, { AttachmentMeta } from "../../interfaces/requests/IPostCreate";
const streamToBuffer = require("stream-to-buffer");

declare const sails: Sails;
declare const File: Model;
declare const Media: Model;
export class MediaHelper {
    //Retrieving media
    public static async getFileInfo(opts: Object): Promise<FileInfo> {
        let fileInfo: FileInfo;

        let media = await Media.findOne(opts) as any as Media;
        if (!media) return null;
        
        let file = await File.findOne(media.id) as any as File;

        if (media && file) {
            fileInfo = new FileInfo(media, file);
        }

        return fileInfo;
    }
    public static async fromBuffer(buffer: Buffer, meta: AttachmentMeta, owningUser: number) {
        if (!buffer) return [];
        
        let mediaModel = this.createMediaModelFromMeta(meta);
        let fileModel: File = new Object() as File;
        fileModel.binary = buffer;
        //inser binary
        var createdFile = await File.create(fileModel).fetch();
        //asign id to media
        mediaModel.fileId = createdFile.id;
        mediaModel.userId = owningUser || 0;
        var createdMeida = await Media.create(mediaModel).fetch();
        return [{
            mediaId: createdMeida.id,
            name: mediaModel.name
        }] as UploadedFile[];
    }
    // Creating media
    public static fromRequest(field: string, req: Request): Promise<Array<UploadedFile>> {
        let ses = req.session as any;
        let ownerId = ses && ses.passport && ses.passport.user ? ses.passport.user  : 0;
        return new Promise((resolve, reject)=> {
            let recieving = MediaHelper.recieve(ownerId);
            let fuq = req.file(field).upload(recieving, (err: any, uploadedFiles: Array<any>) => {
                if (err) reject(err);

                let uploads = new Array<UploadedFile>();

                uploadedFiles.forEach((el) => {
                    //extact name
                    let nameAr = (el.stream.filename as string).split(".");
                    nameAr.pop();
                    let name = nameAr.join("");
                    //extarct file id
                    let id = el.stream.id;
                    //put file data into files
                    let file = new UploadedFile(id, name);
                    uploads.push(file);
                })
                //return files
                resolve(uploads);
            });
        });
    }

    private static recieve(owningUser: number) {
        var receiver__ = new Stream.Writable({objectMode: true});

        receiver__._write = async function(__newFile: any, _unused: string, done: any) {
            MediaHelper.writeToDB(__newFile, owningUser).then((id: Number)=>{
                __newFile["id"] = id;
                done();
            }).catch((err: any)=>{
                done(err);
            });
        }
        return receiver__;
    }

    public static async writeToDB(stream: any, owningUser: number): Promise<any> {
       
        //populate models
        let mediaModel = MediaHelper.createMediaModel(stream);
        let fileModel: File = new Object() as File;
        fileModel.binary = await this.bufferStream(stream);

        //inser binary
        var createdFile = await File.create(fileModel).fetch();
        //asign id to media
        mediaModel.fileId = createdFile.id;
        mediaModel.userId = owningUser || 0;
        var createdMeida = await Media.create(mediaModel).fetch();
        return createdMeida.id;
    }

    public static bufferStream(stream: Readable): Promise<Buffer> {
        return new Promise((resolve, reject) => {
            try {
                streamToBuffer(stream, (err:any , buffer: Buffer)=>{
                    if (err) throw new Error(err);
                    resolve(buffer);
                });
            } catch(err) {
                reject(err);
            }
        });
    }

    public static createMediaModel(stream: any): Media {
        let media: Media = new Object() as Media;

        let name = (stream.filename as string).split(".");

        media.extension = name.pop();
        media.name = name.join("");
        media.size = stream.byteCount;
        media.mime = stream.headers["content-type"];

        return media;
    }
    public static createMediaModelFromMeta(meta: AttachmentMeta): Media {
        let media: Media = new Object() as Media;

        let name = (meta.filename as string).split(".");

        media.extension = name.pop();
        media.name = name.join("");
        media.size = meta.size;
        media.mime = meta.mime;

        return media;
    }
};