import PostQueries from "../../queries/PostQueries";
import { IPostStats } from "../../interfaces/responses/IPostStats";
import Query from "../Query";

export default class PostHelper {
    static postStats(user: string): Promise<IPostStats> {
        return new Promise(async (resolve,reject)=>{
            try {
                let result = await (new Query(PostQueries.postStats, [user])).execute();
                if (!result) return resolve(null);
                let rows = (result as any).rows;
                if (rows.length > 0) {
                    var data = rows[0] as IPostStats;
                }
                return resolve(data);
            } catch(err) {
                return reject(err);
            }
        });
    }
}