import { Request, Response, Sails } from "typed-sails";
import { Session } from "inspector";
import { Model } from "waterline";

/**
 * authorized
 *
 * @module      :: Policy
 * @description :: TODO: You might write a short summary of how this policy works and what it represents here.
 * @help        :: http://sailsjs.org/#!/documentation/concepts/Policies
 */
declare const sails: Sails;
declare const User: Model;
module.exports = function (req: Request, res: Response, next: Function) {
  if (req.isSocket) {
  let ses = req.session as any;
    if (ses && ses.passport && ses.passport.user) {
      let usId = ses.passport.user;
      let user = User.findOne(usId);
      if (user) {
        return next();
      } else {
        return res.redirect("/signin");
      }
    }
  } else {
    if (req.isAuthenticated() && req.user) {
      return next();
    } else {
      return res.redirect("/signin");
    }
  }
};
