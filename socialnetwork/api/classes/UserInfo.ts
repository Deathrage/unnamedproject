import { Model } from "typed-sails";
import { User } from "../models/User";
import { OstUser } from "../models/OstUser";
import IPublicUser from "../interfaces/responses/IPublicUserInfo";
import { Media } from "../models/Media";

declare const OstUser: Model;
declare const User: Model;
declare const Media: Model;

export class UserInfo {
    user: User;
    ostUser: OstUser;

    constructor(user: User, ostUser: OstUser) {
        this.user = user;
        this.ostUser = ostUser;
    }
    /**
     * @description Returns IPublicUser to be sent in response
     * @returns {Promie<IPublicUser>}
     */
    public async toPublicUser(): Promise<IPublicUser> {
        let pU = new Object() as IPublicUser;
        //transform user
        if (this.user) {
            pU.email = this.user.email;
            pU.fullName = this.user.fullName;
            pU.id = this.user.id;
            pU.phone = this.user.phone;
            pU.username = this.user.username;
            pU.description = this.user.description;
            //fetch profile picture
            let pC = await Media.findOne({
                id: this.user.profilePicture
            }) as any as Media;
            if (pC) {
                pU.profilePictureUrl = UserInfo.profilePictureUrl(pC.id, pC.name);
            }
        }
        //transform ost user
        if (this.ostUser) {
            pU.ostId = this.ostUser.ostId;
        }
        
        return pU;
    }
    /**
     * @description fetches UserInfo based on username
     * @param  {string} username
     * @returns {UserInfo}
     */
    public static async fetch(username: string): Promise<UserInfo>
    /**
     * @description fetches UserInfo based on user id
     * @param  {number|string} userId
     * @returns {UserInfo}
     */
    public static async fetch(userId: number|string): Promise<UserInfo> {
        let user: User;
        let ostUser: OstUser;
        //based on overload fetch both
        if (typeof userId === "string") {
            user = await User.findOne({username: userId}) as any as User;
        } else {
            user = await User.findOne({id: userId}) as any as User;
        } 
        if (user) {
            ostUser = await OstUser.findOne({userId: user.id}) as any as OstUser;
        }

        return new UserInfo(user, ostUser);
    }
    /**
     * @description returns UserInfo based on User object
     * @param  {User} user
     * @returns {UserInfo}
     */
    public static async fromUser(user: User): Promise<UserInfo> {
        let ostUser = await OstUser.findOne({userId: user.id}) as any as OstUser;
        return new UserInfo(user, ostUser);
    }

    /**
     * @description if there is not ost user creates it
     */
    public static async enforceOst() {

    }

    public static profilePictureUrl(id: number, name: string) {
        return `/media/get/${id}/${name}`
    }
}