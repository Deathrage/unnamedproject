
export class UploadedFiles {
    files: Array<UploadedFile>;
    constructor(files: Array<UploadedFile>) {
        this.files = files;
    }
}

export class UploadedFile {
    mediaId: Number;
    name: String;
    constructor(mediaId: Number, name:string) {
        this.mediaId = mediaId;
        this.name = name;
    }
}