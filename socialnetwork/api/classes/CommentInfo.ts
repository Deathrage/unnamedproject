import { IPublicComment } from "../interfaces/responses/IPublicComment";
import { Model } from "typed-sails";
import Query from "../utilities/Query";
import CommentQueries from "../queries/CommentQueries";

declare const Comment: Model;
export default class CommentInfo implements IPublicComment {
    attachedFileUrl: string;
    attachedFile: number;
    username: string;
    profilePicture: number;
    profilePictureUrl: string;
    text: String;    
    userId: Number;
    postId: Number;
    transactionId: String;
    id: number;
    createdAt: Date;
    updatedAt: Date;

    static async fetch(commentid: number): Promise<CommentInfo> {
        let query = new Query(CommentQueries.singleComment(commentid));
        try {
            let result = await query.execute();
            if (!result) return null;
            let rows = (result as any).rows as Array<Object>;
            if (!rows || rows.length == 0) return null;
            return rows[0] as CommentInfo;;
        } catch(er) {
            return Promise.reject(er);
        }
    }

    static async listByPost(postid: number, skip: number = 0, limit: number = 20, orderby: string = `createdAt`): Promise<CommentInfo[]> {
        let query = new Query(CommentQueries.commentsByPost(postid, skip, limit, orderby));
        try {
            let result = await query.execute();
            if (!result) return null;
            let rows = (result as any).rows as Array<Object>;
            if (!rows || rows.length == 0) return null;
            return rows as CommentInfo[];
        } catch(er) {
            return Promise.reject(er);
        }
    }

}