import { Post } from "../models/Post";
import { User } from "../models/User";
import { Model, Sails } from "typed-sails";
import { Media } from "../models/Media";
import IPublicUserInfo from "../interfaces/responses/IPublicUserInfo";
import { UserInfo } from "./UserInfo";
import PostQueries from "../queries/PostQueries";
import { IPublicPostInfo } from "../interfaces/responses/IPublicPostInfo";
import Query from "../utilities/Query";

declare const Post: Model;
declare const User: Model;
declare const Media: Model;
declare const sails: Sails;

export default class PostInfo implements IPublicPostInfo {
    [key: string] :any;
    //post properties
    id: number;
    title: string;
    description: string;
    attachedFile: Number;
    userId: Number; 
    name: string;
    mime: string;
    extension: string;
    categoryId: number[];
    transactionId: String;
    createdAt: Date;
    updatedAt: Date;
    //public post properties
    commentCount: number;
    likeCount: number;
    attachedFileUrl: string;
    //public user properties
    fullName: string;
    username: string;
    profilePicture: string;

    constructor(copycat: any, user?: IPostUser, mediaArray?: Array<Media>) {
        //copy post in here
        for(var prop in copycat) this[prop]=copycat[prop];
    };
    /**
     * @description fetches post info based on post id
     * @param  {Number} postId
     * @returns {PostInfo}
     */
    static async fetch(postId: number): Promise<PostInfo> {
        let query = new Query(PostQueries.singlePost(postId));
        try {
            let result = await query.execute();
            if (!result) return null;
            let rows = (result as any).rows as Array<Object>;
            let posts = rows.map((el: any)=>{
                let postm = new PostInfo(el);
                postm.attachedFileUrl = "/media/get/" + postm.attachedFile + "/" + el.name;
                return postm;
            });
            return (posts.length > 0 ? posts[0] : null);
        } catch(err) {
            return Promise.reject(err);
        }
    }

    static async getFeed(userid: number, skip: number, limit: number, orderby: string): Promise<Array<PostInfo>> {
        let query = new Query(PostQueries.postFeed(userid, skip, limit, orderby));
        try {
            let result = await query.execute();
            if (!result) return [];
            let rows = (result as any).rows as Array<Object>;
            let posts = rows.map((el: any)=>{
                let postm = new PostInfo(el);
                postm.attachedFileUrl = "/media/get/" + postm.attachedFile + "/" + el.name;
                return postm;
            });
            return posts;
        } catch(err) {
            return Promise.reject(err);
        }
    }

    static async getUserPosts(userid: number, skip: number, limit: number, orderby: string): Promise<Array<PostInfo>>  {
        let query = new Query(PostQueries.userPosts(userid, skip, limit, orderby));
        try {
            let result = await query.execute();
            if (!result) return [];
            let rows = (result as any).rows as Array<Object>;
            let posts = rows.map((el: any)=>{
                let postm = new PostInfo(el);
                postm.attachedFileUrl = "/media/get/" + postm.attachedFile + "/" + el.name;
                return postm;
            });
            return posts;
        } catch(err) {
            return Promise.reject(err);
        }
    }

    static async getCategoryPosts(categoryid: number, skip: number, limit: number, orderby: string): Promise<Array<PostInfo>>  {
        let query = new Query(PostQueries.categoryPosts(categoryid, skip, limit, orderby));
        try {
            let result = await query.execute();
            if (!result) return [];
            let rows = (result as any).rows as Array<Object>;
            let posts = rows.map((el: any)=>{
                let postm = new PostInfo(el);
                postm.attachedFileUrl = "/media/get/" + postm.attachedFile + "/" + el.name;
                return postm;
            });
            return posts;
        } catch(err) {
            return Promise.reject(err);
        }
    }
}

export interface IPostUser {
    fullName: string
    username: string
    profilePictureUrl: string
}