import { Media } from "../models/Media";
import { File } from "../models/File";

export class FileInfo {
    media: Media;
    file: File;
    constructor(media: Media, file: File) {
        this.media = media;
        this.file = file;
    }
}