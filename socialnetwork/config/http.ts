import { Request, Response } from "typed-sails";
import { Model } from "waterline";

/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For more information on configuration, check out:
 * https://sailsjs.com/config/http
 */
declare const User: Model;
module.exports.http = {

  /****************************************************************************
  *                                                                           *
  * Sails/Express middleware to run for every HTTP request.                   *
  * (Only applies to HTTP requests -- not virtual WebSocket requests.)        *
  *                                                                           *
  * https://sailsjs.com/documentation/concepts/middleware                     *
  *                                                                           *
  ****************************************************************************/
  trustProxy: true,
  middleware: {

    passportInit: require('passport').initialize(),
    passportSession: require('passport').session(),
    // socketAuthorization: (function () {
    //   return function (req: Request, res: Response, next: Function) {
    //     //if is socket add custom auth
    //     if (req.isSocket) {
    //       //aut function
    //       req.isAuthenticated = () => {
    //         if (req.session && (req.session as any).passport && (req.session as any).passport.user) {
    //           let usId = (req.session as any).passport.user;
    //           let user = User.findOne(usId);
    //           if (user) {
    //             return true;
    //           }
    //         }
    //         return false;
    //       }
    //       //
    //     }
    //     return next();
    //   };
    // })(),

    // logeveryrequest: (req, res, next) => {
    //   var ip = (req.headers['x-forwarded-for'] ||
    //             req.connection.remoteAddress ||
    //             req.socket.remoteAddress ||
    //             req.connection.socket.remoteAddress).split(",")[0];

    //   console.log("Request from " + ip + " to " + req.originalUrl);
    //   next();
    // },
    /***************************************************************************
    *                                                                          *
    * The order in which middleware should be run for HTTP requests.           *
    * (This Sails app's routes are handled by the "router" middleware below.)  *
    *                                                                          *
    ***************************************************************************/

    order: [
      'cookieParser',
      'session',
      'passportInit',
      'passportSession',
      //'socketAuthorization',
      'bodyParser',
      // 'logeveryrequest',
      //'compress',
      //'poweredBy',
      'router',
      'www',
      'favicon',
    ],


    /***************************************************************************
    *                                                                          *
    * The body parser that will handle incoming multipart HTTP requests.       *
    *                                                                          *
    * https://sailsjs.com/config/http#?customizing-the-body-parser             *
    *                                                                          *
    ***************************************************************************/

    // bodyParser: (function _configureBodyParser(){
    //   var skipper = require('skipper');
    //   var middlewareFn = skipper({ strict: true });
    //   return middlewareFn;
    // })(),

    // sqlInjection: (function(){
    //   let sqlinjection = require('sql-injection');
    //   return sqlinjection;
    // })(),

    // passportInit: (function() {
    //   var passport = require('passport');
    //   var reqResNextFn = passport.initialize();
    //   return reqResNextFn;
    // })(),

    // passportSession: (function() {
    //   var passport = require('passport');
    //   var reqResNextFn = passport.session();
    //   return reqResNextFn;
    // })()

  },

};
