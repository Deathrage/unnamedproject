/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  // "*": {
  //   view: "pages/placeholder",
  //   skipAssets: true
  // },

  "GET /signin": {
    view: "pages/signin"
  },
  "GET /signup": {
    view: "pages/signup"
  },
  "GET /spam": {
    controller: "post",
    action: "spam"
  },

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  //Users api
  "POST /user/signup": {
    controller: "user",
    action: "signup"
  },
  "POST /user/signin": {
    controller: "user",
    action: "signin"
  },
  "POST /user/signout": {
    controller: "user",
    action: "signout"
  },
  "GET /user/current": {
    controller: "user",
    action: "currentuser"
  },
  "GET /user/:username": {
    controller: "user",
    action: "getuserinfo"
  },
  "GET /user/follows": {
    controller: "following",
    action: "countfollows"
  },
  "GET /user/follows/:userid": {
    controller: "following",
    action: "countfollows"
  },
  "GET /user/follows/suggest": {
    controller: "following",
    action: "suggestfollowers"
  },

  //Posts api
  "POST /createpost": {
    controller: "post",
    action: "create"
  },
  "POST /createcategory": {
    controller: "category",
    action: "create"
  },
  "GET /posts/get/:postid": {
    controller: "post",
    action: "get"
  },
  "GET /posts/feed/:orderby/:topn/:skip": {
    controller: "post",
    action: "getfeed"
  },
  "GET /posts/list/:orderby/:topn/:skip": {
    controller: "post",
    action: "getpostsbyuser"
  },
  "GET /posts/list/:orderby/:topn/:skip/:userid": {
    controller: "post",
    action: "getpostsbyuser"
  },
  "GET /posts/stats": {
    controller: "post",
    action: "getpoststats"
  },
  "GET /posts/stats/:userid": {
    controller: "post",
    action: "getpoststats"
  },
  "GET /posts/category/:orderby/:topn/:skip/:categoryid": {
    controller: "post",
    action: "getpostsbycategory"
  },

  //comment api
  "POST /comments/create": {
    controller: "comment",
    action: "create"
  },
  "GET /comments/post/:orderby/:topn/:skip/:postid": {
    controller: "comment",
    action: "getcomments"
  },

  //search api
  "GET /smartsearch/:topn/:skip/:phrase": {
    controller: "search",
    action: "smartsearch"
  },

  //follow api
  "GET /follows/state/:who": {
    controller: "following",
    action: "state"
  },
  "POST /follows/follow": {
    controller: "following",
    action: "follow"
  },
  "POST /follows/unfollow": {
    controller: "following",
    action: "unfollow"
  },

  //Likes api
  "POST /posts/like": {
    controller: "like",
    action: "like"
  },

  //categories api
  "GET /categories/list": {
    controller: "category",
    action: "list"
  },
  "POST /categories/create": {
    controller: "category",
    action: "create"
  },
  "GET /categories/:categoryname": {
    controller: "category",
    action: "getcategoryinfo"
  },


  //Media library API
  "POST /media/new": {
    controller: "media",
    action: "create"
  },
  "POST /media/delete/:name": {
    controller: "media",
    action: "create"
  },
   "GET /media/get/:id/:name": {
    controller: "media",
    action: "get"
  },

  "POST /validate/:rule": {
    controller: "validation",
    action: "validate"
  },

  //alpha
  "POST /alpha/signup": {
    controller: "alpha",
    action: "signup"
  },

  
  '/*': {
    skipAssets: true,
    controller: "general",
    action: "homepage"
  }

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
