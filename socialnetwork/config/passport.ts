//Typescript
import * as passport from "passport";
import {Strategy as LocalStrategy} from "passport-local";
import { Model, Sails } from "typed-sails";
import { User } from "../api/models/User";
declare const User: Model;
declare const sails: Sails;

//Require
// const passport = require('passport');
// const LocalStrategy = require('passport-local').Strategy;
const crypto = require("crypto");

passport.serializeUser((user: User, cb) => {
    cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
    User.findOne({ id }, (err, user) => {
        cb(err, user);
    });
});

passport.use(new LocalStrategy({
    usernameField: "username",
    passwordField: "password"
}, (username, password, cb) => {
    User.findOne({email: username}, (err,user)=>{
        if (err) return cb(err);
        //check aftech user
        if (!user) return cb(null, false, {message: "Username not found"});
        let foundUser = user as any as User;
        //enforce encryption on password
        let passToCompare = password;
        if (foundUser.passwordEncryption) {
            passToCompare = crypto.createHash(foundUser.passwordEncryption).update(password).digest("hex");
        }
        //match passwords
        let passMatch = passToCompare == foundUser.password;
        if (passMatch) {
            return cb(null, foundUser, {message: "Login succesful"});
        } else {
            return cb(null, false, {message: "Invalid e-mail or password"});
        }

    })
}));