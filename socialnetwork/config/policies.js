/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  "user/currentuser": "authorized",
  
  "like/like": "authorized",

  "following/countfollows": "authorized",
  "following/suggestfollowers": "authorized",
  "following/state": "authorized",
  "following/follow": "authorized",
  "following/unfollow": "authorized",

  "post/getpoststats": "authorized",
  "post/create": "authorized",
  "post/getpostsbyuser": "authorized",
  "post/getfeed": "authorized",
  "post/create": "authorized",

  "comment/create": "authorized"
  
  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,

};
