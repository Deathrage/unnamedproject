import SmartSearch from "../api/utilities/SmartSearch";
import Message, { transporter } from "../api/utilities/Mailer";

/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function(done: Function) {
  //log env
  console.log(process.env.NODE_ENV);

  //test mailer
  transporter.verify().then((val)=>{
    console.log('Email service is connected');
  }).catch((reason)=>{
    console.log(`Email service failed with ${reason.stack}`)
  });
  
  return done();
};
