var path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const globImporter = require('node-sass-glob-importer');
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: ["babel-polyfill", "./assets/js/innerapp.ts", "./assets/styles/importer.scss"],
  output: {
    path: path.resolve(__dirname, '.tmp/public/'),
    filename: 'js/innerapp.js'
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  mode: process.env.NODE_ENV == "production" ? "production" : "development",
  devtool: process.env.NODE_ENV == "production" ? false : "cheap-module-source-map",
  module: {
    rules: [
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              importer: globImporter()
            }
          }
        ]
      },
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          configFile: "../../tsconfig-fe.json"
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          minified: true,
          compact: false
        } 
      },
      {
        test: /\.(ttf|eot|woff|woff2|otf|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ // define where to save the file
      filename: 'styles/bundle.css',
      chunkFilename: "styles/[id].css"
    }),
  ],
  // optimization: {
  //   minimizer: [new UglifyJsPlugin()]
  // }
};
