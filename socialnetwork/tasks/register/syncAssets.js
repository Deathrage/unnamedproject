/**
 * `tasks/register/syncAssets.js`
 *
 * ---------------------------------------------------------------
 *
 * For more information see:
 *   https://sailsjs.com/anatomy/tasks/register/sync-assets.js
 *
 */
module.exports = function(grunt) {
  grunt.registerTask('syncAssets', [
    'jst:dev',
    'webpack:inner',
    'webpack:outer',
    /*'sass:dev', - tohle bude dělat webpack
    'coffee:dev'*/
    'copy:dev'
  ]);
};
