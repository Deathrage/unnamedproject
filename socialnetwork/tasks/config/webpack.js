const webpackConfig = require('../../webpack.config');
const outerWebpackConfig = require('../../outer.webpack.config');

module.exports = function (grunt) {
  grunt.config.set("webpack", {
    options: {
      stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
    },
    outer: outerWebpackConfig,
    inner: webpackConfig
  });

  grunt.loadNpmTasks('grunt-webpack');
};
