let input = [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15];
let inputAdj = input;


let itemCount = input.length;

let result = [];

for(let i = 0; i < itemCount; i++) {
    let curObs = input[i];
    let avg = getArAvg(inputAdj);

    console.log(`Reamining array has avg of ${avg}`);

    inputAdj.splice(inputAdj.indexOf(curObs), 1);
    if (curObs < avg && curObs > (result.length > 0 ?  result[result.length - 1] : -1)) {
        result.push(curObs);
    }
}

console.log(result.join(", "));

function getArAvg(ar) {
    let sum = ar.reduce((prev, cur) => prev + cur);
    let avg = sum / ar.length;
    return avg;
}